package com.ygaps.travelapp.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.ygaps.travelapp.R;
import com.ygaps.travelapp.network.MyTourMainCommunicationInterface;

public class MyTourMainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener, MyTourMainCommunicationInterface {

    private  static  final String TAG="myTourMainActivity";
    private BottomNavigationView bottomNavigationView;
    public static final int REQUEST_CODE_FOR_SEARCHUSER = 4230;
    public static final int REQUEST_CODE_FOR_MEMBER = 4231;
    public static final int REQUEST_CODE_FOR_RATE = 4239;
    int tourId;

    InfoMyTourFragment infoMyTourFragment= new InfoMyTourFragment();
    MemberOfMyTourFragment memberOfMyTourFragment= new MemberOfMyTourFragment();
    ChatMyTourFragment chatMyTourFragment=new ChatMyTourFragment();
    RateOfMyTourFragment rateOfMyTourFragment= new RateOfMyTourFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_tour_main);

        bottomNavigationView=findViewById(R.id.bottom_myTour_navigation);
        bottomNavigationView.setSelectedItemId(R.id.nav_myTour_information);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_myTour_container,infoMyTourFragment).commit();

        DataByBundleForInFoMyTour();
        DataByBundleForChat();
//        DataByBundleForSearchUser();

    }



    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem)
    {
        Log.i(TAG, "onNavigationItemSelected: ");
        switch (menuItem.getItemId()){
            case R.id.nav_myTour_information:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_myTour_container,infoMyTourFragment).commit();
                return true;
            case R.id.nav_myTour_members:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_myTour_container,memberOfMyTourFragment).commit();
                return true;
            case R.id.nav_myTour_chat:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_myTour_container,chatMyTourFragment).commit();
                return true;
            case R.id.nav_mtTour_rate:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_myTour_container,rateOfMyTourFragment).commit();
                return true;
        }
        return true;
    }

    public void DataByBundleForInFoMyTour()
    {
        Intent intent=getIntent();
        Bundle bundle=intent.getExtras();

        //put data to fragment
        Bundle bundleForInfo= new Bundle();

        if (bundle != null) {
            int id = bundle.getInt("id",0 );
            bundleForInfo.putInt("id", id);
            tourId=id;
            int status = bundle.getInt("status", 0);
            bundleForInfo.putInt("status", status);
            String name = bundle.getString("name", "");
            bundleForInfo.putString("name", name);
            String minCost = bundle.getString("minCost", "");
            bundleForInfo.putString("minCost", minCost);
            String maxCost = bundle.getString("maxCost", "");
            bundleForInfo.putString("maxCost", maxCost);
            String startDate = bundle.getString("startDate", "");
            bundleForInfo.putString("startDate", startDate);
            String endDate = bundle.getString("endDate", "");
            bundleForInfo.putString("endDate", endDate);
            int adults = bundle.getInt("adults",0);
            bundleForInfo.putInt("adults", adults);
            int childs = bundle.getInt("childs", 0);
            bundleForInfo.putInt("childs", childs);
            boolean isPrivate=bundle.getBoolean("isPrivate", false);
            bundleForInfo.putBoolean("isPrivate", isPrivate);
            String avatar= bundle.getString("avatar", "");
            bundleForInfo.putString("avatar", avatar);

            infoMyTourFragment.setArguments(bundleForInfo);

        }
    }


    public void DataByBundleForChat(){
        Intent intent=getIntent();
        Bundle bundle=intent.getExtras();

        //put data to fragment
        Bundle bundleForInfo= new Bundle();
        if (bundle != null) {
            int id = bundle.getInt("id",0 );
            bundleForInfo.putInt("id", id);
            chatMyTourFragment.setArguments(bundleForInfo);
            rateOfMyTourFragment.setArguments(bundleForInfo);
            memberOfMyTourFragment.setArguments(bundleForInfo);
        }
    }

    public void DataByBundleForSearchUser(){
        Intent intent=new Intent(MyTourMainActivity.this, SearchUserActivity.class);
        Bundle bundle=new Bundle();
        bundle.putInt("id",tourId);
        intent.putExtras(bundle);
        startActivityForResult(intent, REQUEST_CODE_FOR_SEARCHUSER);
    }


    @Override
    public String TourId(String tourId) {
        byBundleForReview(tourId);
        return null;
    }

    public void byBundleForReview(String sTourId){
        Intent intent=new Intent(MyTourMainActivity.this, ReviewTour.class);
        Bundle bundle=new Bundle();
        bundle.putString("tourid_review", sTourId);
        intent.putExtras(bundle);
        startActivityForResult(intent, REQUEST_CODE_FOR_RATE);
    }
}
