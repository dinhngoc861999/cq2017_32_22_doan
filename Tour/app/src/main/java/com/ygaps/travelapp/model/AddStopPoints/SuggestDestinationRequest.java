package com.ygaps.travelapp.model.AddStopPoints;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SuggestDestinationRequest {
@SerializedName("hasOneCoordinate")
    private boolean hasOneCoordinate;
@SerializedName("coordList")
    private List<CoordinateSet> coorList;

    public boolean isHasOneCoordinate() {
        return hasOneCoordinate;
    }

    public List<CoordinateSet> getCoorList() {
        return coorList;
    }

    public void setHasOneCoordinate(boolean hasOneCoordinate) {
        this.hasOneCoordinate = hasOneCoordinate;
    }

    public void setCoorList(List<CoordinateSet> coorList) {
        this.coorList = coorList;
    }
}
