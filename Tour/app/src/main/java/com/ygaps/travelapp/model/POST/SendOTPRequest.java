package com.ygaps.travelapp.model.POST;

import com.google.gson.annotations.SerializedName;

public class SendOTPRequest {
    @SerializedName("type")
    private String type;

    public SendOTPRequest(String type, String value) {
        this.type = type;
        this.value = value;
    }

    @SerializedName("value")
    private String value;

    public String getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
