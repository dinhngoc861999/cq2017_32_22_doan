package com.ygaps.travelapp.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.material.textfield.TextInputLayout;
import com.ygaps.travelapp.R;
import com.ygaps.travelapp.model.POST.RegisterResponse;
import com.ygaps.travelapp.model.POST.SendOTPRequest;
import com.ygaps.travelapp.model.POST.SendOTPResponse;
import com.ygaps.travelapp.network.MyAPIClient;
import com.ygaps.travelapp.network.UserServices;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPassword extends AppCompatActivity {
    private EditText editTextEmail;
    private TextInputLayout textInputEmail;
    private Button buttonForgot;
    private UserServices userServices;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        widgets();
        buttonForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkEmail())
                {
                    userServices=MyAPIClient.getInstance().getAdapter().create(UserServices.class);
                    String email=editTextEmail.getText().toString();
                    SendOTPRequest sendOTPRequest=new SendOTPRequest("email",email);
                    Call<SendOTPResponse> call = userServices.sendOTP(sendOTPRequest);
                    call.enqueue(new Callback<SendOTPResponse>() {
                        @Override
                        public void onResponse(Call<SendOTPResponse> call, Response<SendOTPResponse> response) {
                            if(response.code()==200)
                            {
                                saveData();
                                saveUserId(response.body().getUserId());
                                Log.e("forgot: ",response.body().getUserId()+"");
                                Intent intent=new Intent(ForgotPassword.this,ResetPassword.class);
                                startActivity(intent);
                                finish();
                            }
                            if(response.code()==404)
                            {
                                textInputEmail.setError("EMAIL doesn't exist");
                                requestFocus(editTextEmail);
                            }
                        }

                        @Override
                        public void onFailure(Call<SendOTPResponse> call, Throwable t) {

                        }
                    });
                }
            }
        });
    }
    public  void widgets()
    {
        editTextEmail=(EditText)findViewById(R.id.forgot_editText_email);
        textInputEmail=(TextInputLayout)findViewById(R.id.forgot_textInput_email);
        buttonForgot=(Button)findViewById(R.id.buttonForgot);
    }
    public boolean checkEmail()
    {
        if(editTextEmail.getText().toString().trim().isEmpty())
        {
            textInputEmail.setError(getString(R.string.register_err_email));
            requestFocus(editTextEmail);
            return false;
        }
        textInputEmail.setErrorEnabled(false);
        return true;
    }
    public void requestFocus(View view){
        if(view.requestFocus())
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    private void saveData(){
        SharedPreferences sharedPreferences=getSharedPreferences(getString(R.string.SHARED_PREFS),MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString(getString(R.string.save_user),editTextEmail.getText().toString());
        editor.apply();
    }

    private void saveUserId(int userIdSave){
        SharedPreferences sharedPreferences=getSharedPreferences(getString(R.string.SHARED_PREFS),MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putInt(getString(R.string.save_userId),userIdSave);
        editor.apply();
    }
}
