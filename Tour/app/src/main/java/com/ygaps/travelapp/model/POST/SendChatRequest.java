package com.ygaps.travelapp.model.POST;

import com.google.gson.annotations.SerializedName;

public class SendChatRequest {
    @SerializedName("tourId")
    private String tourId;
    @SerializedName("userId")
    private String userId;
    @SerializedName("noti")
    private String noti;

    public SendChatRequest(String tourId, String userId, String noti) {
        this.tourId = tourId;
        this.userId = userId;
        this.noti = noti;
    }

    public String getTourId() {
        return tourId;
    }

    public String getUserId() {
        return userId;
    }

    public String getNoti() {
        return noti;
    }

    public void setTourId(String tourId) {
        this.tourId = tourId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setNoti(String noti) {
        this.noti = noti;
    }
}
