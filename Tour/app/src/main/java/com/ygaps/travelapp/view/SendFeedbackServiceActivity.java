package com.ygaps.travelapp.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.ygaps.travelapp.R;
import com.ygaps.travelapp.model.POST.SendFeedbackRequest;
import com.ygaps.travelapp.model.POST.SendReviewTourResponse;
import com.ygaps.travelapp.network.MyAPIClient;
import com.ygaps.travelapp.network.UserServices;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SendFeedbackServiceActivity extends AppCompatActivity {
    private int serviceId;
    private EditText editText;
    private RatingBar ratingBar;
    private Button button;
    private int star = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_tour);
        widget();
        event();
    }

    private void widget() {
        editText = (EditText) findViewById(R.id.review_edittextinfo);
        ratingBar = (RatingBar) findViewById(R.id.ruleRatingBar);
        button = (Button) findViewById(R.id.button_send_review);
        Drawable drawable = ratingBar.getProgressDrawable();
        drawable.setColorFilter(Color.parseColor("#FFFF00"), PorterDuff.Mode.SRC_ATOP);

        Intent intent = getIntent();
        serviceId = intent.getIntExtra("serviceId",-1);
    }

    public void event(){
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                star=(int)rating;
                ratingBar.setRating(rating);
            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String review = "";

                review=editText.getText().toString();
                Log.e("serviceID",serviceId+"");
                UserServices userServices= MyAPIClient.getInstance().getAdapter().create(UserServices.class);
                SendFeedbackRequest sendFeedbackRequest=new SendFeedbackRequest(serviceId,star,review);
                Call<SendReviewTourResponse> call=userServices.send_feedback_service(loadToken(),sendFeedbackRequest);

                call.enqueue(new Callback<SendReviewTourResponse>() {
                    @Override
                    public void onResponse(Call<SendReviewTourResponse> call, Response<SendReviewTourResponse> response) {
                        if(response.code()==200)
                        {
//                            Intent i = new Intent(SendFeedbackServiceActivity.this,MyTourMainActivity.class);
//                            startActivity(i);
//                            finish();
                            Toast.makeText(getApplicationContext(),"Feedback sended",Toast.LENGTH_SHORT).show();

                        }
                        else
                        {
                            Log.e("Send review", "fail");
                        }
                    }

                    @Override
                    public void onFailure(Call<SendReviewTourResponse> call, Throwable t) {

                    }
                });
                Intent resultintent = new Intent();
                setResult(1, resultintent);
                finish();//finishing activity


            }
        });

    }

    private  String loadToken()
    {
        SharedPreferences sharedPreferences=this.getSharedPreferences(getString(R.string.SHARED_PREFS), MODE_PRIVATE);
        return sharedPreferences.getString(getString(R.string.save_token),"");
    }
}
