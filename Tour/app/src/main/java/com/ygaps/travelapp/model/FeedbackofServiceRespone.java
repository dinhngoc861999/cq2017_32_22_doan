package com.ygaps.travelapp.model;

import com.google.gson.annotations.SerializedName;

public class FeedbackofServiceRespone {
    @SerializedName("id")
    private int id;
    @SerializedName("userId")
    private String userId;
    @SerializedName("name")
    private String name;
    @SerializedName("phone")
    private String phone;
    @SerializedName("avatar")
    private String avatar;
    @SerializedName("feedback")
    private String feedback;
    @SerializedName("point")
    private int point;
    @SerializedName("createdOn")
    private String createdOn;

    public void setId(int id) {
        this.id = id;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public int getId() {
        return id;
    }

    public String getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getFeedback() {
        return feedback;
    }

    public int getPoint() {
        return point;
    }

    public String getCreatedOn() {
        return createdOn;
    }
}
