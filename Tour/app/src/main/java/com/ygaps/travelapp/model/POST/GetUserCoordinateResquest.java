package com.ygaps.travelapp.model.POST;

import com.google.gson.annotations.SerializedName;

public class GetUserCoordinateResquest {
    @SerializedName("userId")
    private String userId;
    @SerializedName("tourId")
    private String tourId;
    @SerializedName("lat")
    private double lat;
    @SerializedName("long")
    private double lon;

    public GetUserCoordinateResquest(String userId, String tourId, double lat, double lon) {
        this.userId = userId;
        this.tourId = tourId;
        this.lat = lat;
        this.lon = lon;
    }

    public String getUserId() {
        return userId;
    }

    public String getTourId() {
        return tourId;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setTourId(String tourId) {
        this.tourId = tourId;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }
}
