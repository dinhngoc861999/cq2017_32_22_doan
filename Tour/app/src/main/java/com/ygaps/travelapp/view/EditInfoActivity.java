package com.ygaps.travelapp.view;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.ygaps.travelapp.R;
import com.ygaps.travelapp.model.POST.EditUserInfoRequest;
import com.ygaps.travelapp.model.POST.EditUserInfoResponse;
import com.ygaps.travelapp.network.MyAPIClient;
import com.ygaps.travelapp.network.UserServices;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditInfoActivity extends AppCompatActivity {

    UserServices userServices;

    private EditText edtName;
    private EditText edtEmail;
    private EditText edtPhone;
    private EditText edtGender;
    private EditText edtdob;
    private Button btnSave;
    private String token;
    private int UserGender;
    Date date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_info);

        widgets();
        setDataByBundle();
        token=loadToken();

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditUserInfoRequest editUserInfoRequest=new EditUserInfoRequest();
                editUserInfoRequest.setFullName(edtName.getText().toString());
                editUserInfoRequest.setEmail(edtEmail.getText().toString());
                editUserInfoRequest.setPhone(edtPhone.getText().toString());
                String temp=edtGender.getText().toString();
                Log.e("gender", temp);
                if(temp.equals("Nam"))
                {
                    UserGender=1;
                    Log.e("gender", 1+"");
                }else{
                    UserGender=0;
                    Log.e("gender",0+"");
                }
                editUserInfoRequest.setGender(UserGender);

                String dates=edtdob.getText().toString();
                SimpleDateFormat formatter1=new SimpleDateFormat("yyyy-MM-dd");
                try {
                    date=formatter1.parse(dates);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                editUserInfoRequest.setDob(date);
                userServices= MyAPIClient.getInstance().getAdapter().create(UserServices.class);
                Call<EditUserInfoResponse> call=userServices.editUserInfoResponse(token,editUserInfoRequest);
                call.enqueue(new Callback<EditUserInfoResponse>() {
                    @Override
                    public void onResponse(Call<EditUserInfoResponse> call, Response<EditUserInfoResponse> response) {
                        if(response.code()==200) {
                            // Toast.makeText(TourDetailActivity.this, tourId,Toast.LENGTH_SHORT).show();
                            Log.e("ahihi","400");
                            returnMainActivity(edtName.getText().toString(),edtEmail.getText().toString(),edtPhone.getText().toString(), UserGender, edtdob.getText().toString());
                        }
                        else{
                            //todo
                            Log.e("error", response.message());
                            Log.e("error", response.errorBody().toString());
                            Log.e("400","400");
                        }
                    }

                    @Override
                    public void onFailure(Call<EditUserInfoResponse> call, Throwable t) {

                    }
                });

            }
        });
    }

    private  String loadToken()
    {
        SharedPreferences sharedPreferences=this.getSharedPreferences(getString(R.string.SHARED_PREFS), Context.MODE_PRIVATE);
        return sharedPreferences.getString(getString(R.string.save_token),"");
    }
    public void widgets()
    {
        edtName=(EditText) findViewById(R.id.editTextUserName);
        edtEmail=(EditText) findViewById(R.id.editTextUserEmail);
        edtPhone=(EditText) findViewById(R.id.editTextUserPhone);
        edtGender=(EditText) findViewById(R.id.editTextGender);
        edtdob=(EditText) findViewById(R.id.editTextDob);
        btnSave=(Button) findViewById(R.id.buttonUserSave);
    }

    public void setDataByBundle()
    {
        Intent intent=getIntent();
        Bundle bundle=intent.getExtras();
        if (bundle != null) {

            String Username = bundle.getString("UserName", "");
            String UserEmail = bundle.getString("UserEmail", "");
            String UserPhone = bundle.getString("UserPhone", "");
            UserGender = bundle.getInt("UserGender", 0);
            String Userdob = bundle.getString("Userdob", "");

            edtName.setText(Username);
            Log.e("name", edtName.getText().toString());
            edtEmail.setText(UserEmail);
            Log.e("Email", edtEmail.getText().toString());
            edtPhone.setText(UserPhone);
            Log.e("Phone", edtPhone.getText().toString());
            if(UserGender==1){
                edtGender.setText("Nam");
                Log.e("gender", edtGender.getText().toString());}
            else if(UserGender==0)
            { edtGender.setText("Nữ");
                Log.e("gender", edtGender.getText().toString());}
            //sửa cái này

            String dob="Birthday: "+Userdob;
            Log.e("123",dob+" "+dob.length());
            if(dob.length()>14)
                edtdob.setText(Userdob.substring(0,10));


            //edtdob.setText(Userdob.substring(0,10));
            Log.e("dob",Userdob);
           }
    }

    public  void  returnMainActivity(String name, String email, String phone, int gender, String dob ){
        Intent intent = new Intent();
        Bundle bundle= new Bundle();
        bundle.putString("UserName", name);
        bundle.putString("UserEmail", email);
        bundle.putString("UserPhone", phone);
        bundle.putInt("UserGender", gender);
        bundle.putString("Userdob", dob);
        intent.putExtras(bundle);
        setResult(RESULT_OK,intent);
        finish();
    }
}
