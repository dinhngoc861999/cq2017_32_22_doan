package com.ygaps.travelapp.model;

import com.google.gson.annotations.SerializedName;

public class ReviewofTourResponse {
    @SerializedName("id")
    private String id;
    @SerializedName("name")
    private String name;
    @SerializedName("avatar")
    private String avatar;
    @SerializedName("review")
    private String review;
    @SerializedName("point")
    private int point;
    @SerializedName("createdOn")
    private String createdOn;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getReview() {
        return review;
    }

    public int getPoint() {
        return point;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }
}
