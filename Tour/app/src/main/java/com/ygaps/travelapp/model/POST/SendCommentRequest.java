package com.ygaps.travelapp.model.POST;

import com.google.gson.annotations.SerializedName;

public class SendCommentRequest {

    @SerializedName("tourId")
    private String tourId;
    @SerializedName("userId")
    private String userId;
    @SerializedName("comment")
    private String comment;

    public String getTourId() {
        return tourId;
    }

    public void setTourId(String tourId) {
        this.tourId = tourId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
