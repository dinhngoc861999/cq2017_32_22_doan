package com.ygaps.travelapp.model;

import com.google.gson.annotations.SerializedName;

public class TourInvitationOfAUserResponse {
    @SerializedName("id")
    private int id;
    @SerializedName("status")
    private int status;
    @SerializedName("hostID")
    private int hostID;
    @SerializedName("hostName")
    private String hostName;
    @SerializedName("hostPhone")
    private String hostPhone;
    @SerializedName("hostEmail")
    private String hostEmail;
    @SerializedName("hostAvatar")
    private String hostAvatar;
    @SerializedName("name")
    private String name;
    @SerializedName("minCost")
    private String minCost;
    @SerializedName("maxCost")
    private  String maxCost;
    @SerializedName("startDate")
    private String startDate;
    @SerializedName("endDate")
    private String endDate;
    @SerializedName("adults")
    private  int adults;
    @SerializedName("childs")
    private  int childs;
    @SerializedName("isPrivate")
    private boolean isPrivate;
    @SerializedName("avatar")
    private String avatar;
    @SerializedName("createOn")
    private String createOn;
}
