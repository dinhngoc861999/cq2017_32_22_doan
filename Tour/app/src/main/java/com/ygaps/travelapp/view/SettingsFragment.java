package com.ygaps.travelapp.view;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.Fragment;


import com.ygaps.travelapp.R;
import com.ygaps.travelapp.model.GET.UserInfoResponse;
import com.ygaps.travelapp.network.CommunicationInterface;
import com.ygaps.travelapp.network.MyAPIClient;
import com.ygaps.travelapp.network.UserServices;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class  SettingsFragment extends Fragment {
    CommunicationInterface communicationInterface;

    private String token;
    private UserServices userServices;
    private TextView txtUserName;
    private TextView txtUserEmail;
    private TextView txtUserPhone;
    private TextView txtUserdob;
    private TextView txtUserGender;
    private Button btnEdit;
    private String UserName;
    private String UserEmail;
    private String UserPhone;
    private String Userdob;
    private int gender;

    public SettingsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        communicationInterface= (CommunicationInterface) getActivity();
        Log.e("oncreate","12");
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_settings, container, false);
        token=loadToken();
        Log.e("oncreate","1");

        txtUserName = (TextView)view.findViewById(R.id.textViewUserName);
        txtUserEmail = (TextView) view.findViewById(R.id.textViewEmail);
        txtUserPhone = (TextView) view.findViewById(R.id.textViewPhone);
        txtUserdob = (TextView) view.findViewById(R.id.textViewDOB);
        txtUserGender=(TextView) view.findViewById(R.id.textViewGender);
        btnEdit=(Button) view.findViewById(R.id.buttonUserEdit);

        event();

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            communicationInterface.UserInfo(UserName,UserEmail,
                    UserPhone,gender,Userdob);
            }
        });
        return view;
    }
    private void event()
    {
        userServices= MyAPIClient.getInstance().getAdapter().create(UserServices.class);
        Call<UserInfoResponse> call=userServices.userInfoResponse(token);

        call.enqueue(new Callback<UserInfoResponse>() {
            @Override
            public void onResponse(Call<UserInfoResponse> call, Response<UserInfoResponse> response) {
                if(response.code()==200) {
                    UserName=response.body().getFullName();
                    UserEmail=response.body().getEmail();
                    UserPhone=response.body().getPhone();
                    Userdob=response.body().getDob();
                    gender=response.body().getGender();




                    if(UserName.equals("null")){
                        txtUserName.setText("Name: No Name ");
                    }
                    else
                        txtUserName.setText("Name: "+UserName);

                    if(UserEmail.equals("null")){
                        txtUserEmail.setText("Email: no email ");
                    }
                    else
                        txtUserEmail.setText("Email: "+UserEmail);

                    if(UserPhone.equals("null")){
                        txtUserPhone.setText("Phone: no phone");
                    }
                    else
                        txtUserPhone.setText("Phone: "+UserPhone);

                    String dob="Birthday: "+Userdob;
                    Log.e("123",dob+" "+dob.length());
                    if(dob.length()>14)
                        txtUserdob.setText("Birthday: "+Userdob.substring(0,10));
                    else
                        txtUserdob.setText("Birthday: "+Userdob);

                    if(gender==1)
                    {
                        txtUserGender.setText("Sex: Nam");
                    }else if(gender==0)
                    {
                        txtUserGender.setText("Sex: Nữ");
                    }
                }
                else{
                    //todo
                    Log.e("400","400");
                }
            }

            @Override
            public void onFailure(Call<UserInfoResponse> call, Throwable t) {

            }
        });
    }

    private  String loadToken()
    {
        SharedPreferences sharedPreferences=this.getActivity().getSharedPreferences(getString(R.string.SHARED_PREFS), Context.MODE_PRIVATE);
        return sharedPreferences.getString(getString(R.string.save_token),"");
    }
}
