package com.ygaps.travelapp.model.POST;

import com.google.gson.annotations.SerializedName;

public class RegisterFirebaseRequest {
    @SerializedName("fcmToken")
    private String fcmToken;
    @SerializedName("deviceId")
    private String diviceId;
    @SerializedName("platform")
    private int platform;
    @SerializedName("appVersion")
    private String appVersion;

    public RegisterFirebaseRequest(String fcmToken, String diviceId, int platform, String appVersion) {
        this.fcmToken = fcmToken;
        this.diviceId = diviceId;
        this.platform = platform;
        this.appVersion = appVersion;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public void setDiviceId(String diviceId) {
        this.diviceId = diviceId;
    }

    public void setPlatform(int platform) {
        this.platform = platform;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public String getDiviceId() {
        return diviceId;
    }

    public int getPlatform() {
        return platform;
    }

    public String getAppVersion() {
        return appVersion;
    }
}
