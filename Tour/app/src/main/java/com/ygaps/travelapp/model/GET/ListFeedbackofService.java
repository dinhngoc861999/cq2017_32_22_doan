package com.ygaps.travelapp.model.GET;

import com.google.gson.annotations.SerializedName;
import com.ygaps.travelapp.model.FeedbackofServiceRespone;

import java.util.List;

public class ListFeedbackofService {
    @SerializedName("feedbackList")
    private List<FeedbackofServiceRespone> feedbackofServiceResponeList;

    public List<FeedbackofServiceRespone> getFeedbackofServiceResponeList() {
        return feedbackofServiceResponeList;
    }

    public void setFeedbackofServiceResponeList(List<FeedbackofServiceRespone> feedbackofServiceResponeList) {
        this.feedbackofServiceResponeList = feedbackofServiceResponeList;
    }
}
