package com.ygaps.travelapp.model;

import com.google.gson.annotations.SerializedName;

public class DestinationResponse {
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("address")
    private String address;
    @SerializedName("provinceId")
    private int provinceId;
    @SerializedName("lat")
    private double lat;
    @SerializedName("long")
    private double longt; // actually long in respon// cannot name this variable is long( conflict with type(long))
    @SerializedName("minCost")
    private int minCost;
    @SerializedName("maxCost")
    private int maxCost;
    @SerializedName("avatar")
    private String avatar;
    @SerializedName("serviceTypeId")
    private int serviceTypeId;
    @SerializedName("landingTimesOfUser")
    private int landingTimesOfUser;

    public void setLandingTimesOfUser(int landingTimesOfUser) {
        this.landingTimesOfUser = landingTimesOfUser;
    }

    public int getProvinceId() {
        return provinceId;
    }

    public int getLandingTimesOfUser() {
        return landingTimesOfUser;
    }

    public void setId(int id) { this.id = id; }
    public void setName(String name) {
        this.name = name;
    }
    public void setAddress(String adr) {
        this.address = adr;
    }
    public void setProvinceId(int id) {this.provinceId= id; }
    public void setLat(double  lat) {this.lat= lat; }
    public void setLongt(double  longt) {this.longt= longt; }
    public void setMinCost(int minCost) {
        this.minCost = minCost;
    }
    public void setMaxCost(int maxCost) {
        this.maxCost = maxCost;
    }
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
    public void setServiceTypeId(int id) {this.serviceTypeId= id; }





    public int getId(){return this.id;}
    public String getName() {
       return this.name;
    }
    public String getAddress() {
        return this.address ;
    }
    public int getProvinceId(int id) {return this.provinceId; }
    public double getLat() {return this.lat; }
    public double getLongt() {return this.longt; }
    public int getMinCost() {
        return this.minCost ;
    }
    public int getMaxCost() {
        return this.maxCost;
    }
    public String getAvatar() {
        return this.avatar;
    }
    public int getServiceTypeId() {return this.serviceTypeId; }




}
