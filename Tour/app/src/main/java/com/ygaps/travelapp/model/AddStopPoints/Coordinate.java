package com.ygaps.travelapp.model.AddStopPoints;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;


public class Coordinate {
    @SerializedName("lat")
    private double lat;
    @SerializedName("long")
    private double longt;

    public Coordinate(LatLng point) {
        this.lat = point.latitude;
        this.longt=point.longitude;
    }

    public double getLat() {
        return lat;
    }

    public double getLongt() {
        return longt;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLongt(double longt) {
        this.longt = longt;
    }
}
