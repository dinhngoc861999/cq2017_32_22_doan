package com.ygaps.travelapp.model.GET;

import com.google.gson.annotations.SerializedName;
import com.ygaps.travelapp.model.PointStar;

import java.util.List;

public class GetPointStatisticOfReviewTourResponse {
    @SerializedName("pointStats")
    private List<PointStar> pointStars;

    public List<PointStar> getPointStars() {
        return pointStars;
    }

    public void setPointStars(List<PointStar> pointStars) {
        this.pointStars = pointStars;
    }
}

