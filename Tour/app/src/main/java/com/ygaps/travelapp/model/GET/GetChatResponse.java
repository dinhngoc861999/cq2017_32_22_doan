package com.ygaps.travelapp.model.GET;

import com.google.gson.annotations.SerializedName;
import com.ygaps.travelapp.model.ChatResponse;

import java.util.List;

public class GetChatResponse {
    @SerializedName("notiList")
    private List<ChatResponse> chatResponses;

    public List<ChatResponse> getChatResponses() {
        return chatResponses;
    }

    public void setChatResponses(List<ChatResponse> chatResponses) {
        this.chatResponses = chatResponses;
    }
}
