package com.ygaps.travelapp.services;


import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.ygaps.travelapp.model.GetCoordinate;
import com.ygaps.travelapp.model.POST.GetUserCoordinateResponse;
import com.ygaps.travelapp.model.POST.GetUserCoordinateResquest;
import com.ygaps.travelapp.network.MyAPIClient;
import com.ygaps.travelapp.network.UserServices;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Header;

public class SendLongLat extends Service{
    private FusedLocationProviderClient fusedLocationClient;
    private  String token;
    private double lon;
    private double lat;
    private String tourId;
    private String userId;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        token=intent.getStringExtra("token123");
        userId=intent.getStringExtra("userId123");
        tourId=intent.getStringExtra("tourId123");
        Handler handler=new Handler();
        doTheAutoRefresh(handler);
        return super.onStartCommand(intent, flags, startId);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    private void doTheAutoRefresh(final Handler handler) {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                UserServices userServices=MyAPIClient.getInstance().getAdapter().create(UserServices.class);
                GetLocal();
                GetUserCoordinateResquest getUserCoordinateResquest=new GetUserCoordinateResquest(userId,tourId,lon,lat);
                //call server here

                Log.e("token",token);
                Log.e("userId",userId);
                Log.e("tourId",tourId);
                Log.e("Long",lon+"");
                Log.e("Lat",lat+"");

                Call<List<GetCoordinate>> call=userServices.getCoordinate(token,getUserCoordinateResquest);
                call.enqueue(new Callback<List<GetCoordinate>>() {
                    @Override
                    public void onResponse(Call<List<GetCoordinate>> call, Response<List<GetCoordinate>> response) {
                        if(response.code()==200)
                        {
                            Log.e("kq:",response.body().get(0).getId()+"");
                            Intent intentDataForMyClient = new Intent("matos.action.GOSERVICE3");
                            intentDataForMyClient.putExtra("service3Data", token);
                            sendBroadcast(intentDataForMyClient);
                        }
                        else
                        {
                            Log.e("400","get coor");
                        }
                    }

                    @Override
                    public void onFailure(Call<List<GetCoordinate>> call, Throwable t) {
                        Log.e("400","get coor1");
                    }
                });
                doTheAutoRefresh(handler);
            }
        }, 10000);
    }

    public void GetLocal()
    {
//        lon=10.123123213;
//        lat=100.1232212;
//        fusedLocationClient.getLastLocation()
//                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
//                    @Override
//                    public void onSuccess(Location location) {
//                      s  // Got last known location. In some rare situations this can be null.
//                        if (location != null) {
//                            // Logic to handle location object
//                        }
//                    }
//                });
        fusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    lon = location.getLongitude();
                    lat = location.getLatitude();
                }
            }
        });
    }
}

