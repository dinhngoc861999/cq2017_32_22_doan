package com.ygaps.travelapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ygaps.travelapp.R;
import com.ygaps.travelapp.model.TourResponseHistory;

import java.util.List;

public class TourHistoryAdapter extends BaseAdapter {
    Context myContext;
    int myLayout;
    List<TourResponseHistory> arrayTour;

    public TourHistoryAdapter(Context context, int layout, List<TourResponseHistory> TourList){
        myContext=context;
        myLayout=layout;
        arrayTour=TourList;
    }

    @Override
    public int getCount() {
        return arrayTour.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater=(LayoutInflater)myContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        convertView=inflater.inflate(myLayout,null);

        //Anh xa va gan gia tri
        TextView txtNameTour = (TextView) convertView.findViewById(R.id.textNameTour);
        txtNameTour.setText(arrayTour.get(position).getName());

        return convertView;
    }
}
