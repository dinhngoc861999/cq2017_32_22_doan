package com.ygaps.travelapp.model;

import com.google.gson.annotations.SerializedName;

public class InviteNotiResponse {
    @SerializedName("id")
    private int id;
    @SerializedName("hostId")
    private String hostIdl;
    @SerializedName("hostName")
    private String hostName;
    @SerializedName("hostPhone")
    private String hostPhone;
    @SerializedName("hostEmail")
    private String hostEmail;
    @SerializedName("hostAvatar")
    private String hostAvatar;
    @SerializedName("status")
    private int status;
    @SerializedName("name")
    private String name;
    @SerializedName("minCost")
    private String minCost;
    @SerializedName("maxCost")
    private  String maxCost;
    @SerializedName("startDate")
    private String startDate;
    @SerializedName("endDate")
    private String endDate;
    @SerializedName("adults")
    private  int adults;
    @SerializedName("childs")
    private  int childs;
    @SerializedName("avatar")
    private String avatar;
    @SerializedName("createdOn")
    private  String createdOn;

    public void setId(int id) {
        this.id = id;
    }

    public void setHostIdl(String hostIdl) {
        this.hostIdl = hostIdl;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public void setHostPhone(String hostPhone) {
        this.hostPhone = hostPhone;
    }

    public void setHostEmail(String hostEmail) {
        this.hostEmail = hostEmail;
    }

    public void setHostAvatar(String hostAvatar) {
        this.hostAvatar = hostAvatar;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMinCost(String minCost) {
        this.minCost = minCost;
    }

    public void setMaxCost(String maxCost) {
        this.maxCost = maxCost;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public void setAdults(int adults) {
        this.adults = adults;
    }

    public void setChilds(int childs) {
        this.childs = childs;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public int getId() {
        return id;
    }

    public String getHostIdl() {
        return hostIdl;
    }

    public String getHostName() {
        return hostName;
    }

    public String getHostPhone() {
        return hostPhone;
    }

    public String getHostEmail() {
        return hostEmail;
    }

    public String getHostAvatar() {
        return hostAvatar;
    }

    public int getStatus() {
        return status;
    }

    public String getName() {
        return name;
    }

    public String getMinCost() {
        return minCost;
    }

    public String getMaxCost() {
        return maxCost;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public int getAdults() {
        return adults;
    }

    public int getChilds() {
        return childs;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getCreatedOn() {
        return createdOn;
    }
}
