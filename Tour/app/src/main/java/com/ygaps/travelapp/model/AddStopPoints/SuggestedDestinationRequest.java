package com.ygaps.travelapp.model.AddStopPoints;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SuggestedDestinationRequest {
    @SerializedName("hasOneCoordinate")
    private boolean hasOneCoordinate;
    @SerializedName("coordList")
    private List<CoordinateSet> coordinateSets;

    public boolean isHasOneCoordinate() {
        return hasOneCoordinate;
    }

    public List<CoordinateSet> getCoordinateSets() {
        return coordinateSets;
    }

    public void setHasOneCoordinate(boolean hasOneCoordinate) {
        this.hasOneCoordinate = hasOneCoordinate;
    }

    public void setCoordinateSets(List<CoordinateSet> coordinateSets) {
        this.coordinateSets = coordinateSets;
    }
}
