package com.ygaps.travelapp.model.POST;

import com.google.gson.annotations.SerializedName;

public class AddStopPointsResponse {
    @SerializedName("message")
    private String message;

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
