package com.ygaps.travelapp.network;

import com.facebook.CallbackManager;
import com.ygaps.travelapp.model.AddStopPoints.CoordinateSet;
import com.ygaps.travelapp.model.AddStopPoints.RegisterFiebaseResponse;
import com.ygaps.travelapp.model.AddStopPoints.SuggestedDestinationRequest;
import com.ygaps.travelapp.model.AddStopPoints.SuggestedDestinationResponse;
import com.ygaps.travelapp.model.GET.GetChatResponse;
import com.ygaps.travelapp.model.GET.GetListInviteTourResponse;
import com.ygaps.travelapp.model.GET.GetPointStatisticOfReviewTourResponse;
import com.ygaps.travelapp.model.GET.GetReviewofTour;
import com.ygaps.travelapp.model.GET.ListFeedbackofService;
import com.ygaps.travelapp.model.GET.ListHistoryTourResponse;
import com.ygaps.travelapp.model.GET.SearchHistoryUserResponse;
import com.ygaps.travelapp.model.GET.SearchUsersAccordingKeyWordResponse;
import com.ygaps.travelapp.model.GET.TourInfoResponse;
import com.ygaps.travelapp.model.GetCoordinate;
import com.ygaps.travelapp.model.POST.AddStopPointsRequest;
import com.ygaps.travelapp.model.POST.AddStopPointsResponse;
import com.ygaps.travelapp.model.POST.CreateTourRequest;
import com.ygaps.travelapp.model.POST.CreateTourResponse;
import com.ygaps.travelapp.model.POST.DeleteTourRequest;
import com.ygaps.travelapp.model.POST.DeleteTourResponse;
import com.ygaps.travelapp.model.POST.EditUserInfoRequest;
import com.ygaps.travelapp.model.POST.EditUserInfoResponse;
import com.ygaps.travelapp.model.GET.GetCommentResponse;
import com.ygaps.travelapp.model.GET.ListTourInvitationOfAUserResponse;
import com.ygaps.travelapp.model.GET.ListTourResponse;
import com.ygaps.travelapp.model.POST.GetUserCoordinateResponse;
import com.ygaps.travelapp.model.POST.GetUserCoordinateResquest;
import com.ygaps.travelapp.model.POST.InviteMemberRequest;
import com.ygaps.travelapp.model.POST.InviteMemberResponse;
import com.ygaps.travelapp.model.POST.LoginFBRequest;
import com.ygaps.travelapp.model.POST.LoginRequest;
import com.ygaps.travelapp.model.POST.LoginResponse;
import com.ygaps.travelapp.model.POST.RegisterFirebaseRequest;
import com.ygaps.travelapp.model.POST.RegisterRequest;
import com.ygaps.travelapp.model.POST.RegisterResponse;
import com.ygaps.travelapp.model.POST.LoginFBResponse;
import com.ygaps.travelapp.model.GET.SearchDestinationResponse;
import com.ygaps.travelapp.model.POST.ResetPasswordRequest;
import com.ygaps.travelapp.model.POST.ResetPasswordResponse;
import com.ygaps.travelapp.model.POST.ResponseInviteResponse;
import com.ygaps.travelapp.model.POST.ResponseInviteResquest;
import com.ygaps.travelapp.model.POST.SendChatRequest;
import com.ygaps.travelapp.model.POST.SendChatResponse;
import com.ygaps.travelapp.model.POST.SendCommentRequest;
import com.ygaps.travelapp.model.POST.SendCommentResponse;
import com.ygaps.travelapp.model.GET.UserInfoResponse;
import com.ygaps.travelapp.model.POST.SendFeedbackRequest;
import com.ygaps.travelapp.model.POST.SendOTPRequest;
import com.ygaps.travelapp.model.POST.SendOTPResponse;
import com.ygaps.travelapp.model.POST.SendReviewTourRequest;
import com.ygaps.travelapp.model.POST.SendReviewTourResponse;
import com.ygaps.travelapp.model.POST.ServiceFeedbackRequeste;
import com.ygaps.travelapp.model.POST.UpdateTourResquest;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface UserServices {
    @POST("/user/register")
    Call<RegisterResponse> register(@Body RegisterRequest request);
    @POST("/user/login")
    Call<LoginResponse> login(@Body LoginRequest request);
    @GET("/tour/list")
    Call<ListTourResponse> listtour(@Header("Authorization") String Authorization,
                                    @Query("pageNum") int pageNum,
                                    @Query("rowPerPage") int rowPerPage,
                                    @Query("isDesc") boolean isDesc);
    @POST("/tour/create")
    Call<CreateTourResponse> createtour(@Header("Authorization") String Authorization,
                                        @Body CreateTourRequest createTourRequest);
    @GET("/tour/search/service")
    Call<SearchDestinationResponse> Searchdestination(@Header("Authorization") String Authorization,
                                                      @Query("searchKey") String searchKey,
                                                      @Query("pageIndex") int pageIndex,
                                                      @Query("pageSize") int pageSize);
    @POST("/user/login/by-facebook")
    Call<LoginFBResponse> loginfb(@Body LoginFBRequest loginFBRequest);
    @POST("/tour/set-stop-points")
    Call<AddStopPointsResponse>  addstop(@Header("Authorization") String Authorization,
                                         @Body AddStopPointsRequest addStopPointsRequest);
    @POST("/tour/current-users-coordinate")
    Call<List<GetCoordinate>> getCoordinate(@Header("Authorization") String Authorization,
                                            @Body GetUserCoordinateResquest getUserCoordinateResquest);

    @POST("/tour/suggested-destination-list")
    Call<SuggestedDestinationResponse> SUGGESTED_DESTINATION_RESPONSE_CALL(@Header("Authorization") String Authorization,
                                                                           @Body SuggestedDestinationRequest suggestedDestinationRequest);
    @GET("/tour/remove-stop-point")
    Call<SendReviewTourResponse> DELETE_STOP_POINT_CALL (@Header("Authorization") String Authorization,
                                                         @Query("stopPointId") int stoppointId);

    @GET("/tour/get/invitation")
    Call<ListTourInvitationOfAUserResponse> listtourInvitationOfAUserResponse(@Header("Authorization") String Authorization,
                                                                              @Query("pageIndex") int pageIndex,
                                                                              @Query("pageSize") int pageSize);
    @GET("/tour/history-user")
    Call<ListHistoryTourResponse> HistoryTourRespon(@Header("Authorization") String Authorization,
                                                    @Query("pageIndex") int pageIndex,
                                                    @Query("pageSize") int pageSize);

    @GET("/tour/info")
    Call<TourInfoResponse> TOUR_INFO_RESPONSE_CALL(@Header("Authorization") String Authorization,
                                             @Query("tourId") int tourId);

    @GET("/tour/comment-list")
    Call<GetCommentResponse>  getCommentResponse(@Header("Authorization") String Authorization,
                                         @Query("tourId") String tourId,
                                         @Query("pageIndex") int pageIndex,
                                         @Query("pageSize") int pageSize);

    @POST("/tour/comment")
    Call<SendCommentResponse> sendCommentResponse(@Header("Authorization") String Authorization,
                                                  @Body SendCommentRequest sendCommentRequest);

    @POST("/user/notification/put-token")
    Call<RegisterFiebaseResponse> registerFirebase(@Header("Authorization") String Authorization,
                                                   @Body RegisterFirebaseRequest registerFirebaseRequest);


    @GET("/user/info")
    Call<UserInfoResponse> userInfoResponse(@Header("Authorization") String Authorization);

    @POST("/user/edit-info")
    Call<EditUserInfoResponse> editUserInfoResponse(@Header("Authorization") String Authorization,
                                                    @Body EditUserInfoRequest editUserInfoRequest);
    @POST("/user/request-otp-recovery")
    Call<SendOTPResponse> sendOTP(@Body SendOTPRequest sendOTPRequest);

    @POST("/user/verify-otp-recovery")
    Call<ResetPasswordResponse> resetPassword(@Body ResetPasswordRequest resetPasswordRequest);
    @GET("/tour/notification-list")
    Call<GetChatResponse> getChat(@Header("Authorization") String Authorization,
                                  @Query("tourId") String tourId,
                                  @Query("pageIndex") int pageIndex,
                                  @Query("pageSize") int pageSize);

    @POST("/tour/notification")
    Call<SendChatResponse> sendChat(@Header("Authorization") String Authorization,
                                    @Body SendChatRequest sendChatRequest);

    @GET("/tour/get/review-list")
    Call<GetReviewofTour> getReview(@Header("Authorization") String Authorization,
                                    @Query("tourId") String tourId,
                                    @Query("pageIndex") int pageIndex,
                                    @Query("pageSize") int pageSize);
    @GET("/tour/get/feedback-service")
    Call<ListFeedbackofService> LIST_FEEDBACKOF_SERVICE_CALL (@Header("Authorization") String Authorization,
                                                              @Query("serviceId") int serviceId,
                                                              @Query("pageIndex") int pageIndex,
                                                              @Query("pageSize") int pageSize);

    @POST("/tour/add/review")
    Call<SendReviewTourResponse> sendreview(@Header("Authorization") String Authorization,
                                            @Body SendReviewTourRequest sendReviewTourRequest);
    @POST("/tour/add/feedback-service")
    Call<SendReviewTourResponse> send_feedback_service (@Header("Authorization") String Authorization,
                                                        @Body SendFeedbackRequest sendFeedbackRequest);


    @POST("/tour/update-tour")
    Call<DeleteTourResponse> deletetour(@Header("Authorization") String Authorization,
                                        @Body DeleteTourRequest deleteTourRequest);

    @POST("/tour/update-tour")
    Call<DeleteTourResponse> updatetour(@Header("Authorization") String Authorization,
                                      @Body UpdateTourResquest updateTourResquest);


    @GET("/tour/get/invitation")
    Call<GetListInviteTourResponse> getInvite(@Header("Authorization") String Authorization,
                                              @Query("pageIndex") int pageIndex,
                                              @Query("pageSize") int pageSize);

    @GET("/user/search")
    Call<SearchUsersAccordingKeyWordResponse> searchUsersAccordingKeyWord(@Query("searchKey") String searchKey,
                                                                          @Query("pageIndex") int pageIndex,
                                                                          @Query("pageSize") int pageSize);

    @POST("/tour/add/member")
    Call<InviteMemberResponse> inviteMember(@Header("Authorization") String Authorization,
                                            @Body InviteMemberRequest inviteMemberRequest);

    @POST("/tour/response/invitation")
    Call<ResponseInviteResponse> responseInvite(@Header("Authorization") String Authorization,
                                                @Body ResponseInviteResquest responseInviteResquest);

    @GET("/tour/search-history-user")
    Call<SearchHistoryUserResponse> searchHistoryUserResponse(@Header("Authorization") String Authorization,
                                                              @Query("searchKey") String searchKey,
                                                              @Query("pageIndex") int pageIndex,
                                                              @Query("pageSize") int pageSize);

    @GET("/tour/get/review-point-stats")
    Call<GetPointStatisticOfReviewTourResponse> getPointStatisticOfReviewTourResponse(@Header("Authorization") String Authorization,
                                                                          @Query("tourId") int tourId);

    @GET("/tour/get/feedback-point-stats")
    Call<GetPointStatisticOfReviewTourResponse> getPointStatisticOfSevice(@Header("Authorization") String Authorization,
                                                                                      @Query("serviceId") int serviceId);
}
