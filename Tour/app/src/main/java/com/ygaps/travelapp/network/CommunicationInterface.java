package com.ygaps.travelapp.network;

import com.ygaps.travelapp.model.TourResponse;
import com.ygaps.travelapp.model.GET.UserInfoResponse;
import com.ygaps.travelapp.model.TourResponseHistory;

public interface CommunicationInterface {
    public TourResponse TourContent(
            int id, int status, String name,String minCost, String maxCost,
            String startDate, String endDate, int adults, int childs,
            boolean isPrivate, String avatar);

    public UserInfoResponse UserInfo(
            String name, String email, String phone, int gender, String dob);

    public TourResponseHistory TourHistory(
            int id, int status, String name,String minCost, String maxCost,
            String startDate, String endDate, int adults, int childs,
            boolean isPrivate, String avatar);

}
