package com.ygaps.travelapp.view;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.ygaps.travelapp.R;
import com.ygaps.travelapp.adapter.ChatAdapter;
import com.ygaps.travelapp.adapter.ReviewAdapter;
import com.ygaps.travelapp.model.ChatResponse;
import com.ygaps.travelapp.model.GET.GetChatResponse;
import com.ygaps.travelapp.model.GET.GetPointStatisticOfReviewTourResponse;
import com.ygaps.travelapp.model.GET.GetReviewofTour;
import com.ygaps.travelapp.model.ReviewofTourResponse;
import com.ygaps.travelapp.network.MyAPIClient;
import com.ygaps.travelapp.network.MyTourMainCommunicationInterface;
import com.ygaps.travelapp.network.UserServices;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class RateOfMyTourFragment extends Fragment {

    MyTourMainCommunicationInterface myTourMainCommunicationInterface;
    private ListView listView;
    private ArrayList<ReviewofTourResponse> reviewofTourResponses;
    private UserServices userServices;
    private Button button;
    private String tourId;
    private ReviewAdapter adapter;
    private Handler handler;
    private String token;
    private String userId;
    private TextView textView, txtOne, txtTwo, txtThree, txtFour, txtFive;

    public RateOfMyTourFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        myTourMainCommunicationInterface= (MyTourMainCommunicationInterface) getActivity();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_rate_of_my_tour, container, false);
        Widget(view);
        adapter = new ReviewAdapter(
                getActivity(),
                R.layout.adapter_review,
                reviewofTourResponses
        );
        listView.setAdapter(adapter);
        userServices = MyAPIClient.getInstance().getAdapter().create(UserServices.class);
        Loadreview();
        PointStatisticOfReviewTour();
        //doTheAutoRefresh();
        event();

        return view;
    }

    private void event()
    {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myTourMainCommunicationInterface.TourId(tourId);
            }
        });
    }

    private void doTheAutoRefresh() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Loadreview();
                doTheAutoRefresh();
            }
        }, 10000);
    }

    public void Loadreview() {
        Call<GetReviewofTour> call = userServices.getReview(token, tourId, 1, 100);
        call.enqueue(new Callback<GetReviewofTour>() {
            @Override
            public void onResponse(Call<GetReviewofTour> call, Response<GetReviewofTour> response) {
                if (response.code() == 200) {
                    reviewofTourResponses.clear();
                    for (int i = 0; i < response.body().getReviewofTourResponses().size(); i++) {
                        reviewofTourResponses.add(fRview(response, i));
                    }
                    Collections.reverse(reviewofTourResponses);
                    adapter.notifyDataSetChanged();
                    if(reviewofTourResponses.size()==0)
                        textView.setText("No review in tour");
                    else
                        textView.setText("");
                } else {
                    try {
                        Log.e("GetChat fail", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<GetReviewofTour> call, Throwable t) {

            }
        });
    }

    private void  PointStatisticOfReviewTour(){
        Call<GetPointStatisticOfReviewTourResponse> call = userServices.getPointStatisticOfReviewTourResponse(token,Integer.valueOf(tourId));
        call.enqueue(new Callback<GetPointStatisticOfReviewTourResponse>() {
            @Override
            public void onResponse(Call<GetPointStatisticOfReviewTourResponse> call, Response<GetPointStatisticOfReviewTourResponse> response) {
                if (response.code() == 200) {
                    Log.e("ahihi", "Được rồi nè");
                    txtOne.setText(Integer.toString(response.body().getPointStars().get(0).getTotal()));
                    txtTwo.setText(Integer.toString(response.body().getPointStars().get(1).getTotal()));
                    txtThree.setText(Integer.toString(response.body().getPointStars().get(2).getTotal()));
                    txtFour.setText(Integer.toString(response.body().getPointStars().get(3).getTotal()));
                    txtFive.setText(Integer.toString(response.body().getPointStars().get(4).getTotal()));
                } else {
                    try {
                        Log.e("GetPoint fail", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<GetPointStatisticOfReviewTourResponse> call, Throwable t) {

            }
        });
    }


    public void Widget(View view) {
        textView=(TextView)view.findViewById(R.id.noti_review);
        reviewofTourResponses = new ArrayList<ReviewofTourResponse>();
        handler = new Handler();
        listView = (ListView) view.findViewById(R.id.listview_review);
        button = (Button) view.findViewById(R.id.buttonreview);
        Bundle bundle = getArguments();
        if (bundle != null) {
            tourId = bundle.getInt("id", 0) + "";
            Log.e("tourId", tourId);
        }
        token = loadToken();
        userId = loadUserId() + "";
        txtOne=(TextView) view.findViewById(R.id.textViewNumberOfOne);
        txtTwo=(TextView) view.findViewById(R.id.textViewNumberOfTwo);
        txtThree=(TextView) view.findViewById(R.id.textViewNumberOfThree);
        txtFour=(TextView) view.findViewById(R.id.textViewNumberOfFour);
        txtFive=(TextView) view.findViewById(R.id.textViewNumberOfFive);
    }

    private String loadToken() {
        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences(getString(R.string.SHARED_PREFS), Context.MODE_PRIVATE);
        return sharedPreferences.getString(getString(R.string.save_token), "");
    }

    private int loadUserId() {
        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences(getString(R.string.SHARED_PREFS), Context.MODE_PRIVATE);
        return sharedPreferences.getInt(getString(R.string.save_userId), 0);
    }

    private ReviewofTourResponse fRview(Response<GetReviewofTour> response, int i) {
        ReviewofTourResponse reviewofTourResponse = new ReviewofTourResponse();
        reviewofTourResponse.setId(response.body().getReviewofTourResponses().get(i).getId());
        reviewofTourResponse.setName(response.body().getReviewofTourResponses().get(i).getName());
        reviewofTourResponse.setAvatar(response.body().getReviewofTourResponses().get(i).getAvatar());
        reviewofTourResponse.setPoint(response.body().getReviewofTourResponses().get(i).getPoint());
        reviewofTourResponse.setReview(response.body().getReviewofTourResponses().get(i).getReview());
        reviewofTourResponse.setCreatedOn(response.body().getReviewofTourResponses().get(i).getCreatedOn());
        return reviewofTourResponse;
    }
}
