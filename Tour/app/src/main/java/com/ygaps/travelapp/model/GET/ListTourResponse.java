package com.ygaps.travelapp.model.GET;

import com.google.gson.annotations.SerializedName;
import com.ygaps.travelapp.model.TourResponse;

import java.util.List;

public class ListTourResponse {
    @SerializedName("total")
    private int total;
    @SerializedName("tours")
    private List<TourResponse> tourResponses;

    public int getTotal() {
        return total;
    }

    public List<TourResponse> getTourResponses() {
        return tourResponses;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public void setTourResponses(List<TourResponse> tourResponses) {
        this.tourResponses = tourResponses;
    }
}
