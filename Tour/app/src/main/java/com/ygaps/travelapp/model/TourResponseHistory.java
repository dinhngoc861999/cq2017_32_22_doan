package com.ygaps.travelapp.model;

import com.google.gson.annotations.SerializedName;

public class TourResponseHistory {
    @SerializedName("id")
    private int id;
    @SerializedName("hostID")
    private String HostId;
    @SerializedName("status")
    private int status;
    @SerializedName("name")
    private String name;
    @SerializedName("minCost")
    private String minCost;
    @SerializedName("maxCost")
    private  String maxCost;
    @SerializedName("startDate")
    private String startDate;
    @SerializedName("endDate")
    private String endDate;
    @SerializedName("adults")
    private  int adults;
    @SerializedName("childs")
    private  int childs;
    @SerializedName("isPrivate")
    private boolean isPrivate;
    @SerializedName("avatar")
    private String avatar;
    @SerializedName("isHost")
    private boolean isHost;
    @SerializedName("isKicked")
    private boolean isKicked;

    public int getId() {
        return id;
    }

    public String getHostId() {
        return HostId;
    }

    public int getStatus() {
        return status;
    }

    public String getName() {
        return name;
    }

    public String getMinCost() {
        return minCost;
    }

    public String getMaxCost() {
        return maxCost;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public int getAdults() {
        return adults;
    }

    public int getChilds() {
        return childs;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public String getAvatar() {
        return avatar;
    }

    public boolean isHost() {
        return isHost;
    }

    public boolean isKicked() {
        return isKicked;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setHostId(String hostId) {
        HostId = hostId;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMinCost(String minCost) {
        this.minCost = minCost;
    }

    public void setMaxCost(String maxCost) {
        this.maxCost = maxCost;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public void setAdults(int adults) {
        this.adults = adults;
    }

    public void setChilds(int childs) {
        this.childs = childs;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public void setHost(boolean host) {
        isHost = host;
    }

    public void setKicked(boolean kicked) {
        isKicked = kicked;
    }
}
