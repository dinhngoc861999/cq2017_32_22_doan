package com.ygaps.travelapp.view;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;

import com.ygaps.travelapp.network.CommunicationInterface;
import com.ygaps.travelapp.R;
import com.ygaps.travelapp.adapter.TourAdapter;
import com.ygaps.travelapp.model.GET.ListTourResponse;
import com.ygaps.travelapp.model.TourResponse;
import com.ygaps.travelapp.network.MyAPIClient;
import com.ygaps.travelapp.network.UserServices;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
/**
 * A simple {@link Fragment} subclass.
 */
public class TourFragment extends Fragment {
    CommunicationInterface communicationInterface;

    private ListView lvTour;
    private ArrayList<TourResponse> mangTour;
    private int fresh_count;
    private ImageButton ibtnCreate;
    private String token;
    private  int rowperpage=10;
    private int total;


    public TourFragment() {
        // Required empty public

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        communicationInterface= (CommunicationInterface) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_tour, container, false);
        fresh_count=1;
        lvTour=(ListView) view.findViewById(R.id.listViewTour);
        ibtnCreate=(ImageButton) view.findViewById(R.id.listtourCreate);
        mangTour= new ArrayList<TourResponse>();
        token=loadToken();
        UserServices userServices= MyAPIClient.getInstance().getAdapter().create(UserServices.class);
        Call<ListTourResponse> call= userServices.listtour(token, fresh_count,rowperpage,true);
        final TourAdapter adapter= new TourAdapter(
                getActivity(),
                R.layout.list_tour,
                mangTour
        );

        call.enqueue(new Callback<ListTourResponse>() {
            @Override
            public void onResponse(Call<ListTourResponse> call, Response<ListTourResponse> response) {
                if(response.code()==200) {
                    for(int i=0; i<response.body().getTourResponses().size(); i++)
                    {
                        mangTour.add(fResponse(response,i));
                    }
                    total=response.body().getTotal();
                    Log.e("total: ",total+"");

                    adapter.notifyDataSetChanged();
                    lvTour.setAdapter(adapter);
                }
                else{
                    //todo
                    Log.e("400","400");
                }
                fresh_count++;
            }

            @Override
            public void onFailure(Call<ListTourResponse> call, Throwable t) {
            }
        });

        lvTour.setTag(0);
        lvTour.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, final int totalItemCount) {

                int lastVisibleItem=firstVisibleItem+visibleItemCount;
                int preLast= ((int) view.getTag());
                if (lastVisibleItem==totalItemCount)
                {
                    if (lastVisibleItem!= preLast)
                    {
                        if(totalItemCount+10<total){
                            UserServices userServices1=MyAPIClient.getInstance().getAdapter().create(UserServices.class);
                            Call<ListTourResponse> call= userServices1.listtour(token, fresh_count,rowperpage,true);
                            call.enqueue(new Callback<ListTourResponse>() {
                                @Override
                                public void onResponse(Call<ListTourResponse> call, Response<ListTourResponse> response) {
                                    if(response.code()==200) {
                                        for(int i=0; i<response.body().getTourResponses().size(); i++)
                                        {
                                            mangTour.add(fResponse(response,i));
                                        }
                                        adapter.notifyDataSetChanged();
                                    /*TourAdapter adapter= new TourAdapter(
                                            getActivity(),
                                            R.layout.list_tour,
                                            mangTour
                                    );
                                    lvTour.setAdapter(adapter);*/
                                        total=response.body().getTotal();
                                        fresh_count++;
                                    }
                                    else{
                                        //todo
                                        Log.e("400","400");
                                    }
                                }
                                @Override
                                public void onFailure(Call<ListTourResponse> call, Throwable t) {
                                    //todo
                                    Log.e("400","failure");
                                }
                            });
                            preLast =lastVisibleItem;
                        }
                    }

                }
                view.setTag(preLast);
            }
        });


        ibtnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(),CreateTourActivity.class);
                startActivity(intent);
            }
        });


        lvTour.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                communicationInterface.TourContent(mangTour.get(position).getId(), mangTour.get(position).getStatus(),
                        mangTour.get(position).getName(),mangTour.get(position).getMinCost(),mangTour.get(position).getMaxCost(),
                        mangTour.get(position).getStartDate(), mangTour.get(position).getEndDate(),mangTour.get(position).getAdults(),
                        mangTour.get(position).getChilds(),mangTour.get(position).isPrivate(), mangTour.get(position).getAvatar());
            }
        });

        return view;

    }

    private  String loadToken()
    {
        SharedPreferences sharedPreferences=this.getActivity().getSharedPreferences(getString(R.string.SHARED_PREFS), Context.MODE_PRIVATE);
        return sharedPreferences.getString(getString(R.string.save_token),"");
    }

    private TourResponse fResponse(Response<ListTourResponse> response,int i)
    {
        TourResponse tResponse;
        tResponse= new TourResponse();
        tResponse.setId(response.body().getTourResponses().get(i).getId());
        tResponse.setStatus(response.body().getTourResponses().get(i).getStatus());
        tResponse.setName(response.body().getTourResponses().get(i).getName());
        tResponse.setMinCost(response.body().getTourResponses().get(i).getMinCost());
        tResponse.setMaxCost(response.body().getTourResponses().get(i).getMaxCost());
        tResponse.setStartDate(response.body().getTourResponses().get(i).getStartDate());
        tResponse.setEndDate(response.body().getTourResponses().get(i).getEndDate());
        tResponse.setAdults(response.body().getTourResponses().get(i).getAdults());
        tResponse.setChilds(response.body().getTourResponses().get(i).getChilds());
        return tResponse;
    }

    public  class MyHandler extends Handler{
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case 0:
            }

        }
    }
}
