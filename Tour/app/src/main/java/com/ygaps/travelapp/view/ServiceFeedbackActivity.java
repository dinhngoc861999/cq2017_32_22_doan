package com.ygaps.travelapp.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.ygaps.travelapp.R;
import com.ygaps.travelapp.adapter.FeedbackAdapter;
import com.ygaps.travelapp.model.FeedbackofServiceRespone;
import com.ygaps.travelapp.model.GET.GetPointStatisticOfReviewTourResponse;
import com.ygaps.travelapp.model.GET.ListFeedbackofService;
import com.ygaps.travelapp.network.MyAPIClient;
import com.ygaps.travelapp.network.UserServices;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServiceFeedbackActivity extends AppCompatActivity {
    Context context=this;
    private ListView listView;
    private ArrayList<FeedbackofServiceRespone> feedbackofServiceRespones;
    private UserServices userServices;
    private Button button;
    private int serviceId;
    private FeedbackAdapter adapter;
    private Handler handler;
    private String token;
    private String userId;
    private TextView textView,txtOne, txtTwo, txtThree, txtFour, txtFive;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_feedback);

        Widget();
        adapter = new FeedbackAdapter(
                this,
                R.layout.adapter_review,
                feedbackofServiceRespones
        );
        listView.setAdapter(adapter);
        userServices = MyAPIClient.getInstance().getAdapter().create(UserServices.class);
        Loadreview();
        doTheAutoRefresh();
        PointStatisticOfReviewTour();
        event();

    }

    public void Widget() {
        textView=(TextView)findViewById(R.id.noti_review);
        feedbackofServiceRespones = new ArrayList<FeedbackofServiceRespone>();
        handler = new Handler();
        listView = (ListView) findViewById(R.id.listview_review);
        button = (Button) findViewById(R.id.buttonreview);

        token = loadToken();
        userId = loadUserId() + "";

        Intent intent = getIntent();
        serviceId = intent.getIntExtra("serviceId", -1);
        Log.d("seviceId in list fk",serviceId+"");

        txtOne=(TextView) findViewById(R.id.textViewNumberOfOne);
        txtTwo=(TextView) findViewById(R.id.textViewNumberOfTwo);
        txtThree=(TextView) findViewById(R.id.textViewNumberOfThree);
        txtFour=(TextView) findViewById(R.id.textViewNumberOfFour);
        txtFive=(TextView) findViewById(R.id.textViewNumberOfFive);
    }
    private void event()
    {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SendFeedbackServiceActivity.class);
                intent.putExtra("serviceId",serviceId);
                startActivityForResult(intent,1);
            }
        });
    }

    private void doTheAutoRefresh() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Loadreview();
                doTheAutoRefresh();
            }
        }, 10000);
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent responintent)
    {
        super.onActivityResult(requestCode, resultCode, responintent);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==1) {
           Loadreview();
        }
    }

    public void Loadreview() {
        Call<ListFeedbackofService> call = userServices.LIST_FEEDBACKOF_SERVICE_CALL(token,serviceId, 1, 100);
        call.enqueue(new Callback<ListFeedbackofService>() {
            @Override
            public void onResponse(Call<ListFeedbackofService> call, Response<ListFeedbackofService> response) {
                if (response.code() == 200) {
                    feedbackofServiceRespones.clear();
                    for (int i = 0; i < response.body().getFeedbackofServiceResponeList().size(); i++) {
                        feedbackofServiceRespones.add(fRview(response, i));
                    }
                    Collections.reverse(feedbackofServiceRespones);
                    adapter.notifyDataSetChanged();
                    if(feedbackofServiceRespones.size()==0)
                        textView.setText("No feedback in service");
                    else
                        textView.setText("");
                } else {
                    try {
                        Log.e("GetChat fail", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ListFeedbackofService> call, Throwable t) {

            }
        });
    }

    private void  PointStatisticOfReviewTour(){
        Call<GetPointStatisticOfReviewTourResponse> call = userServices.getPointStatisticOfSevice(token,serviceId);
        call.enqueue(new Callback<GetPointStatisticOfReviewTourResponse>() {
            @Override
            public void onResponse(Call<GetPointStatisticOfReviewTourResponse> call, Response<GetPointStatisticOfReviewTourResponse> response) {
                if (response.code() == 200) {
                    txtOne.setText(Integer.toString(response.body().getPointStars().get(0).getTotal()));
                    txtTwo.setText(Integer.toString(response.body().getPointStars().get(1).getTotal()));
                    txtThree.setText(Integer.toString(response.body().getPointStars().get(2).getTotal()));
                    txtFour.setText(Integer.toString(response.body().getPointStars().get(3).getTotal()));
                    txtFive.setText(Integer.toString(response.body().getPointStars().get(4).getTotal()));
                } else {
                    try {
                        Log.e("GetPoint fail", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<GetPointStatisticOfReviewTourResponse> call, Throwable t) {

            }
        });
    }


    private String loadToken() {
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.SHARED_PREFS), Context.MODE_PRIVATE);
        return sharedPreferences.getString(getString(R.string.save_token), "");
    }

    private int loadUserId() {
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.SHARED_PREFS), Context.MODE_PRIVATE);
        return sharedPreferences.getInt(getString(R.string.save_userId), 0);
    }

    private FeedbackofServiceRespone fRview(Response<ListFeedbackofService> response, int i) {
        FeedbackofServiceRespone feedbackofServiceRespone = new FeedbackofServiceRespone();
        feedbackofServiceRespone.setId(response.body().getFeedbackofServiceResponeList().get(i).getId());
        feedbackofServiceRespone.setUserId(response.body().getFeedbackofServiceResponeList().get(i).getUserId());
        feedbackofServiceRespone.setName(response.body().getFeedbackofServiceResponeList().get(i).getName());
        feedbackofServiceRespone.setPhone(response.body().getFeedbackofServiceResponeList().get(i).getPhone());
        feedbackofServiceRespone.setAvatar(response.body().getFeedbackofServiceResponeList().get(i).getAvatar());
        feedbackofServiceRespone.setPoint(response.body().getFeedbackofServiceResponeList().get(i).getPoint());
        feedbackofServiceRespone.setFeedback(response.body().getFeedbackofServiceResponeList().get(i).getFeedback());
        feedbackofServiceRespone.setCreatedOn(response.body().getFeedbackofServiceResponeList().get(i).getCreatedOn());
        return feedbackofServiceRespone;
    }

}
