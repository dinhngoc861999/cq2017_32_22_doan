package com.ygaps.travelapp.view;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
//
//import com.example.tour.view.Notficati;
//import com.example.tour.view.SettingsFragment;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.ygaps.travelapp.model.AddStopPoints.RegisterFiebaseResponse;
import com.ygaps.travelapp.model.POST.RegisterFirebaseRequest;
import com.ygaps.travelapp.model.GET.UserInfoResponse;
import com.ygaps.travelapp.model.TourResponseHistory;
import com.ygaps.travelapp.network.CommunicationInterface;
import com.ygaps.travelapp.R;
import com.ygaps.travelapp.model.TourResponse;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.ygaps.travelapp.network.MyAPIClient;
import com.ygaps.travelapp.network.UserServices;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener, CommunicationInterface {

    private  static  final String TAG="mainActivity";
    private BottomNavigationView bottomNavigationView;
    public static final int REQUEST_CODE_FOR_TOUR = 4227;
    public static final int REQUEST_CODE = 4228;
    public static final int REQUEST_CODE_FOR_MYTOUR = 4229;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Firebase();

        bottomNavigationView=findViewById(R.id.bottom_navigation);
        bottomNavigationView.setSelectedItemId(R.id.nav_home);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,tourFragment).commit();

    }

    TourFragment tourFragment= new TourFragment();
    MyTourFragment mytourFragment=new MyTourFragment();
    ExploreFragment exploreFragment=new ExploreFragment();
    NotificationFragment notificationFragment= new NotificationFragment();
    SettingsFragment settingsFragment= new SettingsFragment();


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem)
    {
        Log.i(TAG, "onNavigationItemSelected: ");
        switch (menuItem.getItemId()){
            case R.id.nav_home:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,tourFragment).commit();
                return true;
            case R.id.nav_mytour:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,mytourFragment).commit();
                return true;
            case R.id.nav_explore:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,exploreFragment).commit();
                return true;
            case R.id.nav_notification:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,notificationFragment).commit();
                return true;
            case R.id.nav_setting:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,settingsFragment).commit();
                return true;
        }
        return true;
    }

    public void Firebase()
    {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(MainActivity .this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                RegisterFirebaseRequest registerFirebaseRequest=new RegisterFirebaseRequest("","",1,"1.0");;
                UserServices  userServices= MyAPIClient.getInstance().getAdapter().create(UserServices.class);
                String newToken = instanceIdResult.getToken();
                registerFirebaseRequest.setFcmToken(newToken);

                String diviceid = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                        Settings.Secure.ANDROID_ID);
                registerFirebaseRequest.setDiviceId(diviceid);

                Log.d("fcm",registerFirebaseRequest.getFcmToken());
                Log.d("device id",registerFirebaseRequest.getDiviceId());
                Log.d("flat form",registerFirebaseRequest.getPlatform()+"");
                Call<RegisterFiebaseResponse> call=userServices.registerFirebase(loadToken(),registerFirebaseRequest);
                call.enqueue(new Callback<RegisterFiebaseResponse>() {
                    @Override
                    public void onResponse(Call<RegisterFiebaseResponse> call, Response<RegisterFiebaseResponse> response) {
                        if(response.code()==200)
                        {
                            Log.d("register firebase: ", response.body().getMessage().toString());
                        }
                        else
                        {
                            try {
                                Log.d("register firebase: ", response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<RegisterFiebaseResponse> call, Throwable t) {
                        Log.e("register firebase:","failure");
                    }
                });
            }
        });
    }

    @Override
    public TourResponse TourContent(int id, int status, String name, String minCost, String maxCost, String startDate, String endDate,
                                    int adults, int childs, boolean isPrivate, String avatar) {

        byBundle(id, status, name, minCost, maxCost, startDate, endDate, adults, childs, isPrivate, avatar);
        TourResponse tourResponse= new TourResponse();
        tourResponse.setId(id);
        tourResponse.setStatus(status);
        tourResponse.setName(name);
        tourResponse.setMinCost(minCost);
        tourResponse.setMaxCost(maxCost);
        tourResponse.setStartDate(startDate);
        tourResponse.setEndDate(endDate);
        tourResponse.setAdults(adults);
        tourResponse.setChilds(childs);
        tourResponse.setPrivate(isPrivate);
        tourResponse.setAvatar(avatar);
       // Toast.makeText(this, name, Toast.LENGTH_SHORT).show();
        return  tourResponse;
    }

    @Override
    public UserInfoResponse UserInfo(String name, String email, String phone, int gender, String dob) {
        byBundleForUser(name, email,phone,gender,dob);
        return null;
    }

    @Override
    public TourResponseHistory TourHistory(int id, int status, String name, String minCost, String maxCost, String startDate, String endDate, int adults, int childs, boolean isPrivate, String avatar) {
        byBundleForMyTour(id, status, name, minCost, maxCost, startDate, endDate, adults, childs, isPrivate, avatar);
        return null;
    }

    public  void byBundle(int id, int status, String name, String minCost, String maxCost, String startDate, String endDate,
                          int adults, int childs, boolean isPrivate, String avatar)
    {
        Intent intent= new Intent(MainActivity.this,TourDetailActivity.class);
        Bundle bundle= new Bundle();
        bundle.putInt("id",id );
        bundle.putInt("status", status);
        bundle.putString("name", name);
        bundle.putString("minCost", minCost);
        bundle.putString("maxCost", maxCost);
        bundle.putString("startDate", startDate);
        bundle.putString("endDate", endDate);
        bundle.putInt("adults", adults);
        bundle.putInt("childs", childs);
        bundle.putBoolean("isPrivate", isPrivate);
        bundle.putString("avatar", avatar);
        intent.putExtras(bundle);
        startActivityForResult(intent, REQUEST_CODE_FOR_TOUR);

    }

    private  String loadToken(){
        SharedPreferences sharedPreferences=getSharedPreferences(getString(R.string.SHARED_PREFS),MODE_PRIVATE);
        return sharedPreferences.getString(getString(R.string.save_token),"");
    }

    public void byBundleForUser(String name, String email, String phone, int gender, String dob ){
        Intent intent=new Intent(MainActivity.this, EditInfoActivity.class);
        Bundle bundle=new Bundle();
        bundle.putString("UserName", name);
        bundle.putString("UserEmail", email);
        bundle.putString("UserPhone", phone);
        bundle.putInt("UserGender", gender);
        bundle.putString("Userdob", dob);
        intent.putExtras(bundle);
        startActivityForResult(intent, REQUEST_CODE);
    }


    public void byBundleForMyTour(int id, int status, String name, String minCost, String maxCost, String startDate, String endDate,
                                  int adults, int childs, boolean isPrivate, String avatar) {
        Intent intent= new Intent(MainActivity.this,MyTourMainActivity.class);
        Bundle bundle= new Bundle();
        bundle.putInt("id",id );
        bundle.putInt("status", status);
        bundle.putString("name", name);
        bundle.putString("minCost", minCost);
        bundle.putString("maxCost", maxCost);
        bundle.putString("startDate", startDate);
        bundle.putString("endDate", endDate);
        bundle.putInt("adults", adults);
        bundle.putInt("childs", childs);
        bundle.putBoolean("isPrivate", isPrivate);
        bundle.putString("avatar", avatar);
        intent.putExtras(bundle);
        startActivityForResult(intent, REQUEST_CODE_FOR_MYTOUR);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE) {
            if (resultCode == EditInfoActivity.RESULT_OK ) {
                if(data == null){
                    return;
                }
            }
        }
    }

}
