package com.ygaps.travelapp.model.POST;

import com.google.gson.annotations.SerializedName;

public class LoginFBResponse {
    @SerializedName("userId")
    private int userId;
    @SerializedName("token")
    private String token;


    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getUserId() {
        return userId;
    }

    public String getToken() {
        return token;
    }
}
