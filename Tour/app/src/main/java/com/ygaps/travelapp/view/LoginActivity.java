package com.ygaps.travelapp.view;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.ygaps.travelapp.R;
import com.ygaps.travelapp.model.POST.LoginFBRequest;
import com.ygaps.travelapp.model.POST.LoginRequest;
import com.ygaps.travelapp.model.POST.LoginResponse;
import com.ygaps.travelapp.model.POST.LoginFBResponse;
import com.ygaps.travelapp.network.MyAPIClient;
import com.ygaps.travelapp.network.UserServices;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private Button buttonLogin;
    private LoginButton loginButtonfb;
    private EditText editTextUser, editTextPassword;
    private TextView textViewRegister,textViewnotifications, textViewFogetPW;
    private UserServices userServices;
    private String loaduser,loadpassword;
    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
//        ActionBar actionBar = getSupportActionBar();
//        actionBar.hide(); //Ẩn ActionBar nếu muốn
        LoginManager.getInstance().logOut();
        saveToken("");
        saveUserId(0);
        loadData();
        widgets();
        event();
    }
    public void widgets()
    {
        editTextUser=(EditText)findViewById(R.id.edt_user);
        editTextPassword=(EditText)findViewById(R.id.edt_password);
        buttonLogin=(Button)findViewById(R.id.btn_login);
        textViewRegister=(TextView)findViewById(R.id.tv_SignUp);
        textViewnotifications=(TextView)findViewById(R.id.login_notifications);
        loginButtonfb=(LoginButton)findViewById(R.id.login_fb);
        textViewFogetPW=(TextView)findViewById(R.id.tv_ForgotPassword);
        editTextUser.setText(loaduser);
        editTextPassword.setText(loadpassword);

    }

    public void event()
    {
        textViewRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Register();
            }
        });

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Login();
            }
        });

        LoginFacebook();

        textViewFogetPW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ForgetPW();
            }
        });

    }

    public void ForgetPW()
    {
        Intent intent=new Intent(LoginActivity.this, ForgotPassword.class);
        startActivity(intent);
    }

    public void Register()
    {
        Intent intent=new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(intent);
    }

    public void Login()
    {
        String user=editTextUser.getText().toString();
        String password=editTextPassword.getText().toString();

        final LoginRequest loginRequest=new LoginRequest();
        loginRequest.setEmailPhone(user);
        loginRequest.setPassword(password);

        userServices= MyAPIClient.getInstance().getAdapter().create(UserServices.class);
        Call<LoginResponse> call= userServices.login(loginRequest);

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if(response.code()==200)
                {
                    saveData();
                    saveToken(response.body().getToken());
                    saveUserId(response.body().getUserId());
                    textViewnotifications.setText("");
                    Log.e("id",String.valueOf(response.body().getUserId()));
                    //start activity
                    Intent intent=new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                }
                if(response.code()==404||response.code()==400)
                {
                    JSONObject jsonObject=null;
                    try {
                        jsonObject=new JSONObject(response.errorBody().string());
                        textViewnotifications.setText(jsonObject.get("message").toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
            }
        });
    }

    public void LoginFacebook()
    {

        callbackManager=CallbackManager.Factory.create();
        loginButtonfb.setPermissions("email");
        loginButtonfb.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        callbackManager.onActivityResult(requestCode,resultCode,data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    AccessTokenTracker accessTokenTracker=new AccessTokenTracker() {
        @Override
        protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
            if(currentAccessToken==null)
            {
                Log.e("inf","Log out");
            }
            else {
                Log.e("token fb",currentAccessToken.getToken());
                userServices= MyAPIClient.getInstance().getAdapter().create(UserServices.class);
                LoginFBRequest loginFBRequest=new LoginFBRequest();
                loginFBRequest.setAccessToken(currentAccessToken.getToken());
                Call<LoginFBResponse> call= userServices.loginfb(loginFBRequest);
                call.enqueue(new Callback<LoginFBResponse>() {
                    @Override
                    public void onResponse(Call<LoginFBResponse> call, Response<LoginFBResponse> response) {
                        if(response.code()==200)
                        {
                            Log.e("token tour",response.body().getToken());
                          //  Log.e("UserId", response.body().getUserId()+"");
                            saveToken(response.body().getToken());
                            saveUserId(response.body().getUserId());
                            Intent intent=new Intent(LoginActivity.this,MainActivity.class);
                            startActivity(intent);
                        }
                        else
                        {
                            Log.e("error","Error login with fb");
                    }
                    }

                    @Override
                    public void onFailure(Call<LoginFBResponse> call, Throwable t) {

                    }
                });
            }
        }
    };

    private void saveData(){
        SharedPreferences sharedPreferences=getSharedPreferences(getString(R.string.SHARED_PREFS),MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString(getString(R.string.save_user),editTextUser.getText().toString());
        editor.putString(getString(R.string.save_password),editTextPassword.getText().toString());
        editor.apply();
    }

    private void saveToken(String tokensave){
        SharedPreferences sharedPreferences=getSharedPreferences(getString(R.string.SHARED_PREFS),MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString(getString(R.string.save_token),tokensave);
        editor.apply();
    }

    private void saveUserId(int userIdSave){
        SharedPreferences sharedPreferences=getSharedPreferences(getString(R.string.SHARED_PREFS),MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putInt(getString(R.string.save_userId),userIdSave);
        editor.apply();
    }

    private void loadData(){
        SharedPreferences sharedPreferences=getSharedPreferences(getString(R.string.SHARED_PREFS),MODE_PRIVATE);
        loaduser=sharedPreferences.getString(getString(R.string.save_user),"");
        loadpassword=sharedPreferences.getString(getString(R.string.save_password),"");
    }

    private  String loadToken(){
        SharedPreferences sharedPreferences=getSharedPreferences(getString(R.string.SHARED_PREFS),MODE_PRIVATE);
        return sharedPreferences.getString(getString(R.string.save_token),"");
    }

    private  int loadUserId(){
        SharedPreferences sharedPreferences=getSharedPreferences(getString(R.string.SHARED_PREFS),MODE_PRIVATE);
        return sharedPreferences.getInt(getString(R.string.save_userId),0);
    }
}
