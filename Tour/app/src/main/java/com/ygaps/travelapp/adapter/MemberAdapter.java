package com.ygaps.travelapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ygaps.travelapp.R;
import com.ygaps.travelapp.model.GET.MemberResponse;
import com.ygaps.travelapp.model.SearchUserResponse;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MemberAdapter extends BaseAdapter {

    Context myContext;
    int myLayout;
    List<MemberResponse> arrayMember;

    public MemberAdapter(Context context, int layout, List<MemberResponse> memberList){
        myContext=context;
        myLayout=layout;
        arrayMember=memberList;
    }

    @Override
    public int getCount() {
        return arrayMember.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater=(LayoutInflater)myContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        convertView=inflater.inflate(myLayout,null);
        //Anh xa va gan gia tri
        String default_avatarnull="http://noiswebsite.blob.core.windows.net/images/avatar_null.png";

        CircleImageView circleImageView=(CircleImageView) convertView.findViewById(R.id.imageAvatarSearch);
        if(arrayMember.get(position).getAvatar()!=null) {
            Glide.with(myContext).load(arrayMember.get(position).getAvatar()).into(circleImageView);
        }
        else
        {
            Glide.with(myContext).load(default_avatarnull).into(circleImageView);
        }

        TextView textViewName=(TextView) convertView.findViewById(R.id.textViewNameSearch);
        if(arrayMember.get(position).getName()!=null){
            textViewName.setText(arrayMember.get(position).getName());
        }
        else
            textViewName.setText("No Name");

        TextView txtHost=(TextView) convertView.findViewById(R.id.textViewHost);
        if(arrayMember.get(position).isHost()){
            txtHost.setText("H");
        }
        else
            txtHost.setText("");

        return convertView;

    }
}
