package com.ygaps.travelapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ygaps.travelapp.R;
import com.ygaps.travelapp.model.ChatResponse;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatAdapter extends BaseAdapter {

    Context myContext;
    int myLayout;
    List<ChatResponse> arrayChat;

    public ChatAdapter(Context context, int layout, List<ChatResponse> chatList){
        myContext=context;
        myLayout=layout;
        arrayChat=chatList;
    }

    @Override
    public int getCount() {
        return arrayChat.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater=(LayoutInflater)myContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        convertView=inflater.inflate(myLayout,null);
        //Anh xa va gan gia tri
        String default_avatarnull="http://noiswebsite.blob.core.windows.net/images/avatar_null.png";
        CircleImageView circleImageView=(CircleImageView) convertView.findViewById(R.id.imageavatar);
        if(arrayChat.get(position).getAvatar()!=null) {
            Glide.with(myContext).load(arrayChat.get(position).getAvatar()).into(circleImageView);
        }
        else
        {
            Glide.with(myContext).load(default_avatarnull).into(circleImageView);
        }
        TextView textViewNameChat=(TextView) convertView.findViewById(R.id.chatName);
        if(arrayChat.get(position).getName()!=null){
            textViewNameChat.setText(arrayChat.get(position).getName());
        }
        else
            textViewNameChat.setText("No Name");

        TextView textViewChat=(TextView) convertView.findViewById(R.id.chattextview);
        textViewChat.setText(arrayChat.get(position).getNotification());
        return convertView;
    }
}
