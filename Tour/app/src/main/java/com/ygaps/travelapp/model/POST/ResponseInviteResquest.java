package com.ygaps.travelapp.model.POST;

import com.google.gson.annotations.SerializedName;

public class ResponseInviteResquest {
    @SerializedName("tourId")
    private String tourId;
    @SerializedName("isAccepted")
    private boolean isAccepted;

    public ResponseInviteResquest(String tourId, boolean isAccepted) {
        this.tourId = tourId;
        this.isAccepted = isAccepted;
    }

    public String getTourId() {
        return tourId;
    }

    public boolean isAccepted() {
        return isAccepted;
    }

    public void setTourId(String tourId) {
        this.tourId = tourId;
    }

    public void setAccepted(boolean accepted) {
        isAccepted = accepted;
    }
}
