package com.ygaps.travelapp.view;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.ygaps.travelapp.R;
import com.ygaps.travelapp.adapter.InviteMemberAdapter;
import com.ygaps.travelapp.model.GET.GetListInviteTourResponse;
import com.ygaps.travelapp.model.InviteNotiResponse;
import com.ygaps.travelapp.network.MyAPIClient;
import com.ygaps.travelapp.network.UserServices;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationFragment extends Fragment {

    private ListView listView;
    private ArrayList<InviteNotiResponse> inviteNotiResponses;
    private UserServices userServices;
    private String tourId;
    private InviteMemberAdapter adapter;
    private Handler handler;
    private String token;
    private String userId;
    private TextView textView;


    public NotificationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_notification, container, false);

        widgets(view);
        String token=loadToken();
        adapter = new InviteMemberAdapter(
                getActivity(),
                R.layout.ddapterlistnvite,
                inviteNotiResponses,
                token
        );
        listView.setAdapter(adapter);
        userServices = MyAPIClient.getInstance().getAdapter().create(UserServices.class);
        Loadnoti();
        //doTheAutoRefresh();
        event();
        return view;
    }

    public void event()
    {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Loadnoti();
            }
        });

    }

    private void doTheAutoRefresh() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Loadnoti();
                doTheAutoRefresh();
            }
        }, 10000);
    }
    public void widgets(View view) {
        inviteNotiResponses = new ArrayList<InviteNotiResponse>();
        handler = new Handler();
        textView=(TextView)view.findViewById(R.id.notinon);
        listView = (ListView) view.findViewById(R.id.listview_inview);
        token = loadToken();
    }

    private String loadToken() {
        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences(getString(R.string.SHARED_PREFS), Context.MODE_PRIVATE);
        return sharedPreferences.getString(getString(R.string.save_token), "");
    }

    public void Loadnoti() {
        Call<GetListInviteTourResponse> call = userServices.getInvite(token, 1, 100);
        call.enqueue(new Callback<GetListInviteTourResponse>() {
            @Override
            public void onResponse(Call<GetListInviteTourResponse> call, Response<GetListInviteTourResponse> response) {
                if (response.code() == 200) {
                    inviteNotiResponses.clear();
                    for (int i = 0; i < response.body().getInviteNotiResponses().size(); i++) {
                        inviteNotiResponses.add(fInvite(response,i));
                    }
                    Collections.reverse(inviteNotiResponses);
                    adapter.notifyDataSetChanged();
                    if(inviteNotiResponses.size()==0)
                        textView.setText("No notification in tour");
                    else
                        textView.setText("");
                } else {
                    try {
                        Log.e("GetChat fail", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<GetListInviteTourResponse> call, Throwable t) {

            }
        });
    }
    private InviteNotiResponse fInvite(Response<GetListInviteTourResponse> response,int i)
    {
        InviteNotiResponse inviteNotiResponse=new InviteNotiResponse();
        inviteNotiResponse.setId(response.body().getInviteNotiResponses().get(i).getId());
        inviteNotiResponse.setHostIdl(response.body().getInviteNotiResponses().get(i).getHostIdl());
        inviteNotiResponse.setHostName(response.body().getInviteNotiResponses().get(i).getHostName());
        inviteNotiResponse.setHostPhone(response.body().getInviteNotiResponses().get(i).getHostPhone());
        inviteNotiResponse.setHostEmail(response.body().getInviteNotiResponses().get(i).getHostEmail());
        inviteNotiResponse.setHostAvatar(response.body().getInviteNotiResponses().get(i).getHostAvatar());
        inviteNotiResponse.setStatus(response.body().getInviteNotiResponses().get(i).getStatus());
        inviteNotiResponse.setName(response.body().getInviteNotiResponses().get(i).getName());
        inviteNotiResponse.setMinCost(response.body().getInviteNotiResponses().get(i).getMinCost());
        inviteNotiResponse.setMaxCost(response.body().getInviteNotiResponses().get(i).getMaxCost());
        inviteNotiResponse.setStartDate(response.body().getInviteNotiResponses().get(i).getStartDate());
        inviteNotiResponse.setEndDate(response.body().getInviteNotiResponses().get(i).getEndDate());
        inviteNotiResponse.setAdults(response.body().getInviteNotiResponses().get(i).getAdults());
        inviteNotiResponse.setChilds(response.body().getInviteNotiResponses().get(i).getChilds());
        inviteNotiResponse.setAvatar(response.body().getInviteNotiResponses().get(i).getAvatar());
        inviteNotiResponse.setCreatedOn(response.body().getInviteNotiResponses().get(i).getCreatedOn());
        return inviteNotiResponse;
    }

}
