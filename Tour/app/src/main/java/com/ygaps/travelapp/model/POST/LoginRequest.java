package com.ygaps.travelapp.model.POST;

import com.google.gson.annotations.SerializedName;

public class LoginRequest {
    @SerializedName("emailPhone")
    private String emailPhone;
    @SerializedName("password")
    private  String password;

    public String getEmailPhone() {
        return emailPhone;
    }

    public String getPassword() {
        return password;
    }

    public void setEmailPhone(String emailPhone) {
        this.emailPhone = emailPhone;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
