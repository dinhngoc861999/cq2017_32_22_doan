package com.ygaps.travelapp.model.GET;

import com.google.gson.annotations.SerializedName;
import com.ygaps.travelapp.model.CommentResponse;

import java.util.List;

public class GetCommentResponse {

    @SerializedName("commentList")
    private List<CommentResponse> commentResponses;

    public List<CommentResponse> getCommentResponses() {
        return commentResponses;
    }

    public void setCommentResponses(List<CommentResponse> commentResponses) {
        this.commentResponses = commentResponses;
    }


}
