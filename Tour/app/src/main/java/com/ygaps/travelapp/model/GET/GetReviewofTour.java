package com.ygaps.travelapp.model.GET;

import com.google.gson.annotations.SerializedName;
import com.ygaps.travelapp.model.ReviewofTourResponse;

import java.util.List;

public class GetReviewofTour {
    @SerializedName("reviewList")
    private List<ReviewofTourResponse> reviewofTourResponses;

    public List<ReviewofTourResponse> getReviewofTourResponses() {
        return reviewofTourResponses;
    }

    public void setReviewofTourResponses(List<ReviewofTourResponse> reviewofTourResponses) {
        this.reviewofTourResponses = reviewofTourResponses;
    }
}
