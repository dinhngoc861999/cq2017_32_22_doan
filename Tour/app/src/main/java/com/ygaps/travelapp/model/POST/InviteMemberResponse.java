package com.ygaps.travelapp.model.POST;

import com.google.gson.annotations.SerializedName;

public class InviteMemberResponse {
    @SerializedName("message")
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
