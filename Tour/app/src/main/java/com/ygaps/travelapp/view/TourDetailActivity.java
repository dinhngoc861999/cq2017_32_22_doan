package com.ygaps.travelapp.view;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.ygaps.travelapp.R;
import com.ygaps.travelapp.adapter.CommentAdapter;
import com.ygaps.travelapp.model.CommentResponse;
import com.ygaps.travelapp.model.GET.GetCommentResponse;
import com.ygaps.travelapp.model.POST.SendCommentRequest;
import com.ygaps.travelapp.model.POST.SendCommentResponse;
import com.ygaps.travelapp.network.MyAPIClient;
import com.ygaps.travelapp.network.UserServices;

import java.security.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TourDetailActivity extends AppCompatActivity {

    private TextView txtNameTour;
    private TextView txtStartDate;
    private TextView txtEndDate;
    private TextView txtAdults;
    private TextView txtChildren;
    private TextView txtMinCost;
    private TextView txtMaxCost;
    private EditText edtComment;
    private Button btnSendComment;
    private Handler handler;


    private UserServices userServices;
    private  ListView lvComment;
    private ArrayList<CommentResponse> mangComment;
    private String tourId;
    private String token;
    private int userId;
    private CommentAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tour_detail);

        widgets();
        setDataByBundle();

        token=loadToken();
        userId=loadUserId();

        adapter= new CommentAdapter(
                TourDetailActivity.this,
                R.layout.item_comment,
                mangComment
        );
        lvComment.setAdapter(adapter);
        getComment(tourId);
        //doTheAutoRefresh();

        btnSendComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SendCommentRequest sendCommentRequest=new SendCommentRequest();
                sendCommentRequest.setTourId(tourId);
                String sUserId=Integer.toString(userId);
                sendCommentRequest.setUserId(sUserId);
                String sComment=TourDetailActivity.this.edtComment.getText().toString();
                edtComment.setText("");
                sendCommentRequest.setComment(sComment);
                if(!sComment.equals(""))
                    sendComment(sendCommentRequest);
            }
        });
    }

    private void doTheAutoRefresh() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                getComment(tourId);
                doTheAutoRefresh();
            }
        }, 10000);
    }

    public void widgets()
    {
        txtNameTour = (TextView)findViewById(R.id.textNameTour);
        txtStartDate = (TextView) findViewById(R.id.textStarDate);
        txtEndDate = (TextView) findViewById(R.id.textEndDate);
        txtAdults = (TextView) findViewById(R.id.textViewPerson);
        txtChildren = (TextView) findViewById(R.id.textViewChild);
        txtMinCost = (TextView) findViewById(R.id.textViewMinCost);
        txtMaxCost = (TextView) findViewById(R.id.textViewMaxCost);

        lvComment=(ListView) findViewById(R.id.listViewComment);
        edtComment=(EditText) findViewById(R.id.editTextCommentForSend);
        btnSendComment=(Button) findViewById(R.id.buttonSendComment);
        mangComment=new ArrayList<CommentResponse>();
        handler=new Handler();
    }

    public void setDataByBundle()
    {
        Intent intent=getIntent();
        Bundle bundle=intent.getExtras();
        if (bundle != null) {
            int id = bundle.getInt("id",0 );
            int status = bundle.getInt("status", 0);
            String name = bundle.getString("name", "");
            String minCost = bundle.getString("minCost", "");
            String maxCost = bundle.getString("maxCost", "");
            String startDate = bundle.getString("startDate", "");
            String endDate = bundle.getString("endDate", "");

            Date start= new Date(Long.parseLong(startDate));
            Date end= new Date(Long.parseLong(endDate));
            SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");//formating according to my need

            int adults = bundle.getInt("adults",0);
            int childs = bundle.getInt("childs", 0);
            boolean isPrivate=bundle.getBoolean("isPrivate", false);
            String avatar= bundle.getString("avatar", "");

            txtNameTour.setText(name);
            txtStartDate.setText(formatter.format(start));
            txtEndDate.setText(formatter.format(end));
            txtAdults.setText(Integer.toString(adults));
            txtChildren.setText(Integer.toString(childs));
            txtMinCost.setText(minCost);
            txtMaxCost.setText(maxCost);
            tourId=Integer.toString(id);
        }
    }

    public void getComment(String tourId)
    {
        int pageIndex=1;
        int pageSize=100;
        userServices= MyAPIClient.getInstance().getAdapter().create(UserServices.class);
        Call<GetCommentResponse> call=userServices.getCommentResponse(token,tourId, pageIndex,pageSize);
        call.enqueue(new Callback<GetCommentResponse>() {
            @Override
            public void onResponse(Call<GetCommentResponse> call, Response<GetCommentResponse> response) {
                if(response.code()==200) {
                    mangComment.clear();
                    int size=response.body().getCommentResponses().size();
                    if (size!=0)
                    {
                        for (int i=0; i<size; i++)
                        {
                            mangComment.add(fResponse(response,i));
                        }
                        Collections.reverse(mangComment);
                        adapter.notifyDataSetChanged();
                    }

                }
                else{
                    //todo
                    Log.e("400","400");
                }

            }
            @Override
            public void onFailure(Call<GetCommentResponse> call, Throwable t) {

            }
        });
    }

    public void sendComment(SendCommentRequest sendCommentRequest)
    {
        Log.e("tourId", sendCommentRequest.getTourId());
        Log.e("userId", sendCommentRequest.getUserId());
        Log.e("comment", sendCommentRequest.getComment());
        userServices= MyAPIClient.getInstance().getAdapter().create(UserServices.class);
        Call<SendCommentResponse> callcmt=userServices.sendCommentResponse(token,sendCommentRequest);
        callcmt.enqueue(new Callback<SendCommentResponse>() {
            @Override
            public void onResponse(Call<SendCommentResponse> call, Response<SendCommentResponse> response) {
                if(response.code()==200){
                    getComment(tourId);
                    Log.e("sendcomment",response.body().getMessage());
                }
                else {
                    //todo
                    Log.e("sendcomment", response.errorBody()+"");
                }
            }

            @Override
            public void onFailure(Call<SendCommentResponse> call, Throwable t) {

            }
        });
    }


    private CommentResponse fResponse(Response<GetCommentResponse> response, int i)
    {
        CommentResponse commentResponse;
        commentResponse= new CommentResponse();
        commentResponse.setId(response.body().getCommentResponses().get(i).getId());
        commentResponse.setName(response.body().getCommentResponses().get(i).getName());
        commentResponse.setAvatar(response.body().getCommentResponses().get(i).getAvatar());
        commentResponse.setComment(response.body().getCommentResponses().get(i).getComment());
        commentResponse.setCreatedOn(response.body().getCommentResponses().get(i).getCreatedOn());
        return commentResponse;
    }


    private  String loadToken()
    {
        SharedPreferences sharedPreferences=this.getSharedPreferences(getString(R.string.SHARED_PREFS), Context.MODE_PRIVATE);
        return sharedPreferences.getString(getString(R.string.save_token),"");
    }

    private  int loadUserId(){
        SharedPreferences sharedPreferences=getSharedPreferences(getString(R.string.SHARED_PREFS),MODE_PRIVATE);
        return sharedPreferences.getInt(getString(R.string.save_userId),0);
    }
}
