package com.ygaps.travelapp.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ygaps.travelapp.R;
import com.ygaps.travelapp.model.POST.RegisterRequest;
import com.ygaps.travelapp.model.POST.RegisterResponse;
import com.ygaps.travelapp.network.MyAPIClient;
import com.ygaps.travelapp.network.UserServices;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RegisterActivity extends AppCompatActivity {
    private EditText editTextEmail, editTextPhone, editTextPassword, editTextCfPassword;
    private TextInputLayout textInputEmail, textInputPhone, textInputPassword, textInputCfPassword;
    private Button buttonSignUp;
    private UserServices userServices;
    String email,phone,password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        widgets();
        event();
    }

    public void event()
    {
        buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkInput())
                {
                    email=editTextEmail.getText().toString().trim();
                    phone=editTextPhone.getText().toString().trim();
                    password=editTextPassword.getText().toString().trim();

                    final  RegisterRequest request=new RegisterRequest();
                    request.setEmail(email);
                    request.setPhone(phone);
                    request.setPassword(password);

                    userServices=MyAPIClient.getInstance().getAdapter().create(UserServices.class);
                    Call<RegisterResponse> call = userServices.register(request);

                    call.enqueue(new Callback<RegisterResponse>() {
                        @Override
                        public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                            if(response.code()==200) {
                                saveData();
                                Toast.makeText(getApplicationContext(),"Success",Toast.LENGTH_SHORT).show();
                                Intent intent=new Intent(RegisterActivity.this,LoginActivity.class);
                                startActivity(intent);

                            }
                            if(response.code()==400) {
                                JSONObject jsonObject= null;
                                try {
                                    jsonObject=new JSONObject(response.errorBody().string());
                                    if(jsonObject.getString("message").contains("Invalid email"))
                                        checkEmail(1);
                                    if(jsonObject.getString("message").contains("Email already registered"))
                                        checkEmail(2);
                                    if(jsonObject.getString("message").contains("Invalid phone"))
                                        checkPhone(1);
                                    if(jsonObject.getString("message").contains("Phone already registered"))
                                        checkPhone(2);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<RegisterResponse> call, Throwable t) {
                        }
                    });
                }
            }
        });
    }

    public  void widgets()
    {
        editTextEmail=(EditText)findViewById(R.id.register_editText_email);
        editTextPhone=(EditText)findViewById(R.id.register_editText_phone);
        editTextPassword=(EditText)findViewById(R.id.register_editText_password);
        editTextCfPassword=(EditText)findViewById(R.id.register_editText_confirmPassword);
        textInputEmail=(TextInputLayout)findViewById(R.id.register_textInput_email);
        textInputPhone=(TextInputLayout)findViewById(R.id.register_textInput_phone);
        textInputPassword=(TextInputLayout)findViewById(R.id.register_textInput_password);
        textInputCfPassword=(TextInputLayout)findViewById(R.id.register_textInput_confirmPassword);
        buttonSignUp=(Button)findViewById(R.id.register_signUp);
    }

    public boolean checkInput()
    {
        if(!checkEmail(0))
            return false;
        if(!checkPhone(0))
            return false;
        if(!checkPassword())
            return false;
        if(!checkCfPassword())
            return false;
        return true;
    }

    public boolean checkEmail(int errorNumber)
    {
        //if errorNumber==random just check email empty, if error==1 that mean email invalid, if error==2 that mean email already exist
        if(errorNumber==1||editTextEmail.getText().toString().trim().isEmpty())
        {
            textInputEmail.setError(getString(R.string.register_err_email));
            requestFocus(editTextEmail);
            return false;
        }
        if(errorNumber==2)
        {
            textInputEmail.setError(getString(R.string.register_err_email_exist));
            requestFocus(editTextEmail);
            return false;
        }

        textInputEmail.setErrorEnabled(false);
        return true;
    }

    public boolean checkPhone(int errorNumber)
    {
        //if errorNumber==random just check phone empty, if error==1 that mean phone invalid, if error==2 that mean phone already exist
        if(errorNumber==1||editTextPhone.getText().toString().trim().isEmpty())
        {
            textInputPhone.setError(getString(R.string.register_err_phone));
            requestFocus(editTextPhone);
            return false;
        }

        if(errorNumber==2)
        {
            textInputPhone.setError(getString(R.string.register_err_phone_exist));
            requestFocus(editTextPhone);
            return false;
        }

        textInputPhone.setErrorEnabled(false);
        return true;
    }

    public boolean checkPassword()
    {
        if(editTextPassword.getText().toString().trim().isEmpty())
        {
            textInputPassword.setError(getString(R.string.register_err_password));
            requestFocus(editTextPassword);
            return false;
        }
        else
        {
            textInputPassword.setErrorEnabled(false);
        }
        return true;
    }

    public boolean checkCfPassword()
    {
        if(!editTextCfPassword.getText().toString().equals(editTextPassword.getText().toString()))
        {
            if(!editTextPassword.getText().toString().trim().isEmpty())
            {
                textInputCfPassword.setError(getString(R.string.register_err_cfpassword));
                editTextCfPassword.setText("");
                requestFocus(editTextCfPassword);
            }
            return false;
        }
        else
        {
            textInputCfPassword.setErrorEnabled(false);
        }
        return true;
    }

    public void requestFocus(View view){
        if(view.requestFocus())
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    private void saveData(){
        SharedPreferences sharedPreferences=getSharedPreferences(getString(R.string.SHARED_PREFS),MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString(getString(R.string.save_user),editTextEmail.getText().toString());
        editor.putString(getString(R.string.save_password),editTextPassword.getText().toString());
        editor.apply();
    }

}
