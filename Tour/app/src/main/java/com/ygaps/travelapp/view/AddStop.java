package com.ygaps.travelapp.view;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.VisibleRegion;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.ygaps.travelapp.R;
import com.ygaps.travelapp.model.AddStopPoints.CoordinateSet;
import com.ygaps.travelapp.model.AddStopPoints.SuggestedDestinationRequest;
import com.ygaps.travelapp.model.AddStopPoints.SuggestedDestinationResponse;
import com.ygaps.travelapp.model.GET.TourInfoResponse;
import com.ygaps.travelapp.model.POST.AddStopPointsRequest;
import com.ygaps.travelapp.model.POST.AddStopPointsResponse;
import com.ygaps.travelapp.model.DestinationResponse;
import com.ygaps.travelapp.model.GET.SearchDestinationResponse;
import com.ygaps.travelapp.model.POST.SendReviewTourResponse;
import com.ygaps.travelapp.model.StopPoints;
import com.ygaps.travelapp.network.MyAPIClient;
import com.ygaps.travelapp.network.UserServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddStop extends FragmentActivity implements GoogleMap.OnCameraIdleListener,GoogleMap.OnMarkerClickListener,OnMapReadyCallback, GoogleMap.OnMapLongClickListener {

    DatePickerDialog picker;
    final Context context = this;
    private int TourId = -1, purpose = -1;
    private GoogleMap mMap;
    private UserServices userServices;
    private String token;
    private int fresh_count = 1;
    private ArrayList<DestinationResponse> arraydestination = new ArrayList<DestinationResponse>();
    private Button StopPoint, ButtonSearch;
    private EditText Search;
    private SearchView searchView;
    private PermissionsManager permissionsManager;
    // private ArrayList<StopPoints> ListPoints=new ArrayList<StopPoints>();
    private ArrayList<StopPoints> ListPoints = new ArrayList<StopPoints>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_stop);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        widgets();
        event();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng hotelHcm = new LatLng(10.758104986, 106.675041318);
        if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(AddStop.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    2);
        } else {
            mMap.setMyLocationEnabled(true);

            // Permission has already been granted
        }
        // mMap.setMyLocationEnabled(true);

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(hotelHcm, 13));
        //  mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(hotelHcm, 13));

        // Set a listener for marker click.
        mMap.setOnMarkerClickListener(this);

        //start from my tour infor
        if (purpose != 4) {
            mMap.setOnCameraIdleListener(this);
            mMap.setOnMapLongClickListener(this);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 2: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mMap.setMyLocationEnabled(true);

                } else {
                    Toast.makeText(AddStop.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();

                }
                return;
            }
        }
    }

    public void widgets() {
        StopPoint = (Button) findViewById(R.id.buttonStopPonit);
        // ButtonSearch=(Button) findViewById(R.id.buttonSearch);
        //   Search=(EditText) findViewById(R.id.editTextSearch);
        searchView = (SearchView) findViewById(R.id.Searchview);
        //get intent from main activity
        Intent intent = getIntent();
        TourId = intent.getIntExtra("tourid", -1);
        purpose = intent.getIntExtra("purpose", -1);
        if (purpose == 3 || purpose == 2) {
            StopPoint.setVisibility(View.GONE);
        }
        Log.d("id", Integer.toString(TourId));
        if (purpose == 4) {
            searchView.setVisibility(View.GONE);
            loadListStopPoint();

        }
    }

    public void event() {


        //click button to show all stop_point in list
        StopPoint.setOnClickListener(new View.OnClickListener() {
                                         @Override
                                         public void onClick(View v) {
                                             //show list stop_point
                                             final Dialog ListDialog = new Dialog(context);
                                             ListDialog.setContentView(R.layout.list_top_point);
                                             ListDialog.setTitle("List Stop Point");
                                             ListView lvpoints = (ListView) ListDialog.findViewById(R.id.listview_stop);
                                             Button buttonaddall = (Button) ListDialog.findViewById(R.id.buttonaddall);
                                             if (purpose == 4) {
                                                 buttonaddall.setText("save changes");
                                             }
                                             final ArrayAdapter<StopPoints> adapter = new ArrayAdapter<StopPoints>(getApplicationContext(), android.R.layout.simple_list_item_1, ListPoints){
                                                 @NonNull
                                                 @Override
                                                 public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                                                     View view= super.getView(position, convertView, parent);
                                                     TextView text = (TextView) view.findViewById(android.R.id.text1);
                                                     text.setTextColor(Color.BLACK);
                                                     return view;
                                                 }
                                             };
                                            // final ArrayAdapter<StopPoints> adapter = new ArrayAdapter<StopPoints>(getApplicationContext(), android.R.layout.simple_list_item_1, ListPoints);
                                             lvpoints.setAdapter(adapter);
                                             lvpoints.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                 @Override
                                                 public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                                     LatLng markerposition = new LatLng(ListPoints.get(position).getLat(), ListPoints.get(position).getLongt());
                                                     mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(markerposition, 16));
                                                     ListDialog.dismiss();
                                                 }
                                             });
                                             lvpoints.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                                                 @Override
                                                 public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                                                     final int stoppointId = ListPoints.get(position).getId();
                                                     new AlertDialog.Builder(context)
                                                             .setTitle("Are you sure?")
                                                             .setMessage("Do you want to delete this Stop point?")
                                                             .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                                                 @Override
                                                                 public void onClick(DialogInterface dialog, int which) {
                                                                     if (purpose == 4) {
                                                                         UserServices userServices = MyAPIClient.getInstance().getAdapter().create(UserServices.class);

                                                                         Call<SendReviewTourResponse> call = userServices.DELETE_STOP_POINT_CALL(loadToken(), stoppointId);
                                                                         call.enqueue(new Callback<SendReviewTourResponse>() {
                                                                             @Override
                                                                             public void onResponse(Call<SendReviewTourResponse> call, Response<SendReviewTourResponse> response) {
                                                                                 if (response.code() == 200) {
                                                                                     Toast.makeText(getApplicationContext(), "Stop point deleted", Toast.LENGTH_SHORT).show();
                                                                                     // ListPoints.remove(position);
                                                                                     ListPoints.remove(position);
                                                                                     adapter.notifyDataSetChanged();
                                                                                 } else {
                                                                                     Log.e("delete stoppoint", "fail");
                                                                                     Toast.makeText(getApplicationContext(), "Not permission to delete stop point !", Toast.LENGTH_SHORT).show();

                                                                                 }
                                                                             }

                                                                             @Override
                                                                             public void onFailure(Call<SendReviewTourResponse> call, Throwable t) {
                                                                             }
                                                                         });
                                                                     } else {
                                                                         Toast.makeText(getApplicationContext(), "Stop point deleted", Toast.LENGTH_SHORT).show();
                                                                         ListPoints.remove(position);
                                                                         adapter.notifyDataSetChanged();
                                                                     }
                                                                 }
                                                             })
                                                             .setNegativeButton("No", null)
                                                             .show();

                                                     return true;
                                                 }
                                             });
                                             // click button to request to api
                                             buttonaddall.setOnClickListener(new View.OnClickListener() {
                                                 @Override
                                                 public void onClick(View v) {
                                                     if (ListPoints.size() < 1) {
                                                         Toast.makeText(getApplicationContext(), "List stop Points is empty", Toast.LENGTH_SHORT).show();
                                                     } else {
                                                         ListDialog.dismiss();
                                                         addallstoppoint(TourId, 1);
                                                     }
                                                 }
                                             });

                                             ListDialog.getWindow().setGravity(Gravity.BOTTOM);
                                             ListDialog.show();
                                         }

                                     }
        );
        // perform set on query text listener event
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mMap.clear();
                searchdestinaton(query);
                Log.d("search key", query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

    }

    @Override
    public void onMapLongClick(LatLng point) {
        mMap.addMarker(new MarkerOptions().position(point).title(
                "new point").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN)));
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {

        if (purpose == 1 || purpose == 4) {
            int i = -1;
            // suggested destination
            if (marker.getTag() != null) {
                i = Integer.valueOf(marker.getTag().toString());
            }
            // final int finalI = i;
            final int finalI = i;
            // work with dialog
            final Dialog dialog = new Dialog(this);
            dialog.setContentView(R.layout.stop_point_information);
            dialog.setTitle("Title...");
            final EditText dialogname = (EditText) dialog.findViewById(R.id.editTextName);
            final Spinner service = (Spinner) dialog.findViewById(R.id.spinnerService);
            final Spinner province = (Spinner) dialog.findViewById(R.id.spinnerProvince);
            Button add = (Button) dialog.findViewById(R.id.buttonAddStopPoint);
            ImageButton butleaveat = (ImageButton) dialog.findViewById(R.id.buttonLeaveAt);
            ImageButton butarriveat = (ImageButton) dialog.findViewById(R.id.buttonArriveAt);

            final EditText address = (EditText) dialog.findViewById(R.id.editTextAddress);
            final EditText mincost = (EditText) dialog.findViewById(R.id.editTextMin);
            final EditText maxcost = (EditText) dialog.findViewById(R.id.editTextMax);
            Button feedbutton = (Button) dialog.findViewById(R.id.buttonfeedback);
            final EditText arriveat = (EditText) dialog.findViewById(R.id.editTextArriveAt);
            final EditText leaveat = (EditText) dialog.findViewById(R.id.editTextLeaveAt);

            arriveat.setInputType(InputType.TYPE_NULL);
            leaveat.setInputType(InputType.TYPE_NULL);
            ArrayAdapter<CharSequence> serviceAdapter = ArrayAdapter.createFromResource(this,
                    R.array.service, android.R.layout.simple_spinner_item);
            serviceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            service.setAdapter(serviceAdapter);

            ArrayAdapter<CharSequence> provinceAdapter = ArrayAdapter.createFromResource(this,
                    R.array.province, android.R.layout.simple_spinner_item);
            provinceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            province.setAdapter(provinceAdapter);
            //load data
            dialogname.setText(marker.getTitle());
            if (finalI != -1) {
                if (purpose == 1) {
                    address.setText(arraydestination.get(finalI).getAddress());
                    mincost.setText(arraydestination.get(finalI).getMinCost() + "");
                    maxcost.setText(arraydestination.get(finalI).getMaxCost() + "");
                    service.setSelection(arraydestination.get(finalI).getServiceTypeId());
                    province.setSelection(arraydestination.get(finalI).getProvinceId());

                } else {

                    address.setText(ListPoints.get(finalI).getAddRess());
                    mincost.setText(ListPoints.get(finalI).getMinCost() + "");
                    maxcost.setText(ListPoints.get(finalI).getMaxCost() + "");
                    service.setSelection(ListPoints.get(finalI).getServiceTypeId()-1);
                    province.setSelection(ListPoints.get(finalI).getProvinceId());

                    Date start = new Date(ListPoints.get(finalI).getArrivalAt());
                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                    arriveat.setText(formatter.format(start));

                    Date end = new Date(ListPoints.get(finalI).getLeaveAt());
                    SimpleDateFormat formatterq = new SimpleDateFormat("dd/MM/yyyy");
                    leaveat.setText(formatterq.format(end));
                }
            }


            butarriveat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Calendar cldr = Calendar.getInstance();
                    int day = cldr.get(Calendar.DAY_OF_MONTH);
                    int month = cldr.get(Calendar.MONTH);
                    int year = cldr.get(Calendar.YEAR);
                    // date picker dialog
                    picker = new DatePickerDialog(context,
                            new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                    arriveat.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                                }
                            }, year, month, day);
                    picker.show();
                }
            });
            butleaveat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Calendar cldr = Calendar.getInstance();
                    int day = cldr.get(Calendar.DAY_OF_MONTH);
                    int month = cldr.get(Calendar.MONTH);
                    int year = cldr.get(Calendar.YEAR);
                    // date picker dialog
                    picker = new DatePickerDialog(context,
                            new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                    leaveat.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                                }
                            }, year, month, day);
                    picker.show();
                }
            });
            if (purpose == 4) {
                add.setText("Save");
            }
            //feedback button
            feedbutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (finalI != -1) {
                        Intent intent = new Intent(context, ServiceFeedbackActivity.class);
                        if (purpose == 4) {
                            intent.putExtra("serviceId", ListPoints.get(Integer.valueOf(marker.getTag().toString())).getServiceId());
                        } else {
                            intent.putExtra("serviceId", arraydestination.get(finalI).getId());
                        }
                        startActivity(intent);
                    } else {
                        Toast.makeText(getApplicationContext(), "Can't send feedback to new service", Toast.LENGTH_SHORT).show();
                    }
                    dialog.dismiss();
                }
            });

            add.setOnClickListener(new View.OnClickListener() {
                //get information and add to list stop spoint
                @Override
                public void onClick(View v) {
                    if (dialogname.getText().toString().length() > 0 && mincost.getText().toString().length() > 0 && maxcost.getText().toString().length() > 0 && arriveat.getText().toString().length() > 0 && leaveat.getText().toString().length() > 0) {

                        StopPoints temp = new StopPoints();

                        SimpleDateFormat fomater = new SimpleDateFormat("dd/MM/yyyy");
                        ParsePosition pos = new ParsePosition(0);
                        Date arriveatDate = fomater.parse(arriveat.getText().toString().trim(), pos);


                        ParsePosition pos2 = new ParsePosition(0);
                        Date leaveatDate = fomater.parse(leaveat.getText().toString().trim(), pos2);


                        long arrivemillis = arriveatDate.getTime();
                        long leavemillis = leaveatDate.getTime();
                        String servicetypeId = String.valueOf(service.getSelectedItemId());
                        String provinceId = String.valueOf(province.getSelectedItemId());
                        String addressnew = address.getText().toString().trim();
                        if (purpose == 1) {
                            temp.setName(dialogname.getText().toString());
                            temp.setLongt(marker.getPosition().longitude);
                            temp.setLat(marker.getPosition().latitude);
                            temp.setAddRess(addressnew);

                            temp.setArrivalAt(arrivemillis);
                            temp.setLeaveAt(arrivemillis);
                            temp.setServeTypeId(Integer.valueOf(servicetypeId)+1);
                            temp.setProvinceId(Integer.valueOf(provinceId));
                            if (finalI != -1) {
                                temp.setServiceId(arraydestination.get(finalI).getId());
                            }
                            ListPoints.add(temp);
                        } else {
                            ListPoints.get(finalI).setName(dialogname.getText().toString());
                            ListPoints.get(finalI).setLongt(marker.getPosition().longitude);
                            ListPoints.get(finalI).setLat(marker.getPosition().latitude);


                            ListPoints.get(finalI).setServeTypeId(Integer.valueOf(servicetypeId)+1);
                            ListPoints.get(finalI).setProvinceId(Integer.valueOf(provinceId));
                            ListPoints.get(finalI).setArrivalAt(arrivemillis);
                            ListPoints.get(finalI).setLeaveAt(leavemillis);
                            ListPoints.get(finalI).setAddRess(addressnew);
                        }
                        //Log.d("add", temp.getName());
                        dialog.dismiss();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.toast_fill_in, Toast.LENGTH_SHORT).show();

                    }
                }
            });

            //spinner service show
            dialog.show();  //<-- See This!
        } else {
            Intent resultintent = new Intent();
            if (purpose == 2) {
                resultintent.putExtra("perpose", 2);
            } else {
                resultintent.putExtra("perpose", 3);
            }

            resultintent.putExtra("lat", marker.getPosition().latitude);
            resultintent.putExtra("long", marker.getPosition().longitude);
            setResult(1, resultintent);
            finish();//finishing activity
        }
        // Return false to indicate that we have not consumed the event and that we wish
        // for the default behavior to occur (which is for the camera to move such that the
        // marker is centered and for the marker's info window to open, if it has one).
        return false;
    }

    //load stoppoint when start from my tour info || purpose=4
    public void loadListStopPoint() {
        token = loadToken();
        userServices = MyAPIClient.getInstance().getAdapter().create(UserServices.class);
        Call<TourInfoResponse> call = userServices.TOUR_INFO_RESPONSE_CALL(token, TourId);
        call.enqueue(new Callback<TourInfoResponse>() {
            @Override
            public void onResponse(Call<TourInfoResponse> call, Response<TourInfoResponse> response) {
                if (response.code() == 200) {
                    LatLng temp = null;
                    int total = response.body().getStopPoints().size();
                    Log.e("total stop points", String.valueOf(total));
                    for (int i = 0; i < 14 && i < total; i++) {
                        Log.d("diem 1", response.body().getStopPoints().get(i).getLat() + "");
                        temp = new LatLng(response.body().getStopPoints().get(i).getLat(), response.body().getStopPoints().get(i).getLongt());
                        Marker point = mMap.addMarker(new MarkerOptions()
                                .position(temp)
                                .title(response.body().getStopPoints().get(i).getName()));
                        point.setTag(i);
                        ListPoints.add(response.body().getStopPoints().get(i));
                    }
                } else {
                    //todo
                    Log.e("error", "load stop point error");
                }
            }

            @Override
            public void onFailure(Call<TourInfoResponse> call, Throwable t) {
            }
        });
    }


    public void addallstoppoint(int tourid, int servicetype) {
        token = loadToken();
        userServices = MyAPIClient.getInstance().getAdapter().create(UserServices.class);
        AddStopPointsRequest addStopPointsRequest = new AddStopPointsRequest(tourid, ListPoints);
        Call<AddStopPointsResponse> call = userServices.addstop(token, addStopPointsRequest);
        call.enqueue(new Callback<AddStopPointsResponse>() {
            @Override
            public void onResponse(Call<AddStopPointsResponse> call, Response<AddStopPointsResponse> response) {
                if (response.code() == 200) {
                    Log.d("message", "add all stop points success");
                    ListPoints.clear();
                    Toast.makeText(getApplicationContext(), R.string.toast_sp_added, Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    //todo
                    Log.e("400", "400");
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(response.errorBody().string());
                        Log.e("loi ne", jsonObject.getString("message"));
                        Toast.makeText(getApplicationContext(),jsonObject.getString("message") , Toast.LENGTH_SHORT).show();
                        finish();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                fresh_count++;
            }

            @Override
            public void onFailure(Call<AddStopPointsResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "server error", Toast.LENGTH_SHORT).show();
                ListPoints.clear();

            }
        });
    }

    public void searchdestinaton(String key) {
        token = loadToken();
        userServices = MyAPIClient.getInstance().getAdapter().create(UserServices.class);
        Call<SearchDestinationResponse> call = userServices.Searchdestination(token, key, 1, 100);
        call.enqueue(new Callback<SearchDestinationResponse>() {
            @Override
            public void onResponse(Call<SearchDestinationResponse> call, Response<SearchDestinationResponse> response) {
                if (response.code() == 200) {
                    int total = response.body().getDestinationResponses().size();
                    Log.e("total", String.valueOf(total));
                    arraydestination.clear();
                    for (int i = 0; i < 100 && i < total; i++) {
                        arraydestination.add(fResponse(response, i));
//                        Log.d("name", fResponse(response, i).getName());
//                        Log.d("lat", String.valueOf(arraydestination.get(i).getLat()));
//                        Log.d("long", String.valueOf(arraydestination.get(i).getLongt()));
                        LatLng hotelHcm = new LatLng(arraydestination.get(i).getLat(), arraydestination.get(i).getLongt());
                        Marker temp = mMap.addMarker(new MarkerOptions().position(hotelHcm).title(arraydestination.get(i).getName()));
                        temp.setTag(String.valueOf(i));
                    }
                    if (total == 0) {
                        Toast.makeText(getApplicationContext(), "No stop point found", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    //todo
                    Log.e("search error", "search error");
                }

            }

            @Override
            public void onFailure(Call<SearchDestinationResponse> call, Throwable t) {
            }
        });
    }

    private DestinationResponse fResponse(Response<SearchDestinationResponse> response, int i) {
        DestinationResponse tResponse;
        tResponse = new DestinationResponse();
        tResponse.setId(response.body().getDestinationResponses().get(i).getId());
        tResponse.setName(response.body().getDestinationResponses().get(i).getName());
        tResponse.setMinCost(response.body().getDestinationResponses().get(i).getMinCost());
        tResponse.setMaxCost(response.body().getDestinationResponses().get(i).getMaxCost());
        tResponse.setAddress(response.body().getDestinationResponses().get(i).getAddress());
        tResponse.setLat(response.body().getDestinationResponses().get(i).getLat());
        tResponse.setLongt(response.body().getDestinationResponses().get(i).getLongt());

        return tResponse;
    }

    private String loadToken() {
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.SHARED_PREFS), MODE_PRIVATE);
        return sharedPreferences.getString(getString(R.string.save_token), "");
    }

    @Override
    public void onCameraIdle() {
        String querysearch = searchView.getQuery().toString();
        if (querysearch.trim().length() == 0) {
            VisibleRegion visibleRegion = mMap.getProjection().getVisibleRegion();
            LatLng farRight = visibleRegion.farRight;
            LatLng farLeft = visibleRegion.farLeft;
            LatLng nearRight = visibleRegion.nearRight;
            LatLng nearLeft = visibleRegion.nearLeft;
            mMap.clear();
            token = loadToken();
            userServices = MyAPIClient.getInstance().getAdapter().create(UserServices.class);
            SuggestedDestinationRequest suggestedDestinationRequest = new SuggestedDestinationRequest();
            suggestedDestinationRequest.setHasOneCoordinate(false);
            List<CoordinateSet> listcoordinateSets = new ArrayList<CoordinateSet>();
            listcoordinateSets.add(new CoordinateSet(farRight, nearLeft));

            suggestedDestinationRequest.setCoordinateSets(listcoordinateSets);
            //  Log.d("request",suggestedDestinationRequest.getCoordinateSets().get(0).toString());

            Call<SuggestedDestinationResponse> call = userServices.SUGGESTED_DESTINATION_RESPONSE_CALL(token, suggestedDestinationRequest);
            call.enqueue(new Callback<SuggestedDestinationResponse>() {
                @Override
                public void onResponse(Call<SuggestedDestinationResponse> call, Response<SuggestedDestinationResponse> response) {
                    if (response.code() == 200) {
                        int total = response.body().getDestinationResponses().size();
                        Log.d("total stop points", String.valueOf(total));
                        arraydestination.clear();
                        for (int i = 0; i < 100 && i < total; i++) {
                            arraydestination.add(SuggestResponse(response, i));
                            LatLng hotelHcm = new LatLng(arraydestination.get(i).getLat(), arraydestination.get(i).getLongt());
                            Marker temp = mMap.addMarker(new MarkerOptions().position(hotelHcm).title(arraydestination.get(i).getName()));
                            temp.setTag(String.valueOf(i));
                        }
                        if (total == 0) {
                            Toast.makeText(context, R.string.no_stop_point, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        //todo
                        Log.e("suggested-destination", "400");
                    }
                    // fresh_count++;
                }

                @Override
                public void onFailure(Call<SuggestedDestinationResponse> call, Throwable t) {
                }
            });
        }

    }

    private DestinationResponse SuggestResponse(Response<SuggestedDestinationResponse> response, int i) {
        DestinationResponse tResponse;
        tResponse = new DestinationResponse();
        tResponse.setId(response.body().getDestinationResponses().get(i).getId());
        tResponse.setName(response.body().getDestinationResponses().get(i).getName());
        tResponse.setMinCost(response.body().getDestinationResponses().get(i).getMinCost());
        tResponse.setMaxCost(response.body().getDestinationResponses().get(i).getMaxCost());
        tResponse.setAddress(response.body().getDestinationResponses().get(i).getAddress());
        tResponse.setLat(response.body().getDestinationResponses().get(i).getLat());
        tResponse.setServiceTypeId(response.body().getDestinationResponses().get(i).getServiceTypeId());
        tResponse.setLongt(response.body().getDestinationResponses().get(i).getLongt());

        return tResponse;
    }
    @Override
    public void onBackPressed(){
        Intent resultintent = new Intent();
        if (purpose == 2) {
            resultintent.putExtra("perpose", 2);
            setResult(1, resultintent);

        } else if(purpose==3) {
            resultintent.putExtra("perpose", 3);
            setResult(1, resultintent);
        }
        finish();
    }
}