package com.ygaps.travelapp.view;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.Toast;

import com.ygaps.travelapp.R;
import com.ygaps.travelapp.model.POST.CreateTourRequest;
import com.ygaps.travelapp.model.POST.CreateTourResponse;
import com.ygaps.travelapp.network.MyAPIClient;
import com.ygaps.travelapp.network.UserServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import android.widget.DatePicker;
public class CreateTourActivity extends AppCompatActivity {
    final Context context = this;
    DatePickerDialog picker;
    private UserServices userServices;
    private EditText TourName,StartDay,EndDay,MinCost,MaxCost,Adults,Child,StartPoint,EndPoint;
    private Button CreateTour,butStartPoint,butEndPoint;
    private ImageButton butCalenderStart,butCalenderEnd;
    String tourname,startday,endday,mincost,maxcost,adults,child,startpoint,endpoint;
    private RadioButton radiotrip;
    private Double latstart,longstart,latend,longend;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_tour);
        widgets();
        event();

    }


    public void widgets()
    {
        TourName = (EditText) findViewById(R.id.editTextTourName);
        StartDay= (EditText) findViewById(R.id.editTextStartDay);
        EndDay = (EditText) findViewById(R.id.editTextEndDay);
        MinCost=(EditText) findViewById(R.id.editTextMinCost);
        MaxCost=(EditText) findViewById(R.id.editTextMaxCost);
        CreateTour=(Button) findViewById(R.id.buttonCreateTour);
        radiotrip=(RadioButton) findViewById(R.id.radioPrivateTrip);
        Adults=(EditText) findViewById(R.id.editTextAdults);
        Child=(EditText) findViewById(R.id.editTextChild);
        StartPoint=(EditText) findViewById(R.id.edittextStartPoint);
        EndPoint=(EditText) findViewById(R.id.edittextEndPoint);
        butStartPoint=(Button) findViewById(R.id.buttonStartPoint);
        butEndPoint=(Button) findViewById(R.id.buttonEndPoint);
        butCalenderStart=(ImageButton) findViewById(R.id.buttonCalendarStart);
        butCalenderEnd=(ImageButton) findViewById(R.id.buttonCalendarEnd);


        StartDay.setInputType(InputType.TYPE_NULL);
        EndDay.setInputType(InputType.TYPE_NULL);

    }

    public void event()
    {

        butCalenderStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog
                picker = new DatePickerDialog(context,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                StartDay.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            }
                        }, year, month, day);
                picker.show();
            }
        });
        butCalenderEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog
                picker = new DatePickerDialog(context,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                EndDay.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            }
                        }, year, month, day);
                picker.show();
            }
        });

        butStartPoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,AddStop.class);
                // purpose 1: add stoppoints 2:add startpoint 3:add endpoint
                intent.putExtra("purpose",2);
                startActivityForResult(intent,1);
            }
        });
        butEndPoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,AddStop.class);
                // purpose 1: add stoppoints 2:add startpoint 3:add endpoint
                intent.putExtra("purpose",3);
                startActivityForResult(intent,1);
            }
        });
        CreateTour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startday = StartDay.getText().toString().trim();
                endday = EndDay.getText().toString().trim();
                tourname = TourName.getText().toString().trim();
                mincost = MinCost.getText().toString().trim();
                maxcost = MaxCost.getText().toString().trim();
                adults = Adults.getText().toString().trim();

                child = Child.getText().toString().trim();
                startpoint = StartPoint.getText().toString().trim();
                endpoint = EndPoint.getText().toString().trim();
                if (startday.length() > 0 && endday.length() > 0 && tourname.length() > 0 && startpoint.length() > 0 && endpoint.length() > 0) {
                    //convert startday
                    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
                    ParsePosition pos = new ParsePosition(0);
                    Date date = (Date)formater.parse(startday, pos);
                    final long startmillis = date.getTime();

                    ParsePosition pos2 = new ParsePosition(0);
                    Date dateend=(Date) formater.parse(endday,pos2);
                    final long endmillis=dateend.getTime();


                    CreateTourRequest createTourRequest = new CreateTourRequest();

                    createTourRequest.setName(tourname);
                    createTourRequest.setStartDate(startmillis);
                    createTourRequest.setEndDate(endmillis);
                    createTourRequest.setPrivate(radiotrip.isChecked());
                    createTourRequest.setAdults(Integer.parseInt(adults));
                    createTourRequest.setChilds(Integer.parseInt(child));
                    createTourRequest.setMaxCost(Integer.parseInt(maxcost));
                    createTourRequest.setMinCost(Integer.parseInt(mincost));
                    if(radiotrip.isChecked()){
                        Log.d("radio","true");
                    }
                    else{
                        Log.d("radio","false");
                    }


                    userServices = MyAPIClient.getInstance().getAdapter().create(UserServices.class);
                    Call<CreateTourResponse> call = userServices.createtour(loadToken(), createTourRequest);

                    call.enqueue(new Callback<CreateTourResponse>() {
                        @Override
                        public void onResponse(Call<CreateTourResponse> call, Response<CreateTourResponse> response) {
                            if (response.code() == 200) {

                                Log.d("touridfirsr", response.body().getId() + "");
                                Log.d("token",loadToken());
                                Log.d("startmili",String.valueOf(startmillis));
                                Log.d("endmili",String.valueOf(endmillis));


                                Toast.makeText(getApplicationContext(), "Tour created", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(context, AddStop.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.putExtra("tourid", Integer.parseInt(response.body().getId()));
                                intent.putExtra("purpose",1);
                                startActivity(intent);
                                finish();
                            } else {
                                Log.e("fail", "400/500");

                                //Xu li xem no tra ve gi roi in len layout;
                                JSONObject jsonObject = null;
                                try {
                                    jsonObject = new JSONObject(response.errorBody().string());
                                    Log.e("msg", jsonObject.get("message").toString());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<CreateTourResponse> call, Throwable t) {
                            Log.e("fail", "onFailure");
                        }
                    });
                }
                else{
                    Toast.makeText(getApplicationContext(),R.string.toast_fill_in , Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent responintent)
    {
        super.onActivityResult(requestCode, resultCode, responintent);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==1) {
            int perpose= responintent.getIntExtra("perpose",-1);
            if(perpose ==2)//start day
            {
                latstart=responintent.getDoubleExtra("lat",0);
                longstart=responintent.getDoubleExtra("long",0);
                Log.e("perpose",perpose+"");
                Log.e("lat",latstart+"");
                StartPoint.setText("new point");
            }
            else if(perpose ==3)//end day
            {
                latend=responintent.getDoubleExtra("lat",0);
                longend=responintent.getDoubleExtra("long",0);
                EndPoint.setText("new point");
            }
            else {// add stop points
                finish();
            }
        }
    }
    private  String loadToken(){
        SharedPreferences sharedPreferences=getSharedPreferences(getString(R.string.SHARED_PREFS),MODE_PRIVATE);
        return sharedPreferences.getString(getString(R.string.save_token),"");
    }
}
