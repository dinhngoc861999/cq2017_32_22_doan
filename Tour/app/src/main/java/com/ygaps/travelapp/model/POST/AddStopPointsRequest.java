package com.ygaps.travelapp.model.POST;

import com.google.gson.annotations.SerializedName;
import com.ygaps.travelapp.model.StopPoints;

import java.util.ArrayList;

public class AddStopPointsRequest {
    @SerializedName("tourId")
    private int tourId;
    @SerializedName("stopPoints")
    private ArrayList<StopPoints> stoppoint;

    public AddStopPointsRequest(int tourId, ArrayList<StopPoints> stoppoint) {
        this.tourId = tourId;
        this.stoppoint = stoppoint;
    }

    public AddStopPointsRequest(int tourId) {
        this.tourId = tourId;
    }

    public int getTourId() {
        return tourId;
    }

    public ArrayList<StopPoints> getStoppoint() {
        return stoppoint;
    }

    public void setTourId(int tourId) {
        this.tourId = tourId;
    }

    public void setStoppoint(ArrayList<StopPoints> stoppoint) {
        this.stoppoint = stoppoint;
    }
}
