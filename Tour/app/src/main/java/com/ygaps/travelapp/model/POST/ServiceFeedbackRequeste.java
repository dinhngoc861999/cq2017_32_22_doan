package com.ygaps.travelapp.model.POST;

import com.google.gson.annotations.SerializedName;

public class ServiceFeedbackRequeste {
    @SerializedName("serviceId")
    private int serviceId;
    @SerializedName("point")
    private  int point;
    @SerializedName("review")
    private  String review;

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public int getServiceId() {
        return serviceId;
    }

    public int getPoint() {
        return point;
    }

    public String getReview() {
        return review;
    }
}
