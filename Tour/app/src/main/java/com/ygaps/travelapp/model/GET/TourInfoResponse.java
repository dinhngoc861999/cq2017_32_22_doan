package com.ygaps.travelapp.model.GET;

import com.google.gson.annotations.SerializedName;
import com.ygaps.travelapp.model.StopPoints;

import java.util.List;

public class TourInfoResponse {
    @SerializedName("id")
    private int id;
    @SerializedName("hostId")
    private int hostId;
    @SerializedName("status")
    private int status;
    @SerializedName("name")
    private String name;
    @SerializedName("minCost")
    private String minCost;
    @SerializedName("maxCost")
    private  String maxCost;
    @SerializedName("startDate")
    private String startDate;
    @SerializedName("endDate")
    private String endDate;
    @SerializedName("adults")
    private  int adults;
    @SerializedName("childs")
    private  int childs;
    @SerializedName("isPrivate")
    private boolean isPrivate;
    @SerializedName("avatar")
    private String avatar;
    @SerializedName("stopPoints")
    private List<StopPoints> stopPoints;
    @SerializedName("members")
    private List<MemberResponse> member;

    public void setId(int id) {
        this.id = id;
    }

    public void setHostId(int hostId) {
        this.hostId = hostId;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMinCost(String minCost) {
        this.minCost = minCost;
    }

    public void setMaxCost(String maxCost) {
        this.maxCost = maxCost;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public void setAdults(int adults) {
        this.adults = adults;
    }

    public void setChilds(int childs) {
        this.childs = childs;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public void setStopPoints(List<StopPoints> stopPoints) {
        this.stopPoints = stopPoints;
    }

    public void setMenber(List<MemberResponse> menber) {
        this.member = menber;
    }

    public int getId() {
        return id;
    }

    public int getHostId() {
        return hostId;
    }

    public int getStatus() {
        return status;
    }

    public String getName() {
        return name;
    }

    public String getMinCost() {
        return minCost;
    }

    public String getMaxCost() {
        return maxCost;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public int getAdults() {
        return adults;
    }

    public int getChilds() {
        return childs;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public String getAvatar() {
        return avatar;
    }

    public List<StopPoints> getStopPoints() {
        return stopPoints;
    }

    public List<MemberResponse> getMember() {
        return member;
    }
}
