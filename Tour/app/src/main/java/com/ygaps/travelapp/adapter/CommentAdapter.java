package com.ygaps.travelapp.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ygaps.travelapp.R;
import com.ygaps.travelapp.model.CommentResponse;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class CommentAdapter extends BaseAdapter {

    Context myContext;
    int myLayout;
    List<CommentResponse> arrayComment;

    public CommentAdapter(Context context, int layout, List<CommentResponse> commentList){
        myContext=context;
        myLayout=layout;
        arrayComment=commentList;
    }
    @Override
    public int getCount() {
        return arrayComment.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater=(LayoutInflater)myContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        convertView=inflater.inflate(myLayout,null);
        //Anh xa va gan gia tri
        String default_avatarnull="http://noiswebsite.blob.core.windows.net/images/avatar_null.png";

        CircleImageView circleImageView=(CircleImageView) convertView.findViewById(R.id.imageViewCommentAvatar);
        if(arrayComment.get(position).getAvatar()!=null) {
            Glide.with(myContext).load(arrayComment.get(position).getAvatar()).into(circleImageView);
        }
        else {
            Glide.with(myContext).load(default_avatarnull).into(circleImageView);
        }

        TextView txtCommentName=(TextView) convertView.findViewById(R.id.textViewCommentName);
        if(arrayComment.get(position).getName()!=null){
            txtCommentName.setText(arrayComment.get(position).getName());
        }
        else
        {
            txtCommentName.setText("No Name");
        }

        TextView txtCommentContent=(TextView) convertView.findViewById(R.id.textViewCommentContent);
        txtCommentContent.setText(arrayComment.get(position).getComment());

        return convertView;
    }
}
