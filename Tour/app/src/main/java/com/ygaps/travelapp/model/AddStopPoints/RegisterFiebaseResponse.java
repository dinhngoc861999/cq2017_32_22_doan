package com.ygaps.travelapp.model.AddStopPoints;

import com.google.gson.annotations.SerializedName;

public class RegisterFiebaseResponse {
    @SerializedName("message")
    private String message;

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
