package com.ygaps.travelapp.model.POST;

import com.google.gson.annotations.SerializedName;

public class InviteMemberRequest {
    @SerializedName("tourId")
    private String tourId;
    @SerializedName("invitedUserId")
    private String invitedUserIdoptional;
    @SerializedName("isInvited")
    private Boolean isInvited;

    public InviteMemberRequest(String tourId, String invitedUserIdoptional, Boolean isInvited) {
        this.tourId = tourId;
        this.invitedUserIdoptional = invitedUserIdoptional;
        this.isInvited = isInvited;
    }

    public String getTourId() {
        return tourId;
    }

    public String getInvitedUserIdoptional() {
        return invitedUserIdoptional;
    }

    public Boolean getInvited() {
        return isInvited;
    }

    public void setTourId(String tourId) {
        this.tourId = tourId;
    }

    public void setInvitedUserIdoptional(String invitedUserIdoptional) {
        this.invitedUserIdoptional = invitedUserIdoptional;
    }

    public void setInvited(Boolean invited) {
        isInvited = invited;
    }
}
