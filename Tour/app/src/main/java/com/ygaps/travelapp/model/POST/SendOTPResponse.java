package com.ygaps.travelapp.model.POST;

import com.google.gson.annotations.SerializedName;

public class SendOTPResponse {
    @SerializedName("userId")
    private int userId;
    @SerializedName("type")
    private String type;
    @SerializedName("expiredOn")
    private long expiredOg;

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setExpiredOg(long expiredOg) {
        this.expiredOg = expiredOg;
    }

    public int getUserId() {
        return userId;
    }

    public String getType() {
        return type;
    }

    public long getExpiredOg() {
        return expiredOg;
    }
}
