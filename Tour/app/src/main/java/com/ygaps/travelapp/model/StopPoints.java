package com.ygaps.travelapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StopPoints {
    @SerializedName("id")
    private int id;
    @SerializedName("serviceId")
    private int serviceId;

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }

    public void setServiceTypeId(int serviceTypeId) {
        this.serviceTypeId = serviceTypeId;
    }

    public int getServiceId() {
        return serviceId;
    }

    public int getServiceTypeId() {
        return serviceTypeId;
    }

    @SerializedName("name")
    private String name;
    @SerializedName("address")
    private String addRess;
    @SerializedName("provinceId")
    private int provinceId;
    @SerializedName("lat")
    private double lat;
    @SerializedName("long")
    private double longt; // actually long in respon// cannot name this variable is long( conflict with type(long))
    @SerializedName("arrivalAt")
    private long arrivalAt;
    @SerializedName("leaveAt")
    private long leaveAt;
    @SerializedName("serviceTypeId")
    private int serviceTypeId;
    @SerializedName("minCost")
    private int minCost=0;
    @SerializedName("maxCost")
    private int maxCost=0;
    @SerializedName("avatar")
    private String avatar=null;
    @SerializedName("deleteIds")
    private List<Integer> deleteIds=null;

    public StopPoints() {
    }

    public StopPoints(String name, double lat, double longt, long arrivalAt, long leaveAt, int serveTypeId) {
        this.name = name;
        this.lat = lat;
        this.longt = longt;
        this.arrivalAt = arrivalAt;
        this.leaveAt = leaveAt;
        this.serviceTypeId = serveTypeId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddRess(String addRess) {
        this.addRess = addRess;
    }

    public void setProvinceId(int provinceId) {
        this.provinceId = provinceId;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLongt(double longt) {
        this.longt = longt;
    }

    public void setArrivalAt(long arrivalAt) {
        this.arrivalAt = arrivalAt;
    }

    public void setLeaveAt(long leaveAt) {
        this.leaveAt = leaveAt;
    }

    public void setServeTypeId(int serveTypeId) {
        this.serviceTypeId = serveTypeId;
    }

    public void setMinCost(int minCost) {
        this.minCost = minCost;
    }

    public void setMaxCost(int maxCost) {
        this.maxCost = maxCost;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public void setDeleteIds(List<Integer> deleteIds) {
        this.deleteIds = deleteIds;
    }

    public String getname(){return this.name;}

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddRess() {
        return addRess;
    }

    public int getProvinceId() {
        return provinceId;
    }

    public double getLat() {
        return lat;
    }

    public double getLongt() {
        return longt;
    }

    public long getArrivalAt() {
        return arrivalAt;
    }

    public long getLeaveAt() {
        return leaveAt;
    }

    public int getServeTypeId() {
        return serviceTypeId;
    }

    public int getMinCost() {
        return minCost;
    }

    public int getMaxCost() {
        return maxCost;
    }

    public String getAvatar() {
        return avatar;
    }

    public List<Integer> getDeleteIds() {
        return deleteIds;
    }
    public String toString()
    {
        return this.name;
    }
}
