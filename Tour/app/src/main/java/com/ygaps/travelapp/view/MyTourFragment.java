package com.ygaps.travelapp.view;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
//import android.widget.SearchView;
import androidx.appcompat.widget.SearchView;

import com.ygaps.travelapp.R;
import com.ygaps.travelapp.adapter.TourHistoryAdapter;
import com.ygaps.travelapp.model.GET.ListHistoryTourResponse;
import com.ygaps.travelapp.model.GET.ListTourInvitationOfAUserResponse;
import com.ygaps.travelapp.model.GET.SearchHistoryUserResponse;
import com.ygaps.travelapp.model.TourResponseHistory;
import com.ygaps.travelapp.model.TourInvitationOfAUserResponse;
import com.ygaps.travelapp.network.CommunicationInterface;
import com.ygaps.travelapp.network.MyAPIClient;
import com.ygaps.travelapp.network.UserServices;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class MyTourFragment extends Fragment {

    final Context context = this.getContext();
    private ListView lvTour;
    private ArrayList<TourResponseHistory> mangTour;
    private UserServices userServices;
    //private int fresh_count=1;
    private String token;
    Toolbar toolbar;
    SearchView searchView;
    TourHistoryAdapter adapter;

    CommunicationInterface communicationInterface;

    public MyTourFragment() {
        // Required empty public constructor
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        communicationInterface= (CommunicationInterface) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_my_tour, container, false);

        setHasOptionsMenu(true);
        toolbar=(Toolbar) view.findViewById(R.id.toolbar);
        token=loadToken();
        lvTour=(ListView) view.findViewById(R.id.listViewMyTour);
        mangTour= new ArrayList<TourResponseHistory>();
        token=loadToken();
        userServices= MyAPIClient.getInstance().getAdapter().create(UserServices.class);
        adapter= new TourHistoryAdapter(
                getActivity(),
                R.layout.list_tour,
                mangTour
        );

        lvTour.setAdapter(adapter);


        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("");


        ListUserHistory();


        lvTour.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                communicationInterface.TourHistory(mangTour.get(position).getId(), mangTour.get(position).getStatus(),
                        mangTour.get(position).getName(),mangTour.get(position).getMinCost(),mangTour.get(position).getMaxCost(),
                        mangTour.get(position).getStartDate(), mangTour.get(position).getEndDate(),mangTour.get(position).getAdults(),
                        mangTour.get(position).getChilds(),mangTour.get(position).isPrivate(), mangTour.get(position).getAvatar());
            }
        });

        return view;
    }

    private void ListUserHistory(){
        Call<ListHistoryTourResponse> call=userServices.HistoryTourRespon(token,1, 100);

        call.enqueue(new Callback<ListHistoryTourResponse>() {
            @Override
            public void onResponse(Call<ListHistoryTourResponse> call, Response<ListHistoryTourResponse> response) {
                if(response.code()==200) {
                    Log.e("Total", response.body().getTotal()+"");
                    int total =response.body().getTotal();
                    for(int i=0; i<100 && i<total; i++)
                    {
                        if(response.body().getTourResponses().get(i).getStatus()!=-1)
                            mangTour.add(fhisResponse(response,i));
                    }
//                    TourHistoryAdapter adapter= new TourHistoryAdapter(
//                            getActivity(),
//                            R.layout.list_tour,
//                            mangTour
//                    );

                    adapter.notifyDataSetChanged();
                    //      lvTour.setAdapter(adapter);
                }


                else{
                    //todo
                    Log.e("400","400");
                }
                //fresh_count++;
            }
            @Override
            public void onFailure(Call<ListHistoryTourResponse> call, Throwable t) {

            }
        });
    }

    private void ListTourSearchResponse(final String searchKey){
        Call<SearchHistoryUserResponse> call=userServices.searchHistoryUserResponse(token,searchKey,1,100);
        call.enqueue(new Callback<SearchHistoryUserResponse>() {
            @Override
            public void onResponse(Call<SearchHistoryUserResponse> call, Response<SearchHistoryUserResponse> response) {
                if(response.code()==200) {
                    Log.e("Total", response.body().getTotal()+"");
                    int total =response.body().getTotal();
                    for(int i=0; i<100 && i<total; i++)
                    {
                        if(response.body().getTourResponses().get(i).getStatus()!=-1)
                            mangTour.add(fshisResponse(response,i));
                    }
                    adapter.notifyDataSetChanged();
                }
                else{
                    //todo
                    Log.e("400","400");
                }
            }
            @Override
            public void onFailure(Call<SearchHistoryUserResponse> call, Throwable t) {
            }
        });
    }

    private  String loadToken()
    {
        SharedPreferences sharedPreferences=this.getActivity().getSharedPreferences(getString(R.string.SHARED_PREFS), Context.MODE_PRIVATE);
        return sharedPreferences.getString(getString(R.string.save_token),"");
    }

    private TourResponseHistory fhisResponse(Response<ListHistoryTourResponse> response,int i)
    {
        TourResponseHistory tResponse;
        tResponse= new TourResponseHistory();
        tResponse.setId(response.body().getTourResponses().get(i).getId());
        tResponse.setStatus(response.body().getTourResponses().get(i).getStatus());
        tResponse.setName(response.body().getTourResponses().get(i).getName());
        tResponse.setMinCost(response.body().getTourResponses().get(i).getMinCost());
        tResponse.setMaxCost(response.body().getTourResponses().get(i).getMaxCost());
        tResponse.setStartDate(response.body().getTourResponses().get(i).getStartDate());
        tResponse.setEndDate(response.body().getTourResponses().get(i).getEndDate());
        tResponse.setAdults(response.body().getTourResponses().get(i).getAdults());
        tResponse.setChilds(response.body().getTourResponses().get(i).getChilds());
        tResponse.setKicked(response.body().getTourResponses().get(i).isKicked());
        tResponse.setHostId(response.body().getTourResponses().get(i).getHostId());
        tResponse.setHost(response.body().getTourResponses().get(i).isHost());
        return tResponse;
    }

    private TourResponseHistory fshisResponse(Response<SearchHistoryUserResponse> response,int i)
    {
        TourResponseHistory tResponse;
        tResponse= new TourResponseHistory();
        tResponse.setId(response.body().getTourResponses().get(i).getId());
        tResponse.setStatus(response.body().getTourResponses().get(i).getStatus());
        tResponse.setName(response.body().getTourResponses().get(i).getName());
        tResponse.setMinCost(response.body().getTourResponses().get(i).getMinCost());
        tResponse.setMaxCost(response.body().getTourResponses().get(i).getMaxCost());
        tResponse.setStartDate(response.body().getTourResponses().get(i).getStartDate());
        tResponse.setEndDate(response.body().getTourResponses().get(i).getEndDate());
        tResponse.setAdults(response.body().getTourResponses().get(i).getAdults());
        tResponse.setChilds(response.body().getTourResponses().get(i).getChilds());
        tResponse.setKicked(response.body().getTourResponses().get(i).isKicked());
        tResponse.setHostId(response.body().getTourResponses().get(i).getHostId());
        tResponse.setHost(response.body().getTourResponses().get(i).isHost());
        return tResponse;
    }

    private TourInvitationOfAUserResponse fResponse(Response<ListTourInvitationOfAUserResponse> response, int i)
    {
        TourInvitationOfAUserResponse tResponse;
        tResponse= new TourInvitationOfAUserResponse();
        return tResponse;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search_tour_mytour_toolbar, menu);

        searchView= (SearchView) menu.findItem(R.id.menuSearch).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(newText.equals(""))
                {
                    mangTour.clear();
                    Call<ListHistoryTourResponse> call=userServices.HistoryTourRespon(token,1, 100);

                    call.enqueue(new Callback<ListHistoryTourResponse>() {
                        @Override
                        public void onResponse(Call<ListHistoryTourResponse> call, Response<ListHistoryTourResponse> response) {
                            if(response.code()==200) {
                                Log.e("Total", response.body().getTotal()+"");
                                int total =response.body().getTotal();
                                for(int i=0; i<100 && i<total; i++)
                                {
                                    if(response.body().getTourResponses().get(i).getStatus()!=-1)
                                        mangTour.add(fhisResponse(response,i));
                                }
                               adapter.notifyDataSetChanged();
                            }
                            else{
                                //todo
                                Log.e("400","400");
                            }
                        }
                        @Override
                        public void onFailure(Call<ListHistoryTourResponse> call, Throwable t) {

                        }
                    });
                    adapter.notifyDataSetChanged();
                }
                else{
                    mangTour.clear();
                    ListTourSearchResponse(newText);
                    adapter.notifyDataSetChanged();
                }
                return true;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }
}
