package com.ygaps.travelapp.model.GET;

import com.google.gson.annotations.SerializedName;
import com.ygaps.travelapp.model.InviteNotiResponse;

import java.util.List;

public class GetListInviteTourResponse {
    @SerializedName("total")
    private int total;
    @SerializedName("tours")
    private List<InviteNotiResponse> inviteNotiResponses;

    public int getTotal() {
        return total;
    }

    public List<InviteNotiResponse> getInviteNotiResponses() {
        return inviteNotiResponses;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public void setInviteNotiResponses(List<InviteNotiResponse> inviteNotiResponses) {
        this.inviteNotiResponses = inviteNotiResponses;
    }
}
