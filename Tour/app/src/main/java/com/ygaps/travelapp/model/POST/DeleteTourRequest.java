package com.ygaps.travelapp.model.POST;

import com.google.gson.annotations.SerializedName;

public class DeleteTourRequest {
    @SerializedName("id")
    private String id;
    @SerializedName("status")
    private int status;

    public DeleteTourRequest(String id, int status) {
        this.id = id;
        this.status = status;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public int getStatus() {
        return status;
    }
}

