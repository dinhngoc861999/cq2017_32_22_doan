package com.ygaps.travelapp.model.GET;

import com.google.gson.annotations.SerializedName;
import com.ygaps.travelapp.model.TourResponseHistory;

import java.util.List;

public class ListHistoryTourResponse {
    @SerializedName("total")
    private int total;
    @SerializedName("tours")
    private List<TourResponseHistory> tourResponsesHistory;

    public int getTotal() {
        return total;
    }

    public List<TourResponseHistory> getTourResponses() {
        return tourResponsesHistory;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public void setTourResponses(List<TourResponseHistory> tourResponses) {
        this.tourResponsesHistory = tourResponses;
    }
}
