package com.ygaps.travelapp.model.POST;

import com.google.gson.annotations.SerializedName;

public class ResetPasswordRequest {
    @SerializedName("userId")
    private int userId;
    @SerializedName("newPassword")
    private String newPassword;
    @SerializedName("verifyCode")
    private String verifyCode;

    public ResetPasswordRequest(int userId, String newPassword, String verifyCode) {
        this.userId = userId;
        this.newPassword = newPassword;
        this.verifyCode = verifyCode;
    }

    public int getUserId() {
        return userId;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public String getVerifyCode() {
        return verifyCode;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public void setVerifyCode(String verifyCode) {
        this.verifyCode = verifyCode;
    }
}
