package com.ygaps.travelapp.view;

import androidx.annotation.DrawableRes;
import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;

import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.ygaps.travelapp.R;

import java.util.List;
import android.widget.Toast;

// classes needed to initialize map
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;

// classes needed to add the location component
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.modes.CameraMode;

// classes needed to add a marker
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.geometry.LatLng;

import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAllowOverlap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconIgnorePlacement;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconImage;

// classes to calculate a route
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncherOptions;
import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute;
import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute;
import com.mapbox.api.directions.v5.models.DirectionsResponse;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import android.util.Log;

// classes needed to launch navigation UI
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;

import com.mapbox.services.android.navigation.ui.v5.NavigationLauncher;
import com.ygaps.travelapp.model.GET.TourInfoResponse;
import com.ygaps.travelapp.network.MyAPIClient;
import com.ygaps.travelapp.network.UserServices;
import com.ygaps.travelapp.services.SendLongLat;


public class RouteTourActivity extends AppCompatActivity implements MapboxMap.OnMarkerClickListener,OnMapReadyCallback, MapboxMap.OnMapClickListener, PermissionsListener {
    // variables for adding location layer
    private MapView mapView;
    private MapboxMap mapboxMap;
    // variables for adding location layer
    private PermissionsManager permissionsManager;
    private LocationComponent locationComponent;
    // variables for calculating and drawing a route
    private DirectionsRoute currentRoute;
    private static final String TAG = "DirectionsActivity";
    private NavigationMapRoute navigationMapRoute;
    private boolean statusOfGPS=false;
    // variables needed to initialize navigation
    private Button button_navigate,start;

    private Marker markerView;
    private Point destinationPoint;
    private Point originPoint;

    private UserServices userServices;
    private String token;
    private int userId;


    private int TourId = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //access tokens of your account in strings.xml
        Mapbox.getInstance(this, "pk.eyJ1IjoidHVtZW8xMjMiLCJhIjoiY2szeWlvYTg0MDh2ajNscGE2a3lodnJzbiJ9.PaBMOrBA8LGoy8V2FPQ9cg");
  //      Mapbox.getInstance(this, token);
        setContentView(R.layout.activity_route_tour_activbity);
        LocationManager manager = (LocationManager) getSystemService( getBaseContext().LOCATION_SERVICE);
        statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if(statusOfGPS==false) {
            Toast.makeText(getBaseContext(), R.string.user_location_permission_explanation, Toast.LENGTH_LONG).show();
            finish();
        }
        mapView = findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        widgets();
        event();
    }

    public void widgets() {
        Intent intent = getIntent();
        TourId = intent.getIntExtra("tourid", -1);
        token=loadToken();
        userId=loadUserId();
        start=(Button) findViewById(R.id.startButton);
        button_navigate=(Button) findViewById(R.id.navigatebutton);
    }

    public void event() {
      //  loaddestinaton();
        button_navigate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Location lastKnownLocation = mapboxMap.getLocationComponent().getLastKnownLocation();
                mapboxMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                        new LatLng(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude()),13));
            }
        });
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Service();
                start.setEnabled(false);
                start.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
            }
        });
    }
    public void Service()
    {
        Intent intentMyService1 = new Intent(this, SendLongLat.class);
        //Đoạn này để truyền dữ liệu
        //String userId="746";
       // String tourId="5937";
       // String token="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI3NDYiLCJwaG9uZSI6IjAxMjU0NTQyMDgxIiwiZW1haWwiOiIxMjM0NTY4OUBnbWFpbC5jb20iLCJleHAiOjE1Nzk5Mzc2ODYwNzMsImFjY291bnQiOiJ1c2VyIiwiaWF0IjoxNTc3MzQ1Njg2fQ.o5aq3Y0nbPJM19beYNhqGb7qC4XqAtqjXNlf7nCpHGg";
        Bundle bundle=new Bundle();
        bundle.putString("token123",token);
        bundle.putString("userId123", String.valueOf(userId));
        bundle.putString("tourId123",String.valueOf(TourId));
        intentMyService1.putExtras(bundle);

        //gọi services
        startService(intentMyService1);

        //3 dòng này để lấy dữ liệu về
        IntentFilter mainFilter = new IntentFilter("matos.action.GOSERVICE3");
        BroadcastReceiver receiver = new MyMainLocalReceiver();
        registerReceiver(receiver, mainFilter);
    }
    public class MyMainLocalReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            String serviceData = intent.getStringExtra("service3Data");
            Log.e ("Du lieu tra ve o day: ",  serviceData);

            //chỗ này code hiện lên map
        }
    }

    @Override
    public void onMapReady(@NonNull final MapboxMap mapboxMap) {
        this.mapboxMap = mapboxMap;
        loaddestinaton();
        mapboxMap.setStyle(getString(R.string.navigation_guidance_day), new Style.OnStyleLoaded() {
            @Override
            public void onStyleLoaded(@NonNull Style style) {
                enableLocationComponent(style);
                mapboxMap.addOnMapClickListener(RouteTourActivity.this);
//                button_navigate.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        boolean simulateRoute = false;
//                        NavigationLauncherOptions options = NavigationLauncherOptions.builder()
//                                .directionsRoute(currentRoute)
//                                .shouldSimulateRoute(simulateRoute)
//                                .build();
//                        // Call this method with Context from within an Activity
//                        NavigationLauncher.startNavigation(RouteTourActivity.this, options);
//                    }
//                });
            }
        });
        mapboxMap.setOnMarkerClickListener(this);
    }

    public void loaddestinaton() {
        token = loadToken();
        userServices = MyAPIClient.getInstance().getAdapter().create(UserServices.class);
        Call<TourInfoResponse> call = userServices.TOUR_INFO_RESPONSE_CALL(token, TourId);
        call.enqueue(new Callback<TourInfoResponse>() {
            @Override
            public void onResponse(Call<TourInfoResponse> call, Response<TourInfoResponse> response) {
                if (response.code() == 200) {
                    LatLng temp=null;
                    int total = response.body().getStopPoints().size();
                    Log.e("total stop points", String.valueOf(total));
                    for (int i = 0; i < 14 && i < total; i++) {
                        temp = new LatLng(response.body().getStopPoints().get(i).getLat(), response.body().getStopPoints().get(i).getLongt());
                        markerView = mapboxMap.addMarker(new MarkerOptions()
                                .position(temp)
                                .title(response.body().getStopPoints().get(i).getName()));
                    }
                    if(total>0) {
                        originPoint = Point.fromLngLat(locationComponent.getLastKnownLocation().getLongitude(),
                                locationComponent.getLastKnownLocation().getLatitude());

                        // get router to draw Direction
                        destinationPoint = Point.fromLngLat(temp.getLongitude(), temp.getLatitude());
                        getRoute(originPoint, destinationPoint);

                    }
                } else {
                    //todo
                    Log.e("400", "400");
                    Log.e("error",response.errorBody().toString()+"123");
                }
            }

            @Override
            public void onFailure(Call<TourInfoResponse> call, Throwable t) {
            }
        });
    }

    @SuppressWarnings({"MissingPermission"})
    @Override
    public boolean onMapClick(@NonNull LatLng point) {
        // Check marker if existed => delete
        if (destinationPoint != null)
            markerView.remove();
        // draw marker
        markerView = mapboxMap.addMarker(new MarkerOptions()
                .position(point)
                .title("new points"));
        return true;
    }


    @Override
    public boolean onMarkerClick(final Marker marker) {
        Log.d("marker", "marker clicked");
//        if (destinationPoint != null)
//            markerView.remove();
        // draw marker
        LatLng destinate = new LatLng(marker.getPosition().getLatitude(), marker.getPosition().getLongitude());

        destinationPoint = Point.fromLngLat(destinate.getLongitude(), destinate.getLatitude());
        originPoint = Point.fromLngLat(locationComponent.getLastKnownLocation().getLongitude(),
                locationComponent.getLastKnownLocation().getLatitude());

        // get router to draw Direction
        getRoute(originPoint,destinationPoint);

        return false;
    }


    private void getRoute(Point origin, Point destination) {
        NavigationRoute.builder(this)
                .accessToken(Mapbox.getAccessToken())
                .origin(origin)
                .destination(destination)
                .build()
                .getRoute(new Callback<DirectionsResponse>() {
                    @Override
                    public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
                        // You can get the generic HTTP info about the response
                        Log.d(TAG, "Response code: " + response.code());
                        if (response.body() == null) {
                            Log.e(TAG, "No routes found, make sure you set the right user and access token.");
                            return;
                        } else if (response.body().routes().size() < 1) {
                            Log.e(TAG, "No routes found");
                            return;
                        }
                        currentRoute = response.body().routes().get(0);

                        // Draw the route on the map
                        if (navigationMapRoute != null) {
                            navigationMapRoute.removeRoute();
                        } else {
                            navigationMapRoute = new NavigationMapRoute(null, mapView, mapboxMap, R.style.NavigationMapRoute);
                        }
                        navigationMapRoute.addRoute(currentRoute);
                    }

                    @Override
                    public void onFailure(Call<DirectionsResponse> call, Throwable throwable) {
                        Log.e(TAG, "Error: " + throwable.getMessage());
                    }
                });
    }

    @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
// Check if permissions are enabled and if not request
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
// Activate the MapboxMap LocationComponent to show user location
// Adding in LocationComponentOptions is also an optional parameter
            locationComponent = mapboxMap.getLocationComponent();
            locationComponent.activateLocationComponent(this, loadedMapStyle);
            locationComponent.setLocationComponentEnabled(true);
// Set the component's camera mode
            locationComponent.setCameraMode(CameraMode.TRACKING);
            button_navigate.setEnabled(true);
            start.setEnabled(true);
        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        Toast.makeText(this, R.string.user_location_permission_explanation, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            enableLocationComponent(mapboxMap.getStyle());
            button_navigate.setEnabled(true);
            start.setEnabled(true);

        } else {
            Toast.makeText(this, R.string.user_location_permission_not_granted, Toast.LENGTH_LONG).show();
            finish();
        }
    }

    private String loadToken() {
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.SHARED_PREFS), MODE_PRIVATE);
        return sharedPreferences.getString(getString(R.string.save_token), "");
    }

    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }


    private  int loadUserId(){
        SharedPreferences sharedPreferences=getSharedPreferences(getString(R.string.SHARED_PREFS),MODE_PRIVATE);
        return sharedPreferences.getInt(getString(R.string.save_userId),0);
    }

    /**
     * Called when the user clicks on the map view.
     *
     * @param point The projected map coordinate the user clicked on.
     * @return True if this click should be consumed and not passed further to other listeners registered afterwards,
     * false otherwise.
     */
}