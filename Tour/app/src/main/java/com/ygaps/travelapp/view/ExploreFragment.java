
package com.ygaps.travelapp.view;


import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.VisibleRegion;
import com.ygaps.travelapp.R;


import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.ygaps.travelapp.model.AddStopPoints.CoordinateSet;
import com.ygaps.travelapp.model.AddStopPoints.SuggestedDestinationRequest;
import com.ygaps.travelapp.model.AddStopPoints.SuggestedDestinationResponse;
import com.ygaps.travelapp.model.DestinationResponse;
import com.ygaps.travelapp.model.GET.SearchDestinationResponse;
import com.ygaps.travelapp.model.StopPoints;
import com.ygaps.travelapp.network.MyAPIClient;
import com.ygaps.travelapp.network.UserServices;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class ExploreFragment extends Fragment implements GoogleMap.OnMarkerClickListener,GoogleMap.OnCameraIdleListener {

    MapView mMapView;
    private GoogleMap mMap;
    private UserServices userServices;
    private String token;
    private SearchView searchView;

    private ArrayList<DestinationResponse> arraydestination=new ArrayList<DestinationResponse>();
    private Button ButtonSearch;
    private EditText Search;
    // private ArrayList<StopPoints> ListPoints=new ArrayList<StopPoints>();
    private ArrayList<StopPoints> ListPoints=new ArrayList<StopPoints>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_explore, container, false);

        mMapView = (MapView) rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        searchView=(SearchView) rootView.findViewById(R.id.searchviewfm);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mMap.clear();
                searchdestinaton(query);
                Log.d("search key",query);
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                ExploreFragment.this.mMap = mMap;

                // For showing a move to my location button
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                    ExploreFragment.this.mMap.setMyLocationEnabled(true);

                }

                // For dropping a marker at a point on the Map
                LatLng sydney = new LatLng(-34, 151);
                LatLng hotelHcm= new LatLng(10.758104986,106.675041318);
                ExploreFragment.this.mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(hotelHcm,13));


                // ExploreFragment.this.mMap.animateCamera(CameraUpdateFactory.newLatLng(hotelHcm));

//                mMap.setOnCameraChangeListener(this);
                mMap.setOnCameraIdleListener(ExploreFragment.this);
                mMap.setOnMarkerClickListener(ExploreFragment.this);
            }
        });

        return rootView;
    }

    public void searchdestinaton(String key) {
        token = loadToken();
        userServices = MyAPIClient.getInstance().getAdapter().create(UserServices.class);
        Call<SearchDestinationResponse> call = userServices.Searchdestination(token, key, 1, 15);
        call.enqueue(new Callback<SearchDestinationResponse>() {
            @Override
            public void onResponse(Call<SearchDestinationResponse> call, Response<SearchDestinationResponse> response) {
                if (response.code() == 200) {
                    int total = response.body().getTotal();
                    Log.e("total", String.valueOf(total));
                    arraydestination.clear();
                    for (int i = 0; i < 14 && i < total; i++) {
                        arraydestination.add(fResponse(response, i));
                        Log.e("name", fResponse(response, i).getName());
                        Log.e("lat", String.valueOf(arraydestination.get(i).getLat()));
                        Log.e("long", String.valueOf(arraydestination.get(i).getLongt()));
                        LatLng hotelHcm = new LatLng(arraydestination.get(i).getLat(), arraydestination.get(i).getLongt());
                        Marker temp = mMap.addMarker(new MarkerOptions().position(hotelHcm).title(arraydestination.get(i).getName()));
                        temp.setTag(0);
                    }
                    if(total==0)
                    {
                        Toast.makeText(getContext(), "No stop point found", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    //todo
                    Log.e("search error", "400");
                }
               // fresh_count++;
            }
            @Override
            public void onFailure(Call<SearchDestinationResponse> call, Throwable t) {
            }
        });
    }

    private DestinationResponse fResponse(Response<SearchDestinationResponse> response, int i)
    {
        DestinationResponse tResponse;
        tResponse= new DestinationResponse();
        tResponse.setId(response.body().getDestinationResponses().get(i).getId());
        tResponse.setName(response.body().getDestinationResponses().get(i).getName());
        tResponse.setMinCost(response.body().getDestinationResponses().get(i).getMinCost());
        tResponse.setMaxCost(response.body().getDestinationResponses().get(i).getMaxCost());
        tResponse.setAddress(response.body().getDestinationResponses().get(i).getAddress());
        tResponse.setLat(response.body().getDestinationResponses().get(i).getLat());
        tResponse.setServiceTypeId(response.body().getDestinationResponses().get(i).getServiceTypeId());
        tResponse.setLongt(response.body().getDestinationResponses().get(i).getLongt());

        return tResponse;
    }
    private  String loadToken()
    {
        SharedPreferences sharedPreferences= this.getActivity().getSharedPreferences(getString(R.string.SHARED_PREFS),MODE_PRIVATE);
        return sharedPreferences.getString(getString(R.string.save_token),"");
    }


    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onCameraIdle() {
        String querysearch=  searchView.getQuery().toString();
        if(querysearch.trim().length()==0) {
            VisibleRegion visibleRegion = mMap.getProjection().getVisibleRegion();
            LatLng farRight = visibleRegion.farRight;
            LatLng farLeft = visibleRegion.farLeft;
            LatLng nearRight = visibleRegion.nearRight;
            LatLng nearLeft = visibleRegion.nearLeft;
            mMap.clear();

            token = loadToken();
            userServices = MyAPIClient.getInstance().getAdapter().create(UserServices.class);
            SuggestedDestinationRequest suggestedDestinationRequest = new SuggestedDestinationRequest();
            suggestedDestinationRequest.setHasOneCoordinate(false);
            List<CoordinateSet> listcoordinateSets = new ArrayList<CoordinateSet>();
            listcoordinateSets.add(new CoordinateSet(farRight, nearLeft));
            suggestedDestinationRequest.setCoordinateSets(listcoordinateSets);
            Call<SuggestedDestinationResponse> call = userServices.SUGGESTED_DESTINATION_RESPONSE_CALL(token, suggestedDestinationRequest);
            call.enqueue(new Callback<SuggestedDestinationResponse>() {
                @Override
                public void onResponse(Call<SuggestedDestinationResponse> call, Response<SuggestedDestinationResponse> response) {
                    if (response.code() == 200) {
                        int total = response.body().getDestinationResponses().size();
                        Log.d("total stop point", String.valueOf(total));
                        arraydestination.clear();
                        for (int i = 0; i < 300 && i < total; i++) {
                            arraydestination.add(SuggestResponse(response, i));
                            LatLng hotelHcm = new LatLng(arraydestination.get(i).getLat(), arraydestination.get(i).getLongt());
                            Marker temp = mMap.addMarker(new MarkerOptions().position(hotelHcm).title(arraydestination.get(i).getName()));
                            temp.setTag(String.valueOf(i));
                        }
                        if (total == 0) {
                            Toast.makeText(getContext(), R.string.no_stop_point, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        //todo
                        Log.e("suggested-destination", "400");
                    }
                    // fresh_count++;
                }

                @Override
                public void onFailure(Call<SuggestedDestinationResponse> call, Throwable t) {
                }
            });
        }

    }
    private DestinationResponse SuggestResponse(Response<SuggestedDestinationResponse> response, int i)
    {
        DestinationResponse tResponse;
        tResponse= new DestinationResponse();
        tResponse.setId(response.body().getDestinationResponses().get(i).getId());
        tResponse.setName(response.body().getDestinationResponses().get(i).getName());
        tResponse.setMinCost(response.body().getDestinationResponses().get(i).getMinCost());
        tResponse.setMaxCost(response.body().getDestinationResponses().get(i).getMaxCost());
        tResponse.setAddress(response.body().getDestinationResponses().get(i).getAddress());
        tResponse.setLat(response.body().getDestinationResponses().get(i).getLat());
        tResponse.setServiceTypeId(response.body().getDestinationResponses().get(i).getServiceTypeId());
        tResponse.setLongt(response.body().getDestinationResponses().get(i).getLongt());

        return tResponse;
    }
    public double calculateVisibleRadius() {
        float[] distanceWidth = new float[1];
        VisibleRegion visibleRegion = mMap.getProjection().getVisibleRegion();
        LatLng farRight = visibleRegion.farRight;
        LatLng farLeft = visibleRegion.farLeft;
        LatLng nearRight = visibleRegion.nearRight;
        LatLng nearLeft = visibleRegion.nearLeft;
        //calculate the distance between left <-> right of map on screen
        Location.distanceBetween( (farLeft.latitude + nearLeft.latitude) / 2, farLeft.longitude, (farRight.latitude + nearRight.latitude) / 2, farRight.longitude, distanceWidth );
        // visible radius is / 2  and /1000 in Km:
        return distanceWidth[0] / 2 / 1000 ;
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {
        int i=-1;
        // suggested destination
        if(marker.getTag() !=null) {
            i = Integer.valueOf(marker.getTag().toString());
        }
        // final int finalI = i;
        final int finalI = i;
        // work with dialog
        final Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.stop_point_information);
        dialog.setTitle("Stop point informarion");
        final EditText dialogname = (EditText) dialog.findViewById(R.id.editTextName);
        final Spinner service = (Spinner) dialog.findViewById(R.id.spinnerService);
        final Spinner province=(Spinner) dialog.findViewById(R.id.spinnerProvince);
        Button add = (Button) dialog.findViewById(R.id.buttonAddStopPoint);
        ImageButton butleaveat=(ImageButton) dialog.findViewById(R.id.buttonLeaveAt);
        ImageButton butarriveat=(ImageButton) dialog.findViewById(R.id.buttonArriveAt);

        final EditText address=(EditText) dialog.findViewById(R.id.editTextAddress);
        EditText mincost=(EditText) dialog.findViewById(R.id.editTextMin);
        EditText maxcost=(EditText) dialog.findViewById(R.id.editTextMax);
        Button feedbutton= (Button) dialog.findViewById(R.id.buttonfeedback);
        final EditText arriveat=(EditText) dialog.findViewById(R.id.editTextArriveAt);
        final EditText leaveat=(EditText) dialog.findViewById(R.id.editTextLeaveAt);
        add.setVisibility(View.GONE);

        ArrayAdapter<CharSequence> serviceAdapter = ArrayAdapter.createFromResource(getContext(),
                R.array.service, android.R.layout.simple_spinner_item);
       // serviceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        service.setAdapter(serviceAdapter);

        ArrayAdapter<CharSequence> provinceAdapter = ArrayAdapter.createFromResource(getContext(),
                R.array.province, android.R.layout.simple_spinner_item);
      //  provinceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        province.setAdapter(provinceAdapter);
        //load data
       // dialogname.setText(marker.getTitle());
        if(finalI!=-1) {
                address.setText(arraydestination.get(finalI).getAddress());
                mincost.setText(arraydestination.get(finalI).getMinCost()+"");
                maxcost.setText(arraydestination.get(finalI).getMaxCost()+"");
                service.setSelection(arraydestination.get(finalI).getServiceTypeId());
                province.setSelection(arraydestination.get(finalI).getProvinceId());

            }
        feedbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (finalI!=-1) {
                    Intent intent = new Intent(getContext(), ServiceFeedbackActivity.class);
                    intent.putExtra("serviceId",arraydestination.get(finalI).getId() );
                    startActivity(intent);
                    dialog.dismiss();
                }
                else {
                    Toast.makeText(getContext(),"Can't send feedback to new service",Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }

            }
        });
        //spinner service show
        dialog.show();  //<-- See This!
        return false;
    }
}