package com.ygaps.travelapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ygaps.travelapp.R;
import com.ygaps.travelapp.model.ChatResponse;
import com.ygaps.travelapp.model.SearchUserResponse;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserSearchAdapter extends BaseAdapter {

    Context myContext;
    int myLayout;
    List<SearchUserResponse> arrayUser;

    public UserSearchAdapter(Context context, int layout, List<SearchUserResponse> chatList){
        myContext=context;
        myLayout=layout;
        arrayUser=chatList;
    }

    @Override
    public int getCount() {
        return arrayUser.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater=(LayoutInflater)myContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        convertView=inflater.inflate(myLayout,null);
        //Anh xa va gan gia tri
        String default_avatarnull="http://noiswebsite.blob.core.windows.net/images/avatar_null.png";

        CircleImageView circleImageView=(CircleImageView) convertView.findViewById(R.id.imageAvatarSearch);
        if(arrayUser.get(position).getAvatar()!=null) {
            Glide.with(myContext).load(arrayUser.get(position).getAvatar()).into(circleImageView);
        }
        else
        {
            Glide.with(myContext).load(default_avatarnull).into(circleImageView);
        }

        TextView textViewName=(TextView) convertView.findViewById(R.id.textViewNameSearch);
        if(arrayUser.get(position).getFullName()!=null){
            textViewName.setText(arrayUser.get(position).getFullName());
        }
        else
            textViewName.setText("No Name");

        Button btnAddUser =(Button) convertView.findViewById(R.id.ButtonAddUser);
        btnAddUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        return convertView;

    }
}


