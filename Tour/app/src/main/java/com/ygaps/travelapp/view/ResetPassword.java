package com.ygaps.travelapp.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
import com.ygaps.travelapp.R;
import com.ygaps.travelapp.model.POST.RegisterResponse;
import com.ygaps.travelapp.model.POST.ResetPasswordRequest;
import com.ygaps.travelapp.model.POST.ResetPasswordResponse;
import com.ygaps.travelapp.network.MyAPIClient;
import com.ygaps.travelapp.network.UserServices;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPassword extends AppCompatActivity {

    private EditText editTextOtp;
    private EditText editTextPassword;
    private EditText editTextCfPassword;
    private TextInputLayout textInputPassword;
    private TextInputLayout textInputCfPassword;
    private TextInputLayout textInputOtp;
    private Button buttonreset;
    UserServices userServices;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        widgets();
        event();
    }

    public  void widgets()
    {
        editTextOtp=(EditText)findViewById(R.id.resetpw_editText_OTP);
        editTextPassword=(EditText)findViewById(R.id.reset_editText_password);
        editTextCfPassword=(EditText)findViewById(R.id.reset_editText_confirmPassword);
        textInputOtp=(TextInputLayout)findViewById(R.id.resetpw_TextInput_OTP);
        textInputPassword=(TextInputLayout)findViewById(R.id.reset_textInput_password);
        textInputCfPassword=(TextInputLayout)findViewById(R.id.reset_textInput_confirmPassword);
        buttonreset=(Button)findViewById(R.id.reset_buton);
    }
    public void event()
    {
        buttonreset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkInput())
                {
                    userServices= MyAPIClient.getInstance().getAdapter().create(UserServices.class);
                    String otp=editTextOtp.getText().toString();
                    String password= editTextPassword.getText().toString();
                    Log.e("userid",loadUserId()+"");
                    ResetPasswordRequest resetPasswordRequest=new ResetPasswordRequest(loadUserId(),password,otp);

                    Call<ResetPasswordResponse> call=userServices.resetPassword(resetPasswordRequest);

                    call.enqueue(new Callback<ResetPasswordResponse>() {
                        @Override
                        public void onResponse(Call<ResetPasswordResponse> call, Response<ResetPasswordResponse> response) {
                            if(response.code()==200)
                            {
                                saveData();
                                Toast.makeText(getApplicationContext(),"Reset Password successful",Toast.LENGTH_SHORT).show();
                                Intent intent=new Intent(ResetPassword.this,LoginActivity.class);
                                startActivity(intent);
                                finish();
                            }
                            if(response.code()==403)
                            {
                                textInputOtp.setError("Wrong OTP or your OTP expired");
                                requestFocus(editTextOtp);
                            }
                        }

                        @Override
                        public void onFailure(Call<ResetPasswordResponse> call, Throwable t) {

                        }
                    });


                }
            }
        });
    }
    public boolean checkInput()
    {
        if(!checkOtp())
            return false;
        if(!checkPassword())
            return false;
        if(!checkCfPassword())
            return false;
        return true;
    }

    public boolean checkOtp()
    {
        if(editTextOtp.getText().toString().trim().isEmpty())
        {
            textInputOtp.setError("Input OTP code");
            requestFocus(editTextOtp);
            return false;
        }
        else
        {
            textInputPassword.setErrorEnabled(false);
        }
        return true;
    }


    public boolean checkPassword()
    {
        if(editTextPassword.getText().toString().trim().isEmpty())
        {
            textInputPassword.setError(getString(R.string.register_err_password));
            requestFocus(editTextPassword);
            return false;
        }
        else
        {
            textInputPassword.setErrorEnabled(false);
        }
        return true;
    }

    public boolean checkCfPassword()
    {
        if(!editTextCfPassword.getText().toString().equals(editTextPassword.getText().toString()))
        {
            if(!editTextPassword.getText().toString().trim().isEmpty())
            {
                textInputCfPassword.setError(getString(R.string.register_err_cfpassword));
                editTextCfPassword.setText("");
                requestFocus(editTextCfPassword);
            }
            return false;
        }
        else
        {
            textInputCfPassword.setErrorEnabled(false);
        }
        return true;
    }

    public void requestFocus(View view){
        if(view.requestFocus())
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    private void saveData(){
        SharedPreferences sharedPreferences=getSharedPreferences(getString(R.string.SHARED_PREFS),MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString(getString(R.string.save_password),editTextPassword.getText().toString());
        editor.apply();
    }

    private  int loadUserId()
    {
        SharedPreferences sharedPreferences=getApplicationContext().getSharedPreferences(getString(R.string.SHARED_PREFS), Context.MODE_PRIVATE);
        int result= sharedPreferences.getInt(getString(R.string.save_userId),0);
        return result;
    }
}
