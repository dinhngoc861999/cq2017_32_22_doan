package com.ygaps.travelapp.model.GET;

import com.google.gson.annotations.SerializedName;
import com.ygaps.travelapp.model.TourInvitationOfAUserResponse;

import java.util.List;

public class ListTourInvitationOfAUserResponse {

    @SerializedName("total")
    private int total;
    @SerializedName("tours")
    private List<TourInvitationOfAUserResponse> tourInvitationOfAUserResponses;

    public int getTotal() {
        return total;
    }

    public List<TourInvitationOfAUserResponse> getTourInvitationOfAUserResponse() {
        return tourInvitationOfAUserResponses;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public void setTourInvitationOfAUserResponses(List<TourInvitationOfAUserResponse> tourInvitationOfAUserResponses) {
        this.tourInvitationOfAUserResponses = tourInvitationOfAUserResponses;
    }
}
