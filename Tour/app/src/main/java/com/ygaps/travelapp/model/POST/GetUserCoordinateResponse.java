package com.ygaps.travelapp.model.POST;

import com.google.gson.annotations.SerializedName;
import com.ygaps.travelapp.model.GetCoordinate;

import java.util.List;

public class GetUserCoordinateResponse {
    private  List<GetCoordinate> getCoordinates;

    public List<GetCoordinate> getGetCoordinates() {
        return getCoordinates;
    }

    public void setGetCoordinates(List<GetCoordinate> getCoordinates) {
        this.getCoordinates = getCoordinates;
    }
}
