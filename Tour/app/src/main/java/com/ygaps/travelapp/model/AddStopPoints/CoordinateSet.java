package com.ygaps.travelapp.model.AddStopPoints;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CoordinateSet {
    @SerializedName("coordinateSet")
    private List<Coordinate> coordinates;

    public CoordinateSet(LatLng first, LatLng second) {
        this.coordinates = new ArrayList<Coordinate>();
        this.coordinates.add(new Coordinate(first));
        this.coordinates.add(new Coordinate(second));
    }

    public void setCoordinates(List<Coordinate> coordinates) {
        this.coordinates = coordinates;
    }

    public List<Coordinate> getCoordinates() {
        return coordinates;
    }

}
