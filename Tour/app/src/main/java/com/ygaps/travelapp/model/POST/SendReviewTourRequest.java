package com.ygaps.travelapp.model.POST;

import com.google.gson.annotations.SerializedName;

public class SendReviewTourRequest {
    @SerializedName("tourId")
    private String tourId;
    @SerializedName("point")
    private int point;
    @SerializedName("review")
    private String review;

    public SendReviewTourRequest(String tourId, int point, String review) {
        this.tourId = tourId;
        this.point = point;
        this.review = review;
    }

    public void setTourId(String tourId) {
        this.tourId = tourId;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getTourId() {
        return tourId;
    }

    public int getPoint() {
        return point;
    }

    public String getReview() {
        return review;
    }
}
