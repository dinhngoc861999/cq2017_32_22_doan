package com.ygaps.travelapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ygaps.travelapp.R;
import com.ygaps.travelapp.model.ChatResponse;
import com.ygaps.travelapp.model.ReviewofTourResponse;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ReviewAdapter extends BaseAdapter {

    Context myContext;
    int myLayout;
    List<ReviewofTourResponse> reviewofTourResponses;

    public ReviewAdapter(Context context, int layout, List<ReviewofTourResponse> reviewList){
        myContext=context;
        myLayout=layout;
        reviewofTourResponses=reviewList;
    }

    @Override
    public int getCount() {
        return reviewofTourResponses.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater=(LayoutInflater)myContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        convertView=inflater.inflate(myLayout,null);
        //Anh xa va gan gia tri
        String default_avatarnull="http://noiswebsite.blob.core.windows.net/images/avatar_null.png";
        CircleImageView circleImageView=(CircleImageView) convertView.findViewById(R.id.imageavatar_review);
        if(reviewofTourResponses.get(position).getAvatar()!=null) {
            Glide.with(myContext).load(reviewofTourResponses.get(position).getAvatar()).into(circleImageView);
        }
        else
        {
            Glide.with(myContext).load(default_avatarnull).into(circleImageView);
        }
        TextView textViewNameChat=(TextView) convertView.findViewById(R.id.review_Name);
        if(reviewofTourResponses.get(position).getName()!=null){
            textViewNameChat.setText(reviewofTourResponses.get(position).getName());
        }
        else
            textViewNameChat.setText("No Name");

        RatingBar ratingBar=(RatingBar) convertView.findViewById(R.id.rating);
        ratingBar.setRating(reviewofTourResponses.get(position).getPoint());

        TextView textdate=(TextView) convertView.findViewById(R.id.day_review);
        Date start= new Date(Long.parseLong(reviewofTourResponses.get(position).getCreatedOn()));
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        textdate.setText(formatter.format(start));

        TextView textViewChat=(TextView) convertView.findViewById(R.id.review_textview);
        textViewChat.setText(reviewofTourResponses.get(position).getReview());
        return convertView;
    }
}
