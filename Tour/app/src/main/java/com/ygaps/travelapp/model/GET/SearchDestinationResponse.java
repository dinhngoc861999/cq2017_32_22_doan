package com.ygaps.travelapp.model.GET;

import com.google.gson.annotations.SerializedName;
import com.ygaps.travelapp.model.DestinationResponse;

import java.util.List;

public class SearchDestinationResponse {
    @SerializedName("total")
    private int total;
    @SerializedName("stopPoints")
    private List<DestinationResponse> stopPoints;

    public int getTotal() {
        return total;
    }

    public List<DestinationResponse> getDestinationResponses() {
        return stopPoints;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public void setDestinationResponses(List<DestinationResponse> stopPoints) {
        this.stopPoints = stopPoints;
    }
}
