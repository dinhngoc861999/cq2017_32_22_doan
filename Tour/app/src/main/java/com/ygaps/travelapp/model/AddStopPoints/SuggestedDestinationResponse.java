package com.ygaps.travelapp.model.AddStopPoints;

import com.google.gson.annotations.SerializedName;
import com.ygaps.travelapp.model.DestinationResponse;

import java.util.List;

public class SuggestedDestinationResponse {
    @SerializedName("stopPoints")
    private List<DestinationResponse> stopPoints;

    public List<DestinationResponse> getDestinationResponses() {
        return stopPoints;
    }

    public void setStopPoints(List<DestinationResponse> stopPoints) {
        this.stopPoints = stopPoints;
    }
}
