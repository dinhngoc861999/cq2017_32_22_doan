
package com.ygaps.travelapp.view;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.Toast;

import com.ygaps.travelapp.R;
import com.ygaps.travelapp.model.GET.TourInfoResponse;
import com.ygaps.travelapp.model.POST.CreateTourRequest;
import com.ygaps.travelapp.model.POST.CreateTourResponse;
import com.ygaps.travelapp.model.POST.DeleteTourResponse;
import com.ygaps.travelapp.model.POST.UpdateTourResquest;
import com.ygaps.travelapp.network.MyAPIClient;
import com.ygaps.travelapp.network.UserServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import android.widget.DatePicker;
public class EditTourInfor extends AppCompatActivity {
    final Context context = this;
    DatePickerDialog picker;
    private UserServices userServices;
    private EditText TourName,StartDay,EndDay,MinCost,MaxCost,Adults,Child,StartPoint,EndPoint;
    private Button CreateTour,butStartPoint,butEndPoint;
    private ImageButton butCalenderStart,butCalenderEnd;
    String tourname,startday,endday,mincost,maxcost,adults,child,startpoint,endpoint;
    private RadioButton radiotrip;
    private int tourid;
    private Double latstart,longstart,latend,longend;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_tour_infor);
        widgets();
        event();

    }


    public void widgets()
    {
        TourName = (EditText) findViewById(R.id.editTextTourName);
        StartDay= (EditText) findViewById(R.id.editTextStartDay);
        EndDay = (EditText) findViewById(R.id.editTextEndDay);
        MinCost=(EditText) findViewById(R.id.editTextMinCost);
        MaxCost=(EditText) findViewById(R.id.editTextMaxCost);
        CreateTour=(Button) findViewById(R.id.buttonCreateTour);
        radiotrip=(RadioButton) findViewById(R.id.radioPrivateTrip);
        Adults=(EditText) findViewById(R.id.editTextAdults);
        Child=(EditText) findViewById(R.id.editTextChild);
        butCalenderStart=(ImageButton) findViewById(R.id.buttonCalendarStart);
        butCalenderEnd=(ImageButton) findViewById(R.id.buttonCalendarEnd);


        StartDay.setInputType(InputType.TYPE_NULL);
        EndDay.setInputType(InputType.TYPE_NULL);

        Intent intent = getIntent();
        tourid = intent.getIntExtra("id", -1);
        tourname=intent.getStringExtra("name");
        mincost=intent.getStringExtra("minCost");
        maxcost=intent.getStringExtra("maxCost");


        TourName.setText(tourname);
        MinCost.setText(mincost);
        MaxCost.setText(maxcost);
    }

    public void event()
    {

        butCalenderStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog
                picker = new DatePickerDialog(context,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                StartDay.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            }
                        }, year, month, day);
                picker.show();
            }
        });
        butCalenderEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog
                picker = new DatePickerDialog(context,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                EndDay.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            }
                        }, year, month, day);
                picker.show();
            }
        });
        CreateTour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startday = StartDay.getText().toString().trim();
                endday = EndDay.getText().toString().trim();
                tourname = TourName.getText().toString().trim();
                mincost = MinCost.getText().toString().trim();
                maxcost = MaxCost.getText().toString().trim();
                adults = Adults.getText().toString().trim();

                child = Child.getText().toString().trim();

                if (startday.length() > 0 && endday.length() > 0 && tourname.length() > 0 ) {
                    //convert startday
                    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
                    ParsePosition pos = new ParsePosition(0);
                    Date date = (Date)formater.parse(startday, pos);
                    final long startmillis = date.getTime();

                    ParsePosition pos2 = new ParsePosition(0);
                    Date dateend=(Date) formater.parse(endday,pos2);
                    final long endmillis=dateend.getTime();


                    UpdateTourResquest updateTourResquest = new UpdateTourResquest();
                    updateTourResquest.setId(tourid);

                    updateTourResquest.setName(tourname);
                    updateTourResquest.setStartDate(startmillis);
                    updateTourResquest.setEndDate(endmillis);
                    updateTourResquest.setPrivate(radiotrip.isChecked());
                    updateTourResquest.setAdults(Integer.parseInt(adults));
                    updateTourResquest.setChilds(Integer.parseInt(child));
                    updateTourResquest.setMaxCost(Integer.parseInt(maxcost));
                    updateTourResquest.setMinCost(Integer.parseInt(mincost));
                    if(radiotrip.isChecked()){
                        Log.d("radio","true");
                    }
                    else{
                        Log.d("radio","false");
                    }


                    userServices = MyAPIClient.getInstance().getAdapter().create(UserServices.class);
                    Call<DeleteTourResponse> call = userServices.updatetour(loadToken(), updateTourResquest);

                    call.enqueue(new Callback<DeleteTourResponse>() {
                        @Override
                        public void onResponse(Call<DeleteTourResponse> call, Response<DeleteTourResponse> response) {
                            if (response.code() == 200) {
//
//                                Log.d("touridfirsr", response.body().getId() + "");
//                                Log.d("token",loadToken());
//                                Log.d("startmili",String.valueOf(startmillis));
//                                Log.d("endmili",String.valueOf(endmillis));


                                Toast.makeText(getApplicationContext(), "Tour updated ", Toast.LENGTH_SHORT).show();
                                finish();
                            } else {
                                Log.e("fail", "400/500");

                                //Xu li xem no tra ve gi roi in len layout;
                                JSONObject jsonObject = null;
                                try {
                                    jsonObject = new JSONObject(response.errorBody().string());
                                    Log.e("msg", jsonObject.get("message").toString());

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                Toast.makeText(getApplicationContext(), "Tour update failed ", Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        }

                        @Override
                        public void onFailure(Call<DeleteTourResponse> call, Throwable t) {
                            Log.e("fail", "onFailure");
                        }
                    });
                }
                else{
                    Toast.makeText(getApplicationContext(),R.string.toast_fill_in , Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent responintent)
    {
        super.onActivityResult(requestCode, resultCode, responintent);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==1) {
            int perpose= responintent.getIntExtra("perpose",-1);
            if(perpose ==2)//start day
            {
                latstart=responintent.getDoubleExtra("lat",0);
                longstart=responintent.getDoubleExtra("long",0);
                Log.e("perpose",perpose+"");
                Log.e("lat",latstart+"");
                StartPoint.setText("new point");
            }
            else if(perpose ==3)//end day
            {
                latend=responintent.getDoubleExtra("lat",0);
                longend=responintent.getDoubleExtra("long",0);
                EndPoint.setText("new point");
            }
            else {// add stop points
                finish();
            }
        }
    }
    private  String loadToken(){
        SharedPreferences sharedPreferences=getSharedPreferences(getString(R.string.SHARED_PREFS),MODE_PRIVATE);
        return sharedPreferences.getString(getString(R.string.save_token),"");
    }
}
