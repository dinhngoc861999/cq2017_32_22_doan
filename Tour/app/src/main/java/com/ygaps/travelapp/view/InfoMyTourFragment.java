package com.ygaps.travelapp.view;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ygaps.travelapp.R;
import com.ygaps.travelapp.adapter.CommentAdapter;
import com.ygaps.travelapp.model.CommentResponse;
import com.ygaps.travelapp.model.GET.GetCommentResponse;
import com.ygaps.travelapp.model.POST.DeleteTourRequest;
import com.ygaps.travelapp.model.POST.DeleteTourResponse;
import com.ygaps.travelapp.model.POST.SendCommentRequest;
import com.ygaps.travelapp.model.POST.SendCommentResponse;
import com.ygaps.travelapp.network.MyAPIClient;
import com.ygaps.travelapp.network.UserServices;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class InfoMyTourFragment extends Fragment {
    private TextView txtNameTour,txtStartDate,txtEndDate,txtAdults,txtChildren,txtMinCost,txtMaxCost;
    private Button btnFollow,btnEdit,btnSendComment;
    private String tourId,token;
    private int tourIdint;
    private int userId;
    private UserServices userServices;
    private ListView lvComment;
    private ArrayList<CommentResponse> mangComment;
    private CommentAdapter adapter;
    private Handler handler;
    private EditText edtComment;
    private Intent intentedit=new Intent();

    Toolbar toolbar;

    public InfoMyTourFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_info_my_tour, container, false);
        setHasOptionsMenu(true);
        widgets(view);
//        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("");

        SetText();
        token=loadToken();
        userId=loadUserId();

        Log.e("token",token);
        Log.e("userid",userId+"");
        adapter= new CommentAdapter(
                this.getActivity(),
                R.layout.item_comment,
                mangComment
        );
        lvComment.setAdapter(adapter);
        getComment(tourId);
        //doTheAutoRefresh();
        event();
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.information_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId())
        {
            case R.id.menu_follow:
                Intent intent = new Intent(getActivity(), RouteTourActivity.class);
                intent.putExtra("tourid",tourIdint);
                startActivity(intent);
                return true;
            case R.id.menu_edit:
                intentedit.setClass(getActivity(),EditTourInfor.class);
                startActivity(intentedit);
                return true;
            case R.id.menu_delete:
                new AlertDialog.Builder(getContext())
                        .setTitle("Are you sure?")
                        .setMessage("Do you want to delete this tour?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                UserServices userServices=MyAPIClient.getInstance().getAdapter().create(UserServices.class);
                                DeleteTourRequest deleteTourRequest=new DeleteTourRequest(tourId,-1);
                                Call<DeleteTourResponse> call=userServices.deletetour(loadToken(),deleteTourRequest);
                                call.enqueue(new Callback<DeleteTourResponse>() {
                                    @Override
                                    public void onResponse(Call<DeleteTourResponse> call, Response<DeleteTourResponse> response) {
                                        if(response.code()==200)
                                        {
                                            Intent i = new Intent(getContext(),MainActivity.class);
                                            startActivity(i);
                                        }
                                        else
                                        {
                                            Log.e("delete tour","fail");
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<DeleteTourResponse> call, Throwable t) {

                                    }
                                });

                            }
                        })
                        .setNegativeButton("No",null)
                        .show();
                return true;
            case R.id.menu_stop_point:
                Intent intentstop = new Intent(getContext(), AddStop.class);
                intentstop.putExtra("tourid", tourIdint);
                intentstop.putExtra("purpose",4);
                startActivity(intentstop);
                return true;

            default:break;
        }
        return super.onOptionsItemSelected(item);
    }




    public void event()
    {

        btnSendComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SendCommentRequest sendCommentRequest=new SendCommentRequest();
                sendCommentRequest.setTourId(tourId);
                String sUserId=Integer.toString(userId);
                sendCommentRequest.setUserId(sUserId);
                String sComment=edtComment.getText().toString();
                edtComment.setText("");
                sendCommentRequest.setComment(sComment);
                if(!sComment.equals(""))
                    sendComment(sendCommentRequest);

            }
        });
    }

    public void SetText()
    {
        Bundle bundle= getArguments();
        if(bundle!=null)
        {
            int id = bundle.getInt("id",0 );
            tourIdint=id;
            Log.e("tourid",String.valueOf(tourIdint));
            int status = bundle.getInt("status", 0);
            String name = bundle.getString("name", "");
            String minCost = bundle.getString("minCost", "");
            String maxCost = bundle.getString("maxCost", "");
            String startDate = bundle.getString("startDate", "");
            String endDate = bundle.getString("endDate", "");
            int adults = bundle.getInt("adults",0);
            int childs = bundle.getInt("childs", 0);
            boolean isPrivate=bundle.getBoolean("isPrivate", false);
            String avatar= bundle.getString("avatar", "");

            Date start=new Date(Long.parseLong(startDate));
            Date end=new Date(Long.parseLong(endDate));
            SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");//formating according to my need

            txtNameTour.setText(name);
            txtStartDate.setText(formatter.format(start));
            txtEndDate.setText(formatter.format(end));
            txtAdults.setText(Integer.toString(adults));
            txtChildren.setText(Integer.toString(childs));
            txtMinCost.setText(minCost);
            txtMaxCost.setText(maxCost);
            tourId=Integer.toString(id);

            intentedit.putExtra("id",id);
            intentedit.putExtra("name",name);
            intentedit.putExtra("minCost",minCost);
            intentedit.putExtra("maxCost",maxCost);
            intentedit.putExtra("startday",startDate);
            intentedit.putExtra("endday",endDate);
            intentedit.putExtra("child",childs);
            intentedit.putExtra("adults",adults);





        }

    }

    private void doTheAutoRefresh() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                getComment(tourId);
                doTheAutoRefresh();
            }
        }, 10000);
    }

    public void widgets(View view)
    {
        txtNameTour = (TextView)view.findViewById(R.id.textNameTour);
        txtStartDate = (TextView) view.findViewById(R.id.textStarDate);
        txtEndDate = (TextView) view.findViewById(R.id.textEndDate);
        txtAdults = (TextView) view.findViewById(R.id.textViewPerson);
        txtChildren = (TextView) view.findViewById(R.id.textViewChild);
        txtMinCost = (TextView) view.findViewById(R.id.textViewMinCost);
        txtMaxCost = (TextView) view.findViewById(R.id.textViewMaxCost);

        lvComment=(ListView)view.findViewById(R.id.listViewCommentMytour);
        edtComment=(EditText) view.findViewById(R.id.editTextCommentForSendmyTour);
        btnSendComment=(Button) view.findViewById(R.id.buttonSendCommentMytour);
        mangComment=new ArrayList<CommentResponse>();
        handler=new Handler();
        toolbar=(Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);

    }

    public void getComment(String tourId)
    {
        int pageIndex=1;
        int pageSize=100;
        userServices= MyAPIClient.getInstance().getAdapter().create(UserServices.class);
        Call<GetCommentResponse> call=userServices.getCommentResponse(token,tourId, pageIndex,pageSize);
        call.enqueue(new Callback<GetCommentResponse>() {
            @Override
            public void onResponse(Call<GetCommentResponse> call, Response<GetCommentResponse> response) {
                if(response.code()==200) {
                    Log.e("comment ","success");
                    mangComment.clear();
                    int size=response.body().getCommentResponses().size();
                    if (size!=0)
                    {
                        for (int i=0; i<size; i++)
                        {
                            mangComment.add(fResponse(response,i));
                        }
                        Collections.reverse(mangComment);
                        adapter.notifyDataSetChanged();
                    }

                }
                else{
                    //todo
                    Log.e("400","400");
                }

            }
            @Override
            public void onFailure(Call<GetCommentResponse> call, Throwable t) {

            }
        });
    }

    public void sendComment(SendCommentRequest sendCommentRequest)
    {
        /*Log.e("tourId", sendCommentRequest.getTourId());
        Log.e("userId", sendCommentRequest.getUserId());
        Log.e("comment", sendCommentRequest.getComment());*/
        userServices= MyAPIClient.getInstance().getAdapter().create(UserServices.class);
        Call<SendCommentResponse> callcmt=userServices.sendCommentResponse(token,sendCommentRequest);
        callcmt.enqueue(new Callback<SendCommentResponse>() {
            @Override
            public void onResponse(Call<SendCommentResponse> call, Response<SendCommentResponse> response) {
                if(response.code()==200){
                    getComment(tourId);
                }
                else {
                    //todo
                    Log.e("sendcomment","400");
                }
            }

            @Override
            public void onFailure(Call<SendCommentResponse> call, Throwable t) {
                Log.e("sendcomment","400");
            }
        });
    }

    private CommentResponse fResponse(Response<GetCommentResponse> response, int i)
    {
        CommentResponse commentResponse;
        commentResponse= new CommentResponse();
        commentResponse.setId(response.body().getCommentResponses().get(i).getId());
        commentResponse.setName(response.body().getCommentResponses().get(i).getName());
        commentResponse.setAvatar(response.body().getCommentResponses().get(i).getAvatar());
        commentResponse.setComment(response.body().getCommentResponses().get(i).getComment());
        commentResponse.setCreatedOn(response.body().getCommentResponses().get(i).getCreatedOn());
        return commentResponse;
    }


    private  String loadToken()
    {
        SharedPreferences sharedPreferences=this.getActivity().getSharedPreferences(getString(R.string.SHARED_PREFS), Context.MODE_PRIVATE);
        return sharedPreferences.getString(getString(R.string.save_token),"");
    }

    private  int loadUserId(){
        SharedPreferences sharedPreferences=this.getActivity().getSharedPreferences(getString(R.string.SHARED_PREFS),Context.MODE_PRIVATE);
        return sharedPreferences.getInt(getString(R.string.save_userId),0);
    }

}
