package com.ygaps.travelapp.view;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import com.ygaps.travelapp.R;
import com.ygaps.travelapp.adapter.MemberAdapter;
import com.ygaps.travelapp.model.GET.MemberResponse;
import com.ygaps.travelapp.model.GET.TourInfoResponse;
import com.ygaps.travelapp.network.MyAPIClient;
import com.ygaps.travelapp.network.UserServices;

import java.lang.reflect.Member;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class MemberOfMyTourFragment extends Fragment {

    Button btnSearchUser;
    private ListView lvMember;
    private ArrayList<MemberResponse> mangMemberResponses;
    private MemberAdapter adapter;
    private UserServices userServices;
    private int total;
    private String token;
    private int fresh_count;
    int pageSize=10;
    private int userId;
    private int tourId;

    public MemberOfMyTourFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_member_of_my_tour, container, false);
        btnSearchUser=(Button) view.findViewById(R.id.ButtonAddUser);

        btnSearchUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(getActivity(), SearchUserActivity.class);
                Bundle bundle=new Bundle();
                bundle.putInt("id",tourId);
                intent.putExtras(bundle);
                startActivity(intent);

            }
        });

        lvMember=(ListView) view.findViewById(R.id.listViewMember);
        mangMemberResponses= new ArrayList<MemberResponse>();

        adapter=new MemberAdapter(
                getActivity(),
                R.layout.item_member,
                mangMemberResponses
        );

        lvMember.setAdapter(adapter);
        token = loadToken();
        Bundle bundle= getArguments();
        if(bundle!=null){
            tourId= bundle.getInt("id",0 );
            Log.e("aaa",tourId+"");
        }

        userServices = MyAPIClient.getInstance().getAdapter().create(UserServices.class);
        Call<TourInfoResponse> call = userServices.TOUR_INFO_RESPONSE_CALL(token, tourId);
        call.enqueue(new Callback<TourInfoResponse>() {
            @Override
            public void onResponse(Call<TourInfoResponse> call, Response<TourInfoResponse> response) {
                if (response.code() == 200) {
                    total = response.body().getMember().size();
                    Log.e("total member", String.valueOf(total));
                    for (int i = 0; i < 14 && i < total; i++) {
                        mangMemberResponses.add(fMemberResponse(response,i));
                    }

                    adapter.notifyDataSetChanged();
                    lvMember.setAdapter(adapter);
                } else {
                    //todo
                    Log.e("400", "400");
                    Log.e("error",response.errorBody().toString()+"123");
                }
            }

            @Override
            public void onFailure(Call<TourInfoResponse> call, Throwable t) {

            }
        });


        return view;
    }

    private String loadToken() {
        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences(getString(R.string.SHARED_PREFS), MODE_PRIVATE);
        return sharedPreferences.getString(getString(R.string.save_token), "");
    }

    private MemberResponse fMemberResponse(Response<TourInfoResponse> response, int i)
    {
        MemberResponse memberResponse= new MemberResponse();
        memberResponse.setId(response.body().getMember().get(i).getId());
        memberResponse.setName(response.body().getMember().get(i).getName());
        memberResponse.setPhone(response.body().getMember().get(i).getPhone());
        memberResponse.setAvatar(response.body().getMember().get(i).getAvatar());
        memberResponse.setHost(response.body().getMember().get(i).isHost());
        return memberResponse;
    }

}
