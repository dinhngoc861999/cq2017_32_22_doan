package com.ygaps.travelapp.model;

import com.google.gson.annotations.SerializedName;

public class GetCoordinate {
    @SerializedName("id")
    private String id;
    @SerializedName("lat")
    private double lat;
    @SerializedName("long")
    private double lon;

    public String getId() {
        return id;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }
}
