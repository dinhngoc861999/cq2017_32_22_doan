package com.ygaps.travelapp.view;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.ygaps.travelapp.R;
import com.ygaps.travelapp.adapter.ChatAdapter;
import com.ygaps.travelapp.model.ChatResponse;
import com.ygaps.travelapp.model.GET.GetChatResponse;
import com.ygaps.travelapp.model.POST.SendChatRequest;
import com.ygaps.travelapp.model.POST.SendChatResponse;
import com.ygaps.travelapp.network.MyAPIClient;
import com.ygaps.travelapp.network.UserServices;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatMyTourFragment extends Fragment {

    private ListView listView;
    private ArrayList<ChatResponse> chatResponses;
    private UserServices userServices;
    private EditText editTextchat;
    private Button button;
    private String tourId;
    private ChatAdapter adapter;
    private Handler handler;
    private String token;
    private String userId;
    private TextView textView;
    private Button btnPlay, btnStop, btnRecord;
    private MediaRecorder myAudioRecorder;
    private String outputFile;
    public static final int REQUEST_CODE_RECORD = 4249;

    public ChatMyTourFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_chat_my_tour, container, false);

        Widget(view);
        adapter=new ChatAdapter(
                getActivity(),
                R.layout.adapter_chat,
                chatResponses
        );
        listView.setAdapter(adapter);
        userServices= MyAPIClient.getInstance().getAdapter().create(UserServices.class);
        Loadchat();
        //doTheAutoRefresh();

        Log.e("token",token);
        Log.e("tour id",tourId);

        btnStop.setEnabled(false);
        btnPlay.setEnabled(false);
        outputFile = Environment.getExternalStorageDirectory().getAbsolutePath() + "/recording.3gp";
        myAudioRecorder = new MediaRecorder();
//        myAudioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
//        myAudioRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
//        myAudioRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
//        myAudioRecorder.setOutputFile(outputFile);

        event();
        return view;
    }

    public void event()
    {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String noti=editTextchat.getText().toString();
                if(!noti.equals(""))
                {
                    editTextchat.setText("");
                    sendNoti(noti);
                }
            }
        });


        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myAudioRecorder.stop();
                myAudioRecorder.release();
                myAudioRecorder = null;
                btnRecord.setEnabled(true);
                btnStop.setEnabled(false);
                btnPlay.setEnabled(true);
                Toast.makeText(getActivity(), "Audio Recorder successfully", Toast.LENGTH_LONG).show();

            }
        });

        btnRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)!=PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_RECORD);
                }
                else{
                    myAudioRecorder = new MediaRecorder();
                    myAudioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                    myAudioRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
                    myAudioRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
                    myAudioRecorder.setOutputFile(outputFile);
                    try {
                        myAudioRecorder.prepare();
                        myAudioRecorder.start();
                    } catch (IllegalStateException ise) {
                        // make something ...
                    } catch (IOException ioe) {
                        // make something
                    }
                btnRecord.setEnabled(false);
                btnStop.setEnabled(true);
                }
                Toast.makeText(getActivity(), "Recording started", Toast.LENGTH_LONG).show();

            }
        });


        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    mediaPlayer.setDataSource(outputFile);
                    mediaPlayer.prepare();
                    mediaPlayer.start();
                    Toast.makeText(getActivity(), "Playing Audio", Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    // make something
                }
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode==REQUEST_CODE_RECORD && grantResults.length>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED)
            myAudioRecorder = new MediaRecorder();
            myAudioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            myAudioRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            myAudioRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
            myAudioRecorder.setOutputFile(outputFile);
        try {
                    myAudioRecorder.prepare();
                    myAudioRecorder.start();
            } catch (IllegalStateException ise) {
                    // make something ...
            } catch (IOException ioe) {
                    // make something
            }
            btnRecord.setEnabled(false);
            btnStop.setEnabled(true);

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void sendNoti(String noti)
    {
        SendChatRequest sendChatRequest=new SendChatRequest(tourId,userId,noti);
        Call<SendChatResponse> call=userServices.sendChat(token,sendChatRequest);
        call.enqueue(new Callback<SendChatResponse>() {
            @Override
            public void onResponse(Call<SendChatResponse> call, Response<SendChatResponse> response) {
                if(response.code()==200)
                {
                    Log.e("Send chat", response.body().getMessage());
                    Loadchat();
                }
                else {
                    try {
                        Log.e("Send chat fail", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<SendChatResponse> call, Throwable t) {
                Log.e("Send chat", " failure");
            }
        });
    }


    private void doTheAutoRefresh() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Loadchat();
                doTheAutoRefresh();
            }
        }, 10000);
    }


    public void Widget(View view)
    {
        textView=(TextView)view.findViewById(R.id.noti_chat);
        chatResponses=new ArrayList<ChatResponse>();
        handler=new Handler();
        listView=(ListView)view.findViewById(R.id.listview_chat);
        editTextchat=(EditText)view.findViewById(R.id.edittextChat);
        button=(Button)view.findViewById(R.id.buttonSendchat);
        Bundle bundle= getArguments();
        if(bundle!=null){
            tourId= bundle.getInt("id",0 )+"";
            Log.e("aaa",tourId);
        }
        token=loadToken();
        userId=loadUserId()+"";
        btnPlay = (Button) view.findViewById(R.id.btnPlay);
        btnStop = (Button) view.findViewById(R.id.btnStop);
        btnRecord = (Button) view.findViewById(R.id.btnRecord);


    }

    public void Loadchat()
    {
        Call<GetChatResponse> call=userServices.getChat(token,tourId,1,100);
        call.enqueue(new Callback<GetChatResponse>() {
            @Override
            public void onResponse(Call<GetChatResponse> call, Response<GetChatResponse> response) {
                if(response.code()==200)
                {
                    chatResponses.clear();
                    for(int i=0;i<response.body().getChatResponses().size();i++)
                    {
                        chatResponses.add(fChat(response,i));
                    }
                    Collections.reverse(chatResponses);
                    adapter.notifyDataSetChanged();
                    if(chatResponses.size()==0)
                        textView.setText("No menber Chat in tour");
                    else
                        textView.setText("");
                }
                else
                {
                    try {
                        Log.e("GetChat fail", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<GetChatResponse> call, Throwable t) {
                Log.e("GetChat","failure");
            }
        });
    }

    private  String loadToken()
    {
        SharedPreferences sharedPreferences=this.getActivity().getSharedPreferences(getString(R.string.SHARED_PREFS), Context.MODE_PRIVATE);
        return sharedPreferences.getString(getString(R.string.save_token),"");
    }

    private  int loadUserId(){
        SharedPreferences sharedPreferences=this.getActivity().getSharedPreferences(getString(R.string.SHARED_PREFS),Context.MODE_PRIVATE);
        return sharedPreferences.getInt(getString(R.string.save_userId),0);
    }

    private ChatResponse fChat(Response<GetChatResponse> response, int i)
    {
        ChatResponse chatResponse=new ChatResponse();
        chatResponse.setAvatar(response.body().getChatResponses().get(i).getAvatar());
        chatResponse.setName(response.body().getChatResponses().get(i).getName());
        chatResponse.setUserId(response.body().getChatResponses().get(i).getUserId());
        chatResponse.setNotification(response.body().getChatResponses().get(i).getNotification());
        return  chatResponse;
    }
}
