package com.ygaps.travelapp.model.POST;

import com.google.gson.annotations.SerializedName;

public class LoginFBRequest {
    @SerializedName("accessToken")
    private String accessToken;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
