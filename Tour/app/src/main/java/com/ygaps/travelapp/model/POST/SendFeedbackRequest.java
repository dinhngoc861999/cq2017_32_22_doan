package com.ygaps.travelapp.model.POST;

import com.google.gson.annotations.SerializedName;

public class SendFeedbackRequest {
    @SerializedName("serviceId")
    private int serviceId;
    @SerializedName("point")
    private int point;
    @SerializedName("feedback")
    private String feebback;

    public SendFeedbackRequest(int serviceId, int point, String feebback) {
        this.serviceId = serviceId;
        this.point = point;
        this.feebback = feebback;
    }

    public int getServiceId() {
        return serviceId;
    }

    public int getPoint() {
        return point;
    }

    public String getReview() {
        return feebback;
    }

    public void setTourId(int serviceId) {
        this.serviceId = serviceId;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public void setReview(String feebback) {
        this.feebback = feebback;
    }
}
