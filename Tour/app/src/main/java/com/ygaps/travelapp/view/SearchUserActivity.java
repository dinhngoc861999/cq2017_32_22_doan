package com.ygaps.travelapp.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.SearchView;

import android.os.Bundle;
import android.util.Log;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.Toast;

import com.ygaps.travelapp.R;
import com.ygaps.travelapp.adapter.UserSearchAdapter;
import com.ygaps.travelapp.model.GET.SearchUsersAccordingKeyWordResponse;
import com.ygaps.travelapp.model.POST.InviteMemberRequest;
import com.ygaps.travelapp.model.POST.InviteMemberResponse;
import com.ygaps.travelapp.model.SearchUserResponse;
import com.ygaps.travelapp.model.TourResponse;
import com.ygaps.travelapp.network.MyAPIClient;
import com.ygaps.travelapp.network.UserServices;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchUserActivity extends AppCompatActivity {

    private SearchView searchView;
    private UserServices userServices;
    private ListView lvUser;
    private ArrayList<SearchUserResponse> mangUserResponses;
    private UserSearchAdapter adapter;
    private int total;
    private int fresh_count;
    int pageSize=10;
    private int userId;
    private int tourId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_user);

         lvUser=(ListView) findViewById(R.id.listViewUserSearch);
         mangUserResponses= new ArrayList<SearchUserResponse>();
         searchView=(SearchView) findViewById(R.id.searchUser);

         adapter=new UserSearchAdapter(
                this,
                R.layout.item_user,
                mangUserResponses
         );

         lvUser.setAdapter(adapter);

         setDataByBundle();

        userServices= MyAPIClient.getInstance().getAdapter().create(UserServices.class);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(newText.equals(""))
                {
                    fresh_count=1;
                    mangUserResponses.clear();
                    ListUserResponse("");
                    adapter.notifyDataSetChanged();
                }
                else{
                    fresh_count=1;
                    mangUserResponses.clear();
                    ListUserResponse(newText);
                    adapter.notifyDataSetChanged();
                }
                return true;
            }
        });

        lvUser.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String userid=mangUserResponses.get(position).getId()+"";
                Log.e("userId",userid);
                Log.e("tourId", tourId+"");
                UserServices userServices1= MyAPIClient.getInstance().getAdapter().create(UserServices.class);
                InviteMemberRequest inviteMemberRequest=new InviteMemberRequest(tourId+"",userid,true);

                Call<InviteMemberResponse> call=userServices.inviteMember(loadToken(),inviteMemberRequest);
                call.enqueue(new Callback<InviteMemberResponse>() {
                    @Override
                    public void onResponse(Call<InviteMemberResponse> call, Response<InviteMemberResponse> response) {
                        if(response.code()==200)
                        {
                            Log.e("msg",response.body().getMessage());
                            if(response.body().getMessage().equals("Successful"))
                                Toast.makeText(getApplicationContext(),"Successful",Toast.LENGTH_SHORT).show();
                            if(response.body().getMessage().equals("Already invited this member"))
                                Toast.makeText(getApplicationContext(),"Already invited this member",Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            try {
                                Log.e("fail",response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            Toast.makeText(getApplicationContext(),"fail",Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<InviteMemberResponse> call, Throwable t) {

                    }
                });
                returnMTMainActivity();
            }
        });

    }

    public  void  returnMTMainActivity(){
        Intent intent = new Intent();
        Bundle bundle= new Bundle();
        intent.putExtras(bundle);
        setResult(RESULT_OK,intent);
        finish();
    }

    private void ListUserResponse(final String searchKey){
        Call<SearchUsersAccordingKeyWordResponse> call=userServices.searchUsersAccordingKeyWord(searchKey, fresh_count, pageSize);
     //   Log.e("word: ",searchKey);
        call.enqueue(new Callback<SearchUsersAccordingKeyWordResponse>() {
            @Override
            public void onResponse(Call<SearchUsersAccordingKeyWordResponse> call, Response<SearchUsersAccordingKeyWordResponse> response) {
                if(response.code()==200) {
                    for(int i=0; i<response.body().UsersResponses().size(); i++)
                    {
                            mangUserResponses.add(fUserResponse(response,i));
                    }
                    total=response.body().getTotal();

                    Log.e("total: ",total+"");

                    adapter.notifyDataSetChanged();
                    lvUser.setAdapter(adapter);
                }
                else{
                    //todo
                    Log.e("400","400");
                }
                fresh_count++;
            }

            @Override
            public void onFailure(Call<SearchUsersAccordingKeyWordResponse> call, Throwable t) {

            }
        });

        lvUser.setTag(0);
        lvUser.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, final int totalItemCount) {

                int lastVisibleItem=firstVisibleItem+visibleItemCount;
                int preLast= ((int) view.getTag());
                if (lastVisibleItem==totalItemCount)
                {
                    if (lastVisibleItem!= preLast)
                    {
                        if(totalItemCount+10<total){
                            UserServices userServices1=MyAPIClient.getInstance().getAdapter().create(UserServices.class);
                            Call<SearchUsersAccordingKeyWordResponse> call=userServices.searchUsersAccordingKeyWord(searchKey, fresh_count, pageSize);
                           // Log.e("word+: ",searchKey);
                            call.enqueue(new Callback<SearchUsersAccordingKeyWordResponse>() {
                                @Override
                                public void onResponse(Call<SearchUsersAccordingKeyWordResponse> call, Response<SearchUsersAccordingKeyWordResponse> response) {
                                    if(response.code()==200) {
                                        for(int i=0; i<response.body().UsersResponses().size(); i++)
                                        {
                                                mangUserResponses.add(fUserResponse(response,i));

                                        }
                                        adapter.notifyDataSetChanged();
                                        total=response.body().getTotal();
                                        fresh_count++;
                                    }
                                    else{
                                        //todo
                                        Log.e("400","400");
                                    }
                                }
                                @Override
                                public void onFailure(Call<SearchUsersAccordingKeyWordResponse> call, Throwable t) {
                                    //todo
                                    Log.e("400","failure");
                                }
                            });
                            preLast =lastVisibleItem;
                        }
                    }

                }
                view.setTag(preLast);
            }
        });
    }

    public void setDataByBundle()
    {
        Intent intent=getIntent();
        Bundle bundle=intent.getExtras();
        if (bundle != null) {
            tourId=bundle.getInt("id",0);
            Log.e("tourId", tourId+"");
        }
    }

    private SearchUserResponse fUserResponse(Response<SearchUsersAccordingKeyWordResponse> response, int i)
    {
        SearchUserResponse searchUserResponse= new SearchUserResponse();
        searchUserResponse.setId(response.body().UsersResponses().get(i).getId());
        userId=Integer.valueOf(response.body().UsersResponses().get(i).getId());
        searchUserResponse.setFullName(response.body().UsersResponses().get(i).getFullName());
        searchUserResponse.setEmail(response.body().UsersResponses().get(i).getEmail());
        searchUserResponse.setPhone(response.body().UsersResponses().get(i).getPhone());
        searchUserResponse.setGender(response.body().UsersResponses().get(i).getGender());
        searchUserResponse.setDob(response.body().UsersResponses().get(i).getDob());
        searchUserResponse.setTypeLogin(response.body().UsersResponses().get(i).getTypeLogin());
        return  searchUserResponse;
    }

    private  String loadToken()
    {
        SharedPreferences sharedPreferences=this.getSharedPreferences(getString(R.string.SHARED_PREFS), MODE_PRIVATE);
        return sharedPreferences.getString(getString(R.string.save_token),"");
    }
}
