package com.ygaps.travelapp.model.GET;

import com.google.gson.annotations.SerializedName;
import com.ygaps.travelapp.model.SearchUserResponse;

import java.util.List;

public class SearchUsersAccordingKeyWordResponse {
    @SerializedName("total")
    private int total;
    @SerializedName("users")
    private List<SearchUserResponse> searchUserResponses;

    public int getTotal() {
        return total;
    }

    public List<SearchUserResponse> UsersResponses() {
        return searchUserResponses;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public void setUserResponses(List<SearchUserResponse> userResponses) {
        this.searchUserResponses=userResponses;
    }
}
