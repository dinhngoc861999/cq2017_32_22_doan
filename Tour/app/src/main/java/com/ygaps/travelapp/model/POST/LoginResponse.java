package com.ygaps.travelapp.model.POST;

import com.google.gson.annotations.SerializedName;

public class LoginResponse {
    @SerializedName("userId")
    private int userId;
    @SerializedName("emailVerified")
    private boolean emailVerified;
    @SerializedName("phoneVerified")
    private boolean phoneVerified;
    @SerializedName("token")
    private String token;

    public int getUserId() {
        return userId;
    }

    public boolean isEmailVerified() {
        return emailVerified;
    }

    public boolean isPhoneVerified() {
        return phoneVerified;
    }

    public String getToken() {
        return token;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setEmailVerified(boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public void setPhoneVerified(boolean phoneVerified) {
        this.phoneVerified = phoneVerified;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
