package com.ygaps.travelapp.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ygaps.travelapp.R;
import com.ygaps.travelapp.model.ChatResponse;
import com.ygaps.travelapp.model.InviteNotiResponse;
import com.ygaps.travelapp.model.POST.ResponseInviteResponse;
import com.ygaps.travelapp.model.POST.ResponseInviteResquest;
import com.ygaps.travelapp.network.MyAPIClient;
import com.ygaps.travelapp.network.UserServices;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InviteMemberAdapter extends BaseAdapter {

    Context myContext;
    int myLayout;
    List<InviteNotiResponse> inviteNotiResponses ;
    String token;

    public InviteMemberAdapter(Context context, int layout, List<InviteNotiResponse> inviteList,String token1){
        myContext=context;
        myLayout=layout;
        inviteNotiResponses=inviteList;
        token=token1;
    }

    @Override
    public int getCount() {
        return inviteNotiResponses.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater=(LayoutInflater)myContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate(myLayout,null,true);


            holder.circleImageView=(CircleImageView) convertView.findViewById(R.id.imageavatar_invite);
            holder.textViewName=(TextView) convertView.findViewById(R.id.review_Name);
            holder.textViewDate=(TextView) convertView.findViewById(R.id.day_review);
            holder.textViewNoti=(TextView) convertView.findViewById(R.id.invite_textview);
            holder.textViewDeny=(TextView)convertView.findViewById(R.id.text_deny);
            holder.textViewAccept=(TextView)convertView.findViewById(R.id.text_accept);

            holder.imgDeny=(ImageView)convertView.findViewById(R.id.icon_deny);
            holder.imgAccept=(ImageView)convertView.findViewById(R.id.icon_accept);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder)convertView.getTag();
        }


        //avatar
        String default_avatarnull="http://noiswebsite.blob.core.windows.net/images/avatar_null.png";
        if(inviteNotiResponses.get(position).getHostAvatar()!=null) {
            Glide.with(myContext).load(inviteNotiResponses.get(position).getHostAvatar()).into(holder.circleImageView);
        }
        else
        {
            Glide.with(myContext).load(default_avatarnull).into(holder.circleImageView);
        }
        //ten
        if(inviteNotiResponses.get(position).getHostName()!=null){
            holder.textViewName.setText(inviteNotiResponses.get(position).getHostName());
        }
        else
            holder.textViewName.setText("No Name");
        //ngay
        Date start= new Date(Long.parseLong(inviteNotiResponses.get(position).getCreatedOn()));
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        holder.textViewDate.setText(formatter.format(start));
        //thongbao
        holder.textViewNoti.setText("Invite you in tour "+ inviteNotiResponses.get(position).getName());

        holder.textViewDeny.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final UserServices userServices=MyAPIClient.getInstance().getAdapter().create(UserServices.class);

                String tourId=inviteNotiResponses.get(position).getId()+"";
                ResponseInviteResquest inviteResquest=new ResponseInviteResquest(tourId,false);
                Call<ResponseInviteResponse> call=userServices.responseInvite(token,inviteResquest);
                call.enqueue(new Callback<ResponseInviteResponse>() {
                    @Override
                    public void onResponse(Call<ResponseInviteResponse> call, Response<ResponseInviteResponse> response) {
                        if(response.code()==200)
                        {
                            Log.e("Accept succes","");
                        }
                        else
                        {
                            Log.e("Response invite", " fail 400");
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseInviteResponse> call, Throwable t) {

                    }
                });
            }
        });


        holder.textViewAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final UserServices userServices=MyAPIClient.getInstance().getAdapter().create(UserServices.class);

                String tourId=inviteNotiResponses.get(position).getId()+"";
                ResponseInviteResquest inviteResquest=new ResponseInviteResquest(tourId,true);
                Call<ResponseInviteResponse> call=userServices.responseInvite(token,inviteResquest);
                call.enqueue(new Callback<ResponseInviteResponse>() {
                    @Override
                    public void onResponse(Call<ResponseInviteResponse> call, Response<ResponseInviteResponse> response) {
                        if(response.code()==200)
                        {
                            Log.e("deny succes","");
                        }
                        else
                        {
                            Log.e("Response invite", " fail 400");
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseInviteResponse> call, Throwable t) {

                    }
                });
            }
        });


        holder.imgAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final UserServices userServices=MyAPIClient.getInstance().getAdapter().create(UserServices.class);

                String tourId=inviteNotiResponses.get(position).getId()+"";
                ResponseInviteResquest inviteResquest=new ResponseInviteResquest(tourId,true);
                Call<ResponseInviteResponse> call=userServices.responseInvite(token,inviteResquest);
                call.enqueue(new Callback<ResponseInviteResponse>() {
                    @Override
                    public void onResponse(Call<ResponseInviteResponse> call, Response<ResponseInviteResponse> response) {
                        if(response.code()==200)
                        {
                            Log.e("Accept succes","");
                        }
                        else
                        {
                            Log.e("Response invite", " fail 400");
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseInviteResponse> call, Throwable t) {

                    }
                });
            }
        });

        holder.imgDeny.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final UserServices userServices=MyAPIClient.getInstance().getAdapter().create(UserServices.class);

                String tourId=inviteNotiResponses.get(position).getId()+"";
                ResponseInviteResquest inviteResquest=new ResponseInviteResquest(tourId,false);
                Call<ResponseInviteResponse> call=userServices.responseInvite(token,inviteResquest);
                call.enqueue(new Callback<ResponseInviteResponse>() {
                    @Override
                    public void onResponse(Call<ResponseInviteResponse> call, Response<ResponseInviteResponse> response) {
                        if(response.code()==200)
                        {
                            Log.e("Accept succes","");
                        }
                        else
                        {
                            Log.e("Response invite", " fail 400");
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseInviteResponse> call, Throwable t) {

                    }
                });
            }
        });
        return convertView;
    }

    private class ViewHolder {
        protected CircleImageView circleImageView;
        protected TextView textViewName, textViewDate,textViewNoti, textViewDeny, textViewAccept;
        protected ImageView imgAccept,imgDeny;
    }
}
