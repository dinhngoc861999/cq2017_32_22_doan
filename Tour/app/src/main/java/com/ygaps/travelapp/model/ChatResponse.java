package com.ygaps.travelapp.model;

import com.google.gson.annotations.SerializedName;

public class ChatResponse {
    @SerializedName("userId")
    private String userId;
    @SerializedName("name")
    private String name;
    @SerializedName("avatar")
    private String avatar;
    @SerializedName("notification")
    private String notification;

    public String getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getNotification() {
        return notification;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }
}
